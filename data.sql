/*
SQLyog Trial v12.2.2 (64 bit)
MySQL - 10.1.13-MariaDB : Database - jioscms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jioscms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jioscms`;

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values 
('e4195b7559c762724a88d79962f91e47a9619cba','127.0.0.1',1463928327,'__ci_last_regenerate|i:1463928327;'),
('98f1259d1f822a3dbff4d9c025845126bd63e745','127.0.0.1',1463929544,'__ci_last_regenerate|i:1463929544;'),
('95ea1fb69339d590f4a97c682729e3ab232d8fe2','127.0.0.1',1463929557,'__ci_last_regenerate|i:1463929544;'),
('c3ff915649ae563a3becc0458e3bdb31880d3271','127.0.0.1',1463927884,'__ci_last_regenerate|i:1463927884;'),
('da8e7f26b4fcc3afa193f0a731f6e51b77c41111','127.0.0.1',1463911428,'__ci_last_regenerate|i:1463911428;'),
('f91606bcc461a32671c4ae5cb4e3570cf61f51f5','127.0.0.1',1463912014,'__ci_last_regenerate|i:1463912014;'),
('fd8770755a83100e8f6667e0aa66dadfff754c86','127.0.0.1',1463911428,'__ci_last_regenerate|i:1463911428;'),
('359041433d003bf0c6fc5ed6e0a43375a805f78c','127.0.0.1',1463913066,'__ci_last_regenerate|i:1463913066;'),
('2453a6d759f58194be7ea3a37803d4bc9f1edaef','127.0.0.1',1463913102,'__ci_last_regenerate|i:1463913066;'),
('670e86229b1b559e97b56f8fc5713d8838bc208b','127.0.0.1',1463920189,'__ci_last_regenerate|i:1463920179;'),
('86d920537a6a14a14045c14829d8afe91e279e34','127.0.0.1',1463911177,'__ci_last_regenerate|i:1463911177;'),
('f4b0bdf500b4f402bbf7ac2fbe34b3cd8aac8c9e','127.0.0.1',1463909641,'__ci_last_regenerate|i:1463909641;'),
('fa3b3bee099ec2e5b3da8d59a02709ee45a193fa','127.0.0.1',1463907250,'__ci_last_regenerate|i:1463907250;'),
('2de0746a1a2df652078d68b0c388d0f9605bd51f','127.0.0.1',1463897700,'__ci_last_regenerate|i:1463897700;'),
('5bf01cc5860a8529b8e0082940269b97c6818f3d','127.0.0.1',1463897700,'__ci_last_regenerate|i:1463897700;'),
('2098fea358f28734f975a26a795865d745066dbb','127.0.0.1',1463904390,'__ci_last_regenerate|i:1463904390;'),
('9f423ff7281307acaf4f0774a618bca092d43849','127.0.0.1',1463905814,'__ci_last_regenerate|i:1463905814;');

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1101 DEFAULT CHARSET=utf8;

/*Data for the table `test` */

/*Table structure for table `webts_blog` */

DROP TABLE IF EXISTS `webts_blog`;

CREATE TABLE `webts_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '所属用户',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `safety` int(11) NOT NULL DEFAULT '0' COMMENT '是否加密,默认不加密',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `webts_blog` */

/*Table structure for table `webts_menus` */

DROP TABLE IF EXISTS `webts_menus`;

CREATE TABLE `webts_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '分类名称',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级id默认0为最顶级分类',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT '排序从小到大',
  `is_show` int(11) NOT NULL DEFAULT '1' COMMENT '是否显示在首页,默认1显示,0不显示',
  `diy_url` varchar(100) DEFAULT NULL COMMENT '自定义URL',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `webts_menus` */

insert  into `webts_menus`(`id`,`name`,`parent_id`,`sort_order`,`is_show`,`diy_url`) values 
(1,'博客',0,1,1,NULL),
(2,'美图',0,2,1,NULL),
(3,'资料',0,3,1,NULL),
(4,'工具',0,4,1,NULL),
(5,'海贼王',2,1,1,NULL),
(6,'美女',2,2,1,NULL),
(7,'Java技术',1,2,1,NULL),
(8,'PHP',1,1,1,NULL),
(9,'路飞',5,1,1,NULL),
(10,'罗宾',5,2,1,NULL);

/*Table structure for table `webts_posts` */

DROP TABLE IF EXISTS `webts_posts`;

CREATE TABLE `webts_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '所属用户',
  `menu_id` int(11) NOT NULL COMMENT '文章所属栏目',
  `type_id` int(11) NOT NULL DEFAULT '1' COMMENT '文章类型,比如博文,或者其他特殊的类型',
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `update_date` datetime NOT NULL COMMENT '默认当前时间',
  `create_date` datetime NOT NULL COMMENT '第一次创建的时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

/*Data for the table `webts_posts` */

insert  into `webts_posts`(`id`,`user_id`,`menu_id`,`type_id`,`title`,`content`,`update_date`,`create_date`) values 
(1,1,1,1,'第一篇博文','这是我的第一篇博文谢谢','2016-03-04 15:21:41','2016-03-04 15:21:44'),
(2,1,1,1,'第二篇博文','详细信息','2016-03-06 11:18:47','2016-03-06 11:18:49'),
(73,3,1,1,'There 12312','12312312','2016-03-11 15:16:58','2016-03-10 16:40:26'),
(78,3,8,2,'PHP password','$pwd = \"123456\";<br>$hash = password_hash($pwd, PASSWORD_DEFAULT);<br>echo $hash;<br>echo \'&lt;br&gt;\';<br>var_dump(password_get_info($hash));<br>echo \'&lt;br&gt;\';<br>var_dump(password_verify($pwd,\'$2y$10$Ok9CRBsYTj/W9nkSMo6QquFP0B4OcO8gpB.58M5n0xxrsGone8IWG\'));<br>if (password_verify($pwd,\'$2y$10$Ok9CRBsYTj/W9nkSMo6QquFP0B4OcO8gpB.58M5n0xxrsGone8IWG\')) { <br>&nbsp;&nbsp;&nbsp; echo \"密码正确\";<br>} else {&nbsp; <br>&nbsp;&nbsp;&nbsp; echo \"密码错误\";<br>}<br>','2016-03-15 11:26:20','2016-03-15 11:23:00'),
(79,3,1,1,'facebook广告','Facebook广告系统相关要解决的问题<br><br>一、号的问题<br>&nbsp;&nbsp; 1、我们主要做个人号（美国号，德国号），企业号（代理号）我们暂时不需要关注<br>&nbsp;&nbsp; 2、养号<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1）ip，useragent，cookie<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2）同一ip底下账号的个数，以及操作的时间间隔或频率<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3）统计各操作的次数及频率，以便知道账号的活跃度，总体的评分，并根据情况调整某些操作的概率<br>&nbsp;● 账号注册时间<br>&nbsp;● 账号注册ip<br>&nbsp;● 登录次数<br>&nbsp;● 在线时长<br>&nbsp;● 好友数量<br>&nbsp;● 群数量<br>&nbsp;● post数量<br>&nbsp;● like数量<br>&nbsp;● 照片数量<br>&nbsp;● 基本资料<br>&nbsp;● 模拟点击的次数<br>&nbsp;● Tag好友数量<br>&nbsp;● 考虑创建page<br>&nbsp;● 考虑人工chat<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4）不要出现非英文广告<br><br>&nbsp;&nbsp; 3、客户端工具的准备，按要求取账号<br><br>二、卡的问题<br>&nbsp;&nbsp; 1、paypal，支付宝，信用卡，借记卡<br>&nbsp;&nbsp; 2、卡本身的风控<br><br>三、仿品的问题<br>&nbsp;&nbsp; 1、链接的问题<br>&nbsp;&nbsp; 2、dns<br>&nbsp;&nbsp; 3、正品站及仿品站的桥接<br>&nbsp;&nbsp; 4、广告文字，图片，广告受众的定位<br><br>四、上号率，站多久，防关联的问题<br>&nbsp;&nbsp; 1、广告方式，上广告的相关技巧或手法，如：先不推仿品，先推page或正品站，验证账号和卡没有问题<br>&nbsp;&nbsp; 2、广告策略，堆号或站很久<br>&nbsp;&nbsp; 3、提额，绑定手机<br>&nbsp;&nbsp; 4、防关联，去雷同，不死的各种方案<br>&nbsp;&nbsp; 5、不同时间段，Facebook卡的东西不同<br>&nbsp;&nbsp; 6、成本及利润，投入及产出比<br><br>五、广告监测及分析<br>&nbsp;&nbsp; 1、看怎么样才能让我们的账号被facebook广告定位到这些仿品广告，然后采集图片，文字，链接<br>&nbsp;&nbsp; 2、分析竞争对手的链接方式，网站，广告图片及文字，以及目前广告是否好上等等<br>&nbsp;&nbsp; 3、了解福来，志勇，Paul等的方式<br><br>六、广告API的研究','0000-00-00 00:00:00','2016-03-16 09:30:25'),
(74,3,1,1,'There ','There are some events triggered on ajax-enabled element (which can also be captured on document level) :\n				<ol><li><code>ajaxloadstart</code> triggered when new content is requested.\n					<br>\n					If you call \"preventDefault()\" on event object, loading will be cancelled</li><li><code>ajaxloaddone</code> is triggered when ajax content is loaded</li><li><code>ajaxpostdone</code> is triggered right after \"ajaxloaddone\" event in POST requests and you can use <code>e.preventDefault()</code> to prevent content area from being updated</li><li><code>ajaxloadcomplete</code> is triggered when ajax content is loaded and inserted into dom</li><li><code>ajaxloaderror</code> is triggered when loading ajax content has failed</li><li><code>ajaxloadlong</code> if <b>max_load_wait</b> is specified, this event is triggered when loading ajax has taken too long</li><li><code>ajaxscriptsloaded</code> is triggered when loading scripts is finished</li></ol>','2016-03-10 16:40:26','2016-03-10 16:40:26'),
(75,3,1,1,'There are some events triggered','There are some events triggered on ajax-enabled element (which can also be captured on document level) :\n				<ol><li><code>ajaxloadstart</code> triggered when new content is requested.\n					<br>\n					If you call \"preventDefault()\" on event object, loading will be cancelled</li><li><code>ajaxloaddone</code> is triggered when ajax content is loaded</li><li><code>ajaxpostdone</code> is triggered right after \"ajaxloaddone\" event in POST requests and you can use <code>e.preventDefault()</code> to prevent content area from being updated</li><li><code>ajaxloadcomplete</code> is triggered when ajax content is loaded and inserted into dom</li><li><code>ajaxloaderror</code> is triggered when loading ajax content has failed</li><li><code>ajaxloadlong</code> if <b>max_load_wait</b> is specified, this event is triggered when loading ajax has taken too long</li><li><code>ajaxscriptsloaded</code> is triggered when loading scripts is finished</li></ol>','2016-03-10 16:40:26','2016-03-10 16:40:26'),
(76,3,1,1,'xxxxx','1<br>123<br>123<br>1<br>231<br>245sdaf<br>asdf<br>as<br>df<br>','2016-03-10 16:40:26','2016-03-10 16:40:26'),
(77,3,3,1,'There  11111','<ol><li>\n					If you call \"preventDefault()\" on event object, loading will be cancelled</li><li><code>ajaxloaddone</code> is triggered when ajax content is loaded</li><li><code>ajaxpostdone</code> is triggered right after \"ajaxloaddone\" event in POST requests and you can use <code>e.preventDefault()</code> to prevent content area from being updated</li><li><code>ajaxloadcomplete</code> is triggered when ajax content is loaded and inserted into dom</li><li><code>ajaxloaderror</code> is triggered when loading ajax content has failed</li><li><code>ajaxloadlong</code> if <b>max_load_wait</b> is specified, this event is triggered when loading ajax has taken too long</li><li><code>ajaxscriptsloaded</code> is triggered when loading scripts is finished</li></ol>','2016-03-15 14:13:34','0000-00-00 00:00:00');

/*Table structure for table `webts_role` */

DROP TABLE IF EXISTS `webts_role`;

CREATE TABLE `webts_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `tarurl` varchar(100) NOT NULL COMMENT '链接',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `webts_role` */

insert  into `webts_role`(`id`,`name`,`tarurl`,`parent_id`) values 
(1,'文章列表','/b/posts',0),
(2,'添加文章','/b/savePost',0),
(3,'添加文章','/b/doSavePost',2),
(4,'三级控一','/ssewx/serxde/serwcx',0),
(5,'sdfxe','/s',0),
(6,'用户管理','/user/ulist',0),
(7,'','/user/updateRole',0),
(8,'控制面板','/b/index',0);

/*Table structure for table `webts_user` */

DROP TABLE IF EXISTS `webts_user`;

CREATE TABLE `webts_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(32) DEFAULT NULL,
  `email` varchar(96) NOT NULL,
  `code` varchar(40) DEFAULT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ONELY` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `webts_user` */

insert  into `webts_user`(`user_id`,`user_role_id`,`username`,`password`,`salt`,`firstname`,`lastname`,`email`,`code`,`ip`,`status`,`date_added`) values 
(1,1,'webts','U3nmQ0EH6O2p0YS2Fui5RNiyMSoFrI/thTsgZ0STGFdY/k6URosKIk1onjJkTJWqDbWEkrMeccHyc8MtoVHcUQ==','ACjJiSZFm',NULL,NULL,'duguhu007@qq.com',NULL,'127.0.0.1',1,'2014-02-15 16:38:55'),
(2,2,'demo','ik/RKLLpTKY+4Xqyhb20o5j7JnEsHbKIvWniwlUKN7/E+BYPaYt14S/Zn5IiVqAQEeUbrM58y9Jj8n6i28IW+A==','2uIX3M3Xy',NULL,NULL,'',NULL,'',0,'2016-03-08 16:25:31'),
(3,1,'admin','ik/RKLLpTKY+4Xqyhb20o5j7JnEsHbKIvWniwlUKN7/E+BYPaYt14S/Zn5IiVqAQEeUbrM58y9Jj8n6i28IW+A==','2uIX3M3Xy',NULL,NULL,'admin@jios.com',NULL,'127.0.0.1',1,'2016-03-09 21:45:54');

/*Table structure for table `webts_user_role` */

DROP TABLE IF EXISTS `webts_user_role`;

CREATE TABLE `webts_user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `webts_user_role` */

insert  into `webts_user_role`(`user_role_id`,`name`,`permission`) values 
(1,'webts','[\"\\/abcd\\/a\",\"\\/abcd\\/b\",\"\\/ssce\\/e\",\"\\/ssewx\\/serxde\\/serwcx\",\"\\/s\",\"\\/user\\/ulist\",\"\\/user\\/updateRole\"]'),
(2,'user','[\"\\/b\\/posts\",\"\\/b\\/savePost\",\"\\/b\\/doSavePost\",\"\\/ssewx\\/serxde\\/serwcx\",\"\\/s\",\"\\/user\\/ulist\",\"\\/user\\/updateRole\",\"\\/b\\/index\"]');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
