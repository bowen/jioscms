<?php
/**
 * 简单的通用的model方法
 * @author Bowen
 *
 */
class Public_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	 /**
	  * 通用的删除
	  * @param unknown_type $tabname
	  * @param array('id'=>1) $where
	  * @return boolean
	  */
	 function del_pub($tabname,$where=array())
	 {
		if($this->db->delete($tabname,$where))
		{
			return true;
		}
		return false;
	 }
	 /**
	  * 通用的增加 可以一次加多个
	  * @param unknown_type $tabname
	  * @param unknown_type $data = array($user1=>array(),$user2=>array());
	  * @return boolean
	  */
	 function add_pub($tabname,$data=array())
	 {
		if($this->db->insert_batch($tabname, $data))
		{
			return true;
		}
		return false;
	 }

	 /**
	  * 添加后返回id
	  * @param unknown_type $tabname
	  * @param $user=>array() $data
	  * @return boolean
	  */
	 function add_pub_re_id($tabname,$data=array())
	 {
		if($this->db->insert($tabname, $data))
		{
			return $this->db->insert_id();
		}
		return false;
	 }

	 /**
	  * 通用的更新
	  * $tabname
	  * array('id'=>1) $data 需要更新的列
	  * array('id'=>1) $where 条件
	  */
	 function update_pub($tabname,$data=array(),$where=array())
	 {
		if($this->db->update($tabname, $data, $where))
		{
			return true;
		}
		return false;
	 }
	
	/**
	 *通用的单表list
	 * @param unknown_type $tabname
	 * @param array('id'=>1) $where
	 * @param id desc $order_by
	 * @param 每次查询15 $rows
	 * @param 页数 $pagenum
	 * @return array()
	 */
	function list_pub($tabname,$where=null,$order_by=null,$rows=null,$pagenum=null)
	{
		
		$this->db->where($where);
		if(!empty($order_by))
		{
			$this->db->order_by($order_by);
		}
		if(!empty($rows)&&!empty($pagenum))
		{
			if($pagenum==1)
			{
				$this->db->limit($rows);
			}else{
				$pagenum = ($pagenum-1)*$rows;
				$this->db->limit($pagenum,$rows);
			}
		}
		if(!empty($rows)&&empty($pagenum))
		{
			$this->db->limit($rows);
		}
		$query = $this->db->get($tabname);
		$result =$query->result();
		return $result;
	}
	
	/**
	 * 新的列表查询方法
	 * @param String $tabname
	 * @param array $where eg(array($param,$param2)) $param = new stdClass(); $param->key = 'id';$param->val = '1'; $param->type='LIKE' or 'EQ';default EQ
	 * @param string $order_by eg('title desc, name asc')
	 * @param array $limit eg(array('rows'=>10,'pagenum'=>1))
	 * @return array
	 */
	function new_list_pub($tabname,$where=null,$order_by=null,$rows=null,$pagenum=null)
	{
		if(!empty($where))
		{
			foreach ($where as $key=>$val)
			{
				if(isset($val->type)&&'LIKE'==$val->type)
				{
					$this->db->like($val->key,$val->val);
				}else
				{
					$this->db->where($val->key,$val->val);
				}
			}
		}
		if(!empty($order_by))
		{
			$this->db->order_by($order_by);
		}
		if(!empty($rows)&&!empty($pagenum))
		{
			if($pagenum==1)
			{
				$this->db->limit($rows);
			}else{
				$pagenum = ($pagenum-1)*$rows;
				$this->db->limit($pagenum,$rows);
			}
		}
		if(!empty($rows)&&empty($pagenum))
		{
			$this->db->limit($rows);
		}
		$query = $this->db->get($tabname);
// 		$sql = $this->db->last_query();//显示sql语句
		$result =$query->result();
		return $result;
	}
	function new_list_count_pub($tabname,$where=null)
	{
		if(!empty($where))
		{
			foreach ($where as $key=>$val)
			{
				if(isset($val->type)&&'LIKE'==$val->type)
				{
					$this->db->like($val->key,$val->val);
				}else
				{
					$this->db->where($val->key,$val->val);
				}
			}
		}
		$this->db->from($tabname);
		return $this->db->count_all_results();
	}
	/**
	 * 通用的获取总数(分页的时候需要的数据)
	 * @param unknown_type $tabname
	 * @param array('id'=>'=1') $where
	 */
	function list_count_pub($tabname,$where=null)
	{
		$this->db->where($where);
		$this->db->from($tabname);
		return $this->db->count_all_results();
	}
	
	function page_pub($tabname,$where=null,$order_by=null,$rows=10,$pagenum=1)
	{
		$getArr = $this->input->get();
		$page = new stdClass();
		$page->perpage = $rows;//每页条数
		$page->nowindex = $pagenum;//当前页
		$pageUrl = current_url();
		$pageParam = null;
		if($getArr)
		{
			foreach ($getArr as $key=>$val)
			{
				$param = new stdClass();
				switch ($key)
				{
					case 'page':
						$page->nowindex = $val;
						break;
					case 'row':
						$page->perpage = $val;
						break;
					default:
						$pageParam .= '&'.$key.'='.$val;
						$param->key = $key;
						$param->val = $val;
						$param->type = 'LIKE';
						break;
				}
				if(!empty($param->key))
				{
					array_push($where,$param);
				}
			}
		}
		$list = $this->new_list_pub($tabname,$where,$order_by,$page->perpage,$page->nowindex);
		$page->total = $this->new_list_count_pub($tabname,$where);//总条数
		$page->urlParam = $pageParam;
		$page->url = $pageUrl;
		$page->content = $list;
		$data['page'] = $page;
		return $data;
	}
	
	/**
	 * 通用的单个查询
	 * @param unknown_type $tabname
	 * @param unknown_type $where array('id'=>1,'name'=>'jhion')
	 * @return unknown
	 */
	 function detail_pub($tabname,$where)
	 {
		$this->db->where($where);
		$query = $this->db->get($tabname);
		$result =$query->row();
		return $result;
	 }
	 
	 /**
	  * 查询最后一个
	  * @param unknown_type $tabname
	  * @param unknown_type $where
	  * @param unknown_type $order_by
	  * @return unknown
	  */
	 function detail_pub_finally($tabname,$where,$order_by=null)
	 {
	 	$sql = "SELECT * from ".$tabname." where 1=1";
		if(!empty($where))
		{
			foreach($where as $key=>$val)
	 		{
				$sql.= " AND ".$key." ".$val;
	 		}
		}
		if(!empty($order_by))
	 	{
	 		$sql.= " order by ".$order_by;
	 	}
	 	$sql .= " limit 1";
	 	$query = $this->db->query($sql);
		$result =$query->row();
		return $result;
	 }

	 /**
	  * 获取ID(1,2,3,4)格式
	  * @param unknown_type $tabname
	  * @param unknown_type $where
	  * @param unknown_type $obj
	  * @return unknown
	  */
	 function get_group_concat_obj_pub($tabname,$where,$obj='id')
	 {
	 	$sql = "SELECT GROUP_CONCAT(".$obj.") as obj from ".$tabname." where 1=1";
	 	if(!empty($where))
	 	{
	 		foreach($where as $key=>$val)
	 		{
	 			$sql.= " AND ".$key." ".$val;
	 		}
	 	}
	 	$query = $this->db->query($sql);
		$result =$query->row();
		return $result->obj;
	 }
}
?>