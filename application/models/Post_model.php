<?php
class Post_model extends CI_Model{
	function getPostByid($id,$userid=null)
	{
		$where = array('id'=>$id);
		if(!empty($userid))
		{
			$where['user_id'] = $userid;
		}
		$post = $this->db->select('*')->where($where)->get(TAB_POSTS)->row();
		return $post;
	}
	/**
	 * 获取某个子集的所有文章
	 * @param unknown $menuid
	 * @return unknown
	 */
	function getListByMenuid($menuid)
	{
	    $where = array('menu_id'=>$menuid);
	    $list = $this->db->select('p.*,m.name \'menuname\',m.`diy_url`')
	            ->from(TAB_POSTS .' p')
	            ->join(TAB_MENU.' m','m.id=p.`menu_id`','left')
	            ->where($where)
	            ->get()
	            ->result();
	    return $list;
	}
}