<?php

class DingTalk_model extends CI_Model
{
    const SUITE_KEY = 'suiterjcdkrcghch6pqb7';
    const SUITE_SECRET = 'qpEFpPdHQvgD3SjPeEX5qbmJVBrm4MAHdzO7TFQppCmNfiyiG3a5qu7FIjuDuKhG';
    const ENCODING_AES_KEY = 'etfxhvzog0gj7h1xel6sh5tgau443ifzw1mt6iahcnx';
    const TOKEN = 'webts';
    function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }
    //该API用于获取应用套件令牌（suite_access_token）
    function get_suit_token()
    {
        $url = 'https://oapi.dingtalk.com/service/get_suite_token';
        $post = array();
        $post['suite_key'] = self::SUITE_KEY;
        $post['suite_secret'] = self::SUITE_SECRET;
        $post['suite_ticket'] = "WEv0CkOEz0YNxGUj71wltFxVK4CI9i4ncYE2w6JGpINGpOlL6YXns5PWK0yCtgl3ymq13WAorDOySky1bjBsed";//这个是钉钉推送的 通过ding.6-mi.com API获取吧
        $resJson = $this->curlDingTalkApi($url, $post);
        $res = json_decode($resJson);
        return $res->suite_access_token;
    }
    //获取企业授权的access_token
    function get_access_token()
    {
        $suite_access_token = $this->get_suit_token();
        $url = 'https://oapi.dingtalk.com/service/get_corp_token?suite_access_token='.$suite_access_token;
        $post = array();
        $post['auth_corpid'] = 'ding1b2228b35a2cf46335c2f4657eb6378f';//授权方corpid
        $post['permanent_code'] = 'WavFPMitG_nClfpPu65lhjrZ0kk6nwrjHOMV4Z16sC7tVuJj5PhnViigZ9hxLS9w';//永久授权码，通过get_permanent_code获取
        $resJson = $this->curlDingTalkApi($url, $post);
        $res = json_decode($resJson);
//         print_r($res);
        return $res->access_token;
    }
    //获取企业授权的授权数据 服务端
    function get_auth_info()
    {
        $suite_access_token = $this->get_suit_token();
        $url = 'https://oapi.dingtalk.com/service/get_auth_info?suite_access_token='.$suite_access_token;
        $post = array();
        $post['suite_key'] = self::SUITE_KEY;//	应用套件key
        $post['auth_corpid'] = 'ding1b2228b35a2cf46335c2f4657eb6378f';//	授权方corpid
        $post['permanent_code'] = 'WavFPMitG_nClfpPu65lhjrZ0kk6nwrjHOMV4Z16sC7tVuJj5PhnViigZ9hxLS9w';//永久授权码，通过get_permanent_code获取
        $resJson = $this->curlDingTalkApi($url, $post);
        $res = json_decode($resJson);
        print_r($res);
    }
    
    function gettotoken()
    {
        $url = 'https://oapi.dingtalk.com/sso/gettoken?corpid=id&corpsecret=ssosecret';
    }
    
    function curlDingTalkApi($url, $post)
    {
        $this->curl->create($url);
        $options = array();
        $options[CURLOPT_TIMEOUT] = 10;
//         $options[CURLOPT_HEADER] = true;
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_SSL_VERIFYHOST] = false;
        $options[CURLOPT_SSL_VERIFYPEER] = false;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_MAXREDIRS] = 10;
        $options[CURLOPT_HTTPHEADER] = array('Content-Type: application/json');
        $this->curl->options($options);
        $this->curl->post(json_encode($post));
        $res = $this->curl->execute();
//         print_r($res);exit;
        return $res;
        // print_r($this->curl->info);
    }
}