<?php
class User_Model extends  CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
    function getUserByid($uid)
    {
		$where = array('user_id'=>$uid);
		$this->db->where($where);
		$query = $this->db->get('webts_user');
		$user =$query->row();
		if(!empty($user))
		{
			$role = $this->db->select('*')->where('user_role_id', $user->user_role_id)->get(TAB_USER_ROLE)->row();
			if(!empty($role))
			{
				$role->permission = json_decode($role->permission);
				$user->role = $role;
			}
		}
        return $user;        
    }
    function saveRole($role)
    {
        $reid = $this->public_model->add_pub_re_id(TAB_ROLE,$role);
        return $reid;
    }
    function getRoleByid($user_role_id)
    {
    	$role = $this->db->select('*')->where('user_role_id', $user_role_id)->get(TAB_USER_ROLE)->row();
    	if(!empty($role))
    	{
    		$role->permission = json_decode($role->permission);
    	}
    	return $role;
    }
	
	function getAllRole()
	{
		$roles = $this->db->select('*')->get('webts_role')->result();
		return $roles;
	}
	
	function updateUserRole($data=array(),$where=array())
	{
		if($this->db->update('webts_user_role', $data, $where))
		{
			return true;
		}
		return false;
	}
}