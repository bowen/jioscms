<?php
class Menu_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function get_all_menu(){
		$sql = "SELECT * FROM ".TAB_MENU." WHERE parent_id=0 order by sort_order asc";
		$query = $this->db->query($sql);
		$types = $query->result();
		//		print_r($types);exit;
		foreach($types as $type){
			$type_id = $type->id;
			$this->get_menu_sub($type,$type_id);
		}
		return $types;
	}
	function get_menu_sub($type,$type_id){
		$sql = "SELECT * FROM ".TAB_MENU." WHERE parent_id=$type_id order by sort_order asc";
		$query = $this->db->query($sql);
		$subs = $query->result();
		if(!empty($subs)){
			$type->sub = $subs;
			foreach($subs as $v){
				$type_id = $v->id;
				$this->get_menu_sub($v,$type_id);
			}
		}
	}
	
	function get_munu_by_id($menuid)
	{
		
	}
}