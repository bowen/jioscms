<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('get_date_now'))
{
	/**
	 * 获取当前服务器时区的当前时间
	 */
	function get_date_now()
	{
		return date("Y-m-d H:i:s");
	}
}