<?php
/**
 * 这个是cms的后台控制器
 * @author bowen
 *
 */
class B extends MY_Controller{
	function __construct(){
		parent::__construct('admin');
		$this->load->model('menu_model');
		$this->load->model('public_model');
		$this->load->model('post_model');
		$this->load->helper('pub');
	}
	function index()
	{
		$data = $this->_pub_data();
		$this->load->view('admin/ace',$data);
	}
	function home()
	{
// 		$this->load->view('admin/ace/index');
		$this->posts();
	}
	function s()
	{
		echo get_date_now();
	}
	function blank()
	{
		$this->load->view('admin/ace/blank');
	}
	function ajax($c='content',$s=null){
		echo 'c content';
	}
	public function _remap($method, $params = array())
	{
	    if (method_exists($this, $method))
	    {
	        return call_user_func_array(array($this, $method), $params);
	    }else {
// 	    	$this->load->view('admin/ace/'.$method);
	    	$this->__show(array(), $method);
	    }
	}
	/**
	 * 文章列表
	 */
	function posts()
	{
		$data = $this->_pub_data();
		$data['title'] = '文章管理 - Bowen Admin';
		$whereparam = new stdClass(); 
		$whereparam->key = 'user_id';
		$whereparam->val =  $data['user']->user_id;
		$posts = $this->public_model->new_list_pub(TAB_POSTS,array($whereparam),"update_date desc");
		$data['list'] = $posts;
		$this->__show($data, 'posts');
	}
	function savePost($id=null)
	{
		$data = $this->_pub_data();
		if(empty($id))
		{
			$data['title'] = '添加文章 - Bowen Admin';
		}else 
		{
			$data['title'] = '修改文章 - Bowen Admin';
			$post = $this->post_model->getPostByid($id,$data['user']->user_id);
// 			print_r($post);
			$data['post'] = $post;
		}
		$menus = $this->menu_model->get_all_menu();
		$treeNode = json_encode($this->_ztree($menus));
		$data['treeNode'] = $treeNode;
// 		$this->_log($data);
		$this->__show($data, 'save-post');
	}
	
	function doSavePost()
	{
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$postid = $this->input->post('postid');
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$saveData = array();
			$saveData['menu_id'] = $this->input->post('menu_column_id');
			$saveData['type_id'] = $this->input->post('type_id');
			$saveData['title'] = $title;
			$saveData['content'] = $content;
			//postid没有值 
			if($postid==="")
			{
				$saveData['user_id'] = $data['user']->user_id;
				$saveData['create_date'] = get_date_now();
				$reid = $this->public_model->add_pub_re_id(TAB_POSTS,$saveData);
				if($reid)
				{
					$obj['type'] = true;
					$obj['url'] = '#page/posts';
				}
			}else 
			{
				$saveData['update_date'] = get_date_now();
				$res = $this->public_model->update_pub(TAB_POSTS,$saveData,array('id'=>$postid,'user_id'=>$data['user']->user_id));
				if($res)
				{
					$obj['type'] = true;
					$obj['url'] = '#page/posts';
				}
			}
		}
		echo json_encode($obj);
	}
	public function types()
	{
		$data = array();
		$data['title'] = '分类管理 - Bowen Admin';
		$types = $this->menu_model->get_all_menu();
		$data['menus'] = $types;
		$html = '<ol class="dd-list">';
		foreach($types as $v){
			$html .='<li class="dd-item" data-id="'.$v->id.'"><div class="dd-handle">'.$v->name.'</div>';
// 			print_r($v);exit;
			if(isset($v->sub))
			{
				$html .= $this->arr_foreach_sub($v);
			}		
			$html .='</li>';
		}
		$html .='</ol>';
		$data['html'] = $html;
// 		print_r(json_encode($data['menus']));exit;
// 		echo $html;exit;
		$this->__show($data,'types');
	}
	
	/**
	 *权限管理
	 *
	 *
	 */
	function role()
	{
		$data = $this->_pub_data();
		if($data['user']->role->name!='webts')
		{
			show_404();
		}
		$data['title'] = '权限管理 - Bowen Admin';
		$roles = $this->user_model->getAllRole();
		$this->log($roles,false);
		$data['list'] = $roles;
		$this->__show($data,'role');		
	}
	function doSaveRole()
	{
	    $data = $this->_pub_data();
	    $obj = array();
	    if($this->input->is_ajax_request())
	    {
// 	        print_r($this->input->post());exit;
	        $rolename = $this->input->post('rolename');
	        $tarurl = $this->input->post('tarurl');
	        $role = array('name'=>$rolename,'tarurl'=>$tarurl,'parent_id'=>'0');
	        $res = $this->user_model->saveRole($role);
	        if($res)
	        {
    	        $obj['status'] = true;
    	        $obj['url'] = '#page/role';
	        }else 
	        {
	            $obj['status'] = false;
	            $obj['msg'] = '添加失败';
	        }
	    }
	    echo json_encode($obj);
	}
	/**
	 * type 树的列表
	 * @param unknown $arr
	 */
	function arr_foreach_sub($arr)
	{
	    if ((!isset($arr->sub))||(!is_array ($arr->sub)))
	    {
	        return false;
	    }
	    $html ='<ol class="dd-list">';
	    foreach ($arr->sub as $val)
	    {
	        	
	        // print_r($val);exit;
	        $html .='<li class="dd-item" data-id="'.$val->id.'"><div class="dd-handle">'.$val->name.'</div>';
	        if (isset($val->sub)&&is_array ($val->sub))
	        {
	            $html .= $this->arr_foreach_sub ($val,$html);
	        }
	        $html .='</li>';
	    }
	    $html .='</ol>';
	    return $html;
	}
	
	function saveType($id=null)
	{
		$data = $this->_pub_data();
		if(empty($id))
		{
			$data['title'] = '添加分类 - Bowen Admin';
		}else
		{
			$data['title'] = '修改分类 - Bowen Admin';
			$post = $this->post_model->getPostByid($id,$data['user']->user_id);
			// 			print_r($post);
			$data['post'] = $post;
		}
		$menus = $this->menu_model->get_all_menu();
		$treeNode = json_encode($this->_ztree($menus));
		$data['treeNode'] = $treeNode;
		$this->__show($data, 'save-type');
	}

	function doSaveType()
	{
		
	}
	
	function do_save_type()
	{
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$content = $this->input->post('content');
			$tarr = json_decode($content);
			print_r($tarr);
		}
	}
	
	//用来调试的方法
	function _log($obj,$exit=true)
	{
	    print_r($obj);
	    if($exit)
	        exit;
	}
	/**
	 * 显示调用这个方法
	 * @param unknown $data
	 * @param unknown $theme
	 */
	function __show($data,$theme)
	{
		$this->load->view(ADMINTHEME.$theme,$data);
	}
	
	/**
	 * 生成树需要的数据
	 * @param unknown $arr
	 * @return multitype:
	 */
	function _ztree($arr)
	{
	    $tree = array();
	    foreach ($arr as $key=>$val)
	    {
	        $obj = new stdClass();
	        $obj->id = $val->id;
	        $obj->name = $val->name;
	        $obj->pid = $val->parent_id;
	        if(isset($val->sub)&&!empty($val->sub))
	        {
	            $obj->open = true;
	            $obj->children = $this->_ztree2($val->sub);
	        }
	        array_push($tree, $obj);
	    }
        return $tree;
	}
	/**
	 * 用来递归的---->_ztree
	 * @param unknown $val
	 */
	function _ztree2($val)
	{
	    $arr = array();
	    foreach ($val as $k=>$v)
	    {
	        $obj = new stdClass();
	        $obj->id = $v->id;
	        $obj->name = $v->name;
	        $obj->pid = $v->parent_id;
	        if(isset($v->sub)&&!empty($v->sub))
	        {
	            $obj->open = true;
	            $obj->children = $this->_ztree2($v->sub);
	        }
	        array_push($arr, $obj);
	    }
	    return $arr;
	}
	
	function doSave()
	{
		$name =$this->input->post('name');
		$parent_id = $this->input->post('parent_id');
		$sort_order = $this->input->post('sort_order');
		$is_show = $this->input->post('is_show');
	}
	
	function c()
	{
		print_r($this->router);
	}
}