<?php
/**
 * 这个是cms的前台
 * @author bowen
 *
 */
class A extends MY_Controller{
	function __construct(){
		parent::__construct('cms');
		$this->load->model('menu_model');
		$this->load->model('public_model');
		$this->load->model('post_model');
		$this->load->helper('pub');
	}
	function article($id=null)
	{
		$post = $this->post_model->getPostByid($id);
		if(empty($post))
		    show_404();
// 		print_r($post);exit;
		$data['post'] = $post;
		$this->__show($data, 'article');
	}
	
	function blog($a1,$a2,$a3)
	{
	    echo $a1.'+'.$a2.'+'.$a3;
	    print_r($this->input->get());
	}
	function menus()
	{
	    $data = array();
	    $data['title'] = '分类管理 - Bowen Admin';
	    $types = $this->menu_model->get_all_menu();
	    $data['menus'] = $types;
	    $html = '<ol class="dd-list">';
	    foreach($types as $v){
	        $html .='<li class="dd-item" data-id="'.$v->id.'"><div class="dd-handle">'.$v->name.'</div>';
	        // 			print_r($v);exit;
	        if(isset($v->sub))
	        {
	            $html .= $this->arr_foreach_sub($v);
	        }
	        $html .='</li>';
	    }
	    $html .='</ol>';
	    $data['html'] = $html;
	    // 		print_r(json_encode($data['menus']));exit;
	    // 		echo $html;exit;
	    $this->__show($data,'types');
	}
	function menu($id)
	{
	   $list = $this->post_model->getListByMenuid($id);
	   print_r($list);
	}
	/**
	 * 重新映射方法
	 * @param unknown $method
	 * @param array $params
	 */
    public function _remap($method, $params = array())
    {
//         print_r($method);
//         print_r($params);
//         exit;
        if (method_exists($this, $method))
        {
            return call_user_func_array(array($this, $method), $params);
        }else 
        {
            
        }
        show_404();
    }
	
	function __show($data,$theme)
	{
// 		echo THEME.$theme;exit;
// 		$this->load->view(THEME.'common/header',$data);
// 		$this->load->view(THEME.$theme);
		$this->load->view(THEME.$theme,$data);
	}
}