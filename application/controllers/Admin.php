<?php
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
	/**
	 * 公共数据
	 * @return unknown
	 */
	function _pub_data()
	{
		$data['user'] = $this->session->userdata('user');
		if(!$data)
		{
			redirect("/admin/login");
		}
		return $data;
	}
	
	function index()
	{
		$data = $this->_pub_data();
		$data['title'] = 'Dashboard - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		redirect("/admin/notepads");
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."index",$data);
		$this->load->view(WEBTSTHEME."common/footer",$data);
	}
	function login()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Login Page - Bowen Admin';
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."login",$data);
		//$this->load->view(WEBTSTHEME."common/footer",$data);
	}
	
	/**
	 * 处理登录
	 */
	function dologin()
	{
		//print_r($this->input->post());
		//先判断是否是ajax请求
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$user = $this->public_model->detail_pub(USER,array("username"=>$username));
			//如果当前用户不存在
			if(empty($user))
			{
				$obj['type'] = false;
				$obj['url'] = '/admin/login.html';
			}else 
			{
				$savepass = $this->encrypt->decode($user->password,$user->salt);
				if($password==$savepass)
				{
					$user_group = $this->public_model->detail_pub(USER_GROUP,array("user_group_id"=>$user->user_group_id));
					$sessionUser = new stdClass();
					$sessionUser->user_id = $user->user_id;
					$sessionUser->username = $user->username;
					$sessionUser->user_group_id = $user->user_group_id;
					$sessionUser->firstname = $user->firstname;
					$sessionUser->lastname = $user->lastname;
					$sessionUser->email = $user->email;
					$sessionUser->ip = $user->ip;
					$sessionUser->status = $user->status;
					$sessionUser->salt = $user->salt;
					$sessionUser->code = $user->code;
					$sessionUser->role = $user_group->name;
					$this->session->set_userdata('user',$sessionUser);
					$obj['type'] = true;
					$url = '/admin/index.html';
					//判断是否有来源
					if($this->session->userdata('logreferer'))
					{
						if(!strpos($this->session->userdata('logreferer'),'logout'))
						{
							$url = $this->session->userdata('logreferer');
						}
					}
					$obj['url'] = $url;
				}else 
				{
					$obj['type'] = false;
					$obj['url'] = '/admin/login.html';
				}
			}
		}
		echo json_encode($obj);
	}
	
	/**
	 * 退出
	 */
	function logout()
	{
		$this->session->sess_destroy();
		redirect("/admin/login");
	}
	
	/**
	 * 注册帐号
	 */
	function doregister()
	{
		echo false;exit;
		$obj = null;
		//先判断是否是ajax请求
		if($this->input->is_ajax_request())
		{
			$obj = $this->input->post();
			$uname = $this->input->post("regUname");
			$upwd = $this->input->post("regPwd");
			$uemail = $this->input->post("regEmail");
			$salt = substr($this->encrypt->encode(time()), 0,9);//9位数的随机key
			$user['user_group_id'] = 1;
			$user['username'] = $uname;
			$user['password'] = $this->encrypt->encode($upwd,$salt);
			$user['salt'] = $salt;
			$user['email'] = $uemail;
			$user['ip'] = $this->session->userdata('ip_address');
			$user['status'] = 1;
			$type = $this->public_model->add_pub(USER,array($user));
			if($type)
			{
				$user_group = $this->public_model->detail_pub(USER_GROUP,array("user_group_id"=>$user['user_group_id']));
				$this->session->set_userdata('USER',$user_group->name);
			}
			$obj['type'] = $type;
			$obj['url'] = '/admin/index.html';
		}
		echo json_encode($obj);
	}
	
	//排版
	function typography()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Typography - Bowen Admin';
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."typography",$data);
	}
	
	
	/**
	 * 记事本
	 */
	function notepads()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Notepads - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		
		//得到所有的get参数
		$getArr = $this->input->get();
		$page = new stdClass();
		$page->perpage = 9;//每页条数
		$page->nowindex = 1;//当前页
		
		$pageUrl = current_url();
		$pageParam = null;
		$searchParam = array();
		if($getArr)
		{
			foreach ($getArr as $key=>$val)
			{
				switch ($key)
				{
					case 'page':
						$page->nowindex = $val;
						break;
					case 'row':
						$page->perpage = $val;
						break;
					default:
						$pageParam .= '&'.$key.'='.$val;
						$searchParam[$key]="like '%".$val."%'";
						break;
				}
			}
		}
		$page->total = $this->public_model->list_count_pub(NOTEPADS,$searchParam);//总条数
		$page->urlParam = $pageParam;
		$page->url = $pageUrl;
		$data['page'] = $page;
		
// 		print_r($data['page']);
		$listresult = $this->public_model->list_pub(NOTEPADS,$searchParam," update_date desc",$page->perpage,$page->nowindex);
		$list = array();
		foreach ($listresult as $key=>$val)
		{
			$note = new stdClass();
			$note->id = $val->id;
			//echo $val->title;exit;
			$note->title = $val->title;
			$note->safety = $val->safety;
			$note->update_date = $val->update_date;
			$list[$key] = $note;
		}
		$data['list'] = $list;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."notepadlist",$data);
	}
	/**
	 * 根据id获取一个详细的notepad
	 * @param unknown $id
	 */
	function getNotepad($id=null)
	{
		
		if(empty($id))
		{
			show_error("错误的请求!!!!");
		}
		$data = $this->_pub_data();
		$data['title'] = 'Notepad - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$note = $this->public_model->detail_pub(NOTEPADS,array("id"=>$id));
		$data['note'] = $note;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."notepad",$data);
	}
	/**
	 * 解密操作
	 */
	function webtsNotepad()
	{
		
		$obj = null;
		$data = $this->_pub_data();
		if($this->input->is_ajax_request())
		{
			$note_id = $this->input->post("id");
			$notepad = $this->public_model->detail_pub(NOTEPADS,array("id"=>$note_id));
			if($notepad->safety)
			{	
				$obj['type'] = true;
				$obj['content'] = $this->encrypt->decode($notepad->content,$data['user']->salt);
			}
		}
		echo json_encode($obj);
	}
	
	/**
	 * 添加notepad
	 */
	function addNotepad()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Notepad - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."add_notepad",$data);
	}
	
	function doAddNotepad()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$safety = $this->input->post('safety');
			if($safety)
			{
				$content = $this->encrypt->encode($content,$data['user']->salt);
			}
			$reid = $this->public_model->add_pub_re_id(NOTEPADS,array('title'=>$title,'content'=>$content,'safety'=>$safety,'user_id'=>$data['user']->user_id));
			if($reid)
			{
				$obj['type'] = true;
				$obj['url'] = '/admin/notepads.html';
			}
		}
		echo json_encode($obj);
	}
	
	function updateNotepad($id=null)
	{
		
		if(empty($id))
		{
			show_error("错误的请求!!!!");
		}
		$data = $this->_pub_data();
		$data['title'] = 'Notepad - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$note = $this->public_model->detail_pub(NOTEPADS,array("id"=>$id));
		if($note->safety)
		{
			$note->content = $this->encrypt->decode($note->content,$data['user']->salt);
		}
		$data['note'] = $note;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."add_notepad",$data);
	}
	
	function doUpdateNotepad()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$note_id = $this->input->post('note_id');
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$safety = $this->input->post('safety');
			if($safety)
			{
				$content = $this->encrypt->encode($content,$data['user']->salt);
			}
			$reid = $this->public_model->update_pub(NOTEPADS,array('title'=>$title,'content'=>$content,'safety'=>$safety),array("id"=>$note_id));
			if($reid)
			{
				$obj['type'] = true;
				$obj['url'] = '/admin/notepads.html';
			}
		}
		echo json_encode($obj);
	}
	/**
	 * 快捷锁定or解锁
	 */
	function doUpdateNotepadStatus()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$note_id = $this->input->post('note_id');
			$safety = $this->input->post('safety');
			$note = $this->public_model->detail_pub(NOTEPADS,array("id"=>$note_id));
			if($safety)
			{
				$content = $this->encrypt->encode($note->content,$data['user']->salt);
			}else 
			{
				$content = $this->encrypt->decode($note->content,$data['user']->salt);
			}
			$reid = $this->public_model->update_pub(NOTEPADS,array('content'=>$content,'safety'=>$safety),array("id"=>$note_id));
			if($reid)
			{
				$obj['type'] = true;
			}
		}
		echo json_encode($obj);
	}
	
	/**
	 * 删除notepad
	 */
	function delNotepad()
	{
		
		$obj = null;
		$obj['type'] = true;
		$data = $this->_pub_data();
		if($this->input->is_ajax_request())
		{
			$noteid = $this->input->post("id");
			$obj['type'] = $this->public_model->del_pub(NOTEPADS,array('id'=>$noteid));
		}
		echo json_encode($obj);
	}
	
	/**
	 * 博客
	 */
	function blog()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Blog - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		
		//得到所有的get参数
		$getArr = $this->input->get();
		$page = new stdClass();
		$page->perpage = 9;//每页条数
		$page->nowindex = 1;//当前页
		
		$pageUrl = current_url();
		$pageParam = null;
		$searchParam = array();
		if($getArr)
		{
			foreach ($getArr as $key=>$val)
			{
				switch ($key)
				{
					case 'page':
						$page->nowindex = $val;
						break;
					case 'row':
						$page->perpage = $val;
						break;
					default:
						$pageParam .= '&'.$key.'='.$val;
						$searchParam[$key]="like '%".$val."%'";
						break;
				}
			}
		}
		$page->total = $this->public_model->list_count_pub(BLOG,$searchParam);//总条数
		$page->urlParam = $pageParam;
		$page->url = $pageUrl;
		$data['page'] = $page;
		
// 		print_r($data['page']);
		$listresult = $this->public_model->list_pub(BLOG,$searchParam," update_date desc",$page->perpage,$page->nowindex);
		$list = array();
		foreach ($listresult as $key=>$val)
		{
			$blog = new stdClass();
			$blog->id = $val->id;
			//echo $val->title;exit;
			$blog->title = $val->title;
			$blog->safety = $val->safety;
			$blog->update_date = $val->update_date;
			$list[$key] = $blog;
		}
		$data['list'] = $list;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."bloglist",$data);
	}
	/**
	 * 根据id获取一个详细的blog
	 * @param unknown $id
	 */
	function getBlog($id=null)
	{
		
		if(empty($id))
		{
			show_error("错误的请求!!!!");
		}
		$data = $this->_pub_data();
		$data['title'] = 'Blog - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$blog = $this->public_model->detail_pub(BLOG,array("id"=>$id));
		$data['blog'] = $blog;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."blog",$data);
	}
	
	/**
	 * 添加blog
	 */
	function addBlog()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Blog - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."add_blog",$data);
	}
	
	function doAddBlog()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$safety = $this->input->post('safety');
			if($safety)
			{
				$content = $this->encrypt->encode($content,$data['user']->salt);
			}
			$reid = $this->public_model->add_pub_re_id(BLOG,array('title'=>$title,'content'=>$content,'safety'=>$safety,'user_id'=>$data['user']->user_id));
			if($reid)
			{
				$obj['type'] = true;
				$obj['url'] = '/admin/blog.html';
			}
		}
		echo json_encode($obj);
	}
	
	function updateBlog($id=null)
	{
		
		if(empty($id))
		{
			show_error("错误的请求!!!!");
		}
		$data = $this->_pub_data();
		$data['title'] = 'Blog - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$blog = $this->public_model->detail_pub(BLOG,array("id"=>$id));
		if($blog->safety)
		{
			$blog->content = $this->encrypt->decode($blog->content,$data['user']->salt);
		}
		$data['blog'] = $blog;
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."add_blog",$data);
	}
	
	function doUpdateBlog()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$blog_id = $this->input->post('blog_id');
			$title = $this->input->post('title');
			$content = $this->input->post('content');
			$safety = $this->input->post('safety');
			if($safety)
			{
				$content = $this->encrypt->encode($content,$data['user']->salt);
			}
			$reid = $this->public_model->update_pub(BLOG,array('title'=>$title,'content'=>$content,'safety'=>$safety),array("id"=>$blog_id));
			if($reid)
			{
				$obj['type'] = true;
				$obj['url'] = '/admin/blog.html';
			}
		}
		echo json_encode($obj);
	}
	function webtsBlog()
	{
	
		$obj = null;
		$data = $this->_pub_data();
		if($this->input->is_ajax_request())
		{
			$blog_id = $this->input->post("id");
			$notepad = $this->public_model->detail_pub(BLOG,array("id"=>$blog_id));
			if($notepad->safety)
			{
				$obj['type'] = true;
				$obj['content'] = $this->encrypt->decode($notepad->content,$data['user']->salt);
			}
		}
		echo json_encode($obj);
	}
	/**
	 * 快捷锁定or解锁
	 */
	function doUpdateblogStatus()
	{
		
		$data = $this->_pub_data();
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$blog_id = $this->input->post('blog_id');
			$safety = $this->input->post('safety');
			$blog = $this->public_model->detail_pub(BLOG,array("id"=>$blog_id));
			if($safety)
			{
				$content = $this->encrypt->encode($blog->content,$data['user']->salt);
			}else 
			{
				$content = $this->encrypt->decode($blog->content,$data['user']->salt);
			}
			$reid = $this->public_model->update_pub(BLOG,array('content'=>$content,'safety'=>$safety),array("id"=>$blog_id));
			if($reid)
			{
				$obj['type'] = true;
			}
		}
		echo json_encode($obj);
	}
	
	/**
	 * 删除Blog
	 */
	function delBlog()
	{
		
		$obj = null;
		$obj['type'] = true;
		$data = $this->_pub_data();
		if($this->input->is_ajax_request())
		{
			$blogid = $this->input->post("id");
			$obj['type'] = $this->public_model->del_pub(BLOG,array('id'=>$blogid));
		}
		echo json_encode($obj);
	}
	
	function tables()
	{
		
		$data = $this->_pub_data();
		$data['title'] = 'Tables - Bowen Admin';
		$data['url_method'] = $this->uri->segment(2);
		$listresult = $this->public_model->list_pub('test',null,null,100);
		$data['list'] = $listresult;
		
		$this->load->view(WEBTSTHEME."common/header",$data);
		$this->load->view(WEBTSTHEME."tables",$data);
	}
	function __show($data,$theme)
	{
		
		$this->load->view(ADMINTHEME."common/header",$data);
		$this->load->view(ADMINTHEME.$theme);
		$this->load->view(ADMINTHEME."common/footer");
	}
	
	function test()
	{
		$data = $this->_pub_data();
		print_r($this->input->cookie('webts_rememberMe'));
		$remember = $this->input->cookie('webts_rememberMe');
		$jimi = $this->encrypt->decode($remember);
		echo $jimi;
		$zjimi = json_decode($jimi);
		print_r($zjimi);
		echo $this->encrypt->decode($zjimi->uname);
		echo $this->encrypt->decode($zjimi->pwd);
		/* $num = 5;
		echo 'num%2:'.$num%2;
		echo '<br>';
		echo 'num%2+1:'.($num%2)+1;
		echo '<br>';
		echo $num%2>0?($num%2)+1:$num/2;
		echo '<br>'; */
// 		print_r($data);
// 		echo $this->encrypt->decode('tu2s4SkTEx/9MwqJXH8v5jV7zulBxSg2A+ItygslrYIOuW+5i3gZ962hZIEP/sM+BHz3LAF4IKaEE4zJxyoErCjcMjpCLROx5BQdSqXGEkfHFBHlM5fjnnI3G5kq0Evn4rbc1hJmi8NIGvlZQnA95Dh4VdZ56/1tB5TDNjAJeFoPU03MU35vZ5KsD7A+UzWfNWxUTet7TOd+jG1QgGPNL/3Jm92da5QhgFNab7QIlmgJOY5kNOg0qnNUKkP93MToKGfGPTkkB8GocYk5QOQ5Mwk5zRaQMFI8COTvYdnxqt9b/UuuoQSDmo45DPk1NTgUE8FAQXxE6/l1/eSuENThxKmKYtUPWH3DWqbEzPfsq3blTj/+an430E6KWFtCQOh1f17xovWO/BpWxbGf1PZ5u9crT5v0QQoPlryKrpioVy0nnXxXYBzeAbyXxux0Sqi2XlMKCPhMU0HNUfYDjWSZQsdfOjd1fklcUWwtt3BBJJbAJ61JqTZOtJWo9ykGBF6W6uw8D5b7tefRoj3rgW6CY9C5HiwobbXb1sNM5JfRDyDFErH43FciC4n+T2ka8B7PQX1Y87SkTBs23V/bIf63CQY/SQWuSPPDOeVZgNkrekTsMHh9DYLkFb4F8G/SG6dFOzkg0cBRpyuXJWWlaNKBpB5TpdEFJZURUf2g7SIznGs92wM+3xDOv+yCw/EplB6WPUBQLMOl7UiVJl0ePJ3X7sybuJYx3XukokZqcDP1uWctluld8t8hw8bawxMBKNO4GjJUYOw4Pyjpd4PFASohf4WL2faM3UWdbZj16xys7dOAgQMBdzVF19G6QzGfZGILHJRMKpCbFFlAAhRwJTK80DlTSp8r5/32SiwzUjBYvHo2VO/1GhtSYPFBIeMGLaA7q2qNzI8zEAk9oqOEp6+sdsksesUjT5H1rSTtoVJbI68dDxyEkSu+KWeUHYEByt+7H9TyzAWc2esH9bfapBzbLcJ6n/m1jiHGlfiWAeTWmCZaPhi+pJ69g38Pj5z8rP4W98ov4ye6UgZdn69YOzv1/ePhgPfQeJlvCnTPNn5wohni3EiZQTVnBTKYe8wBFVykMUhwadvfgwupy+FK954XwlcEAfGnZb6dPyjeTEFUT1yLlm2jkmWE81XUskRrBEHWKzQBalya/E0a2tHvB17cRR4TEyiqylPSbtCd51XPuA6W+m1nE0zVOyCLNGRzE6v8+Q6nKFR14f2qiTIHskYqTNlIn5NkA9UXqGR8QIEKRR/n/KqIYq/Aun2JFORgfcyjBgr0f7LPp49Guvk2OECkw5vJrW+Aa4QvFZtAvGfp8sR859Le10qVDwILRA2RlPDY0nO5l9Kn+3aruS8g8poM7NEOzRATwkmHzkxH29KzIfBg7NX5KUQLKrpImQiVfqxw0+NcoC499HwXZ1ZyIjzgS+LRR4NL2ybI//m+THPFmr5AUtk+dspX/P6IIcLljAHkKuSBFBFg6y90W+mNgyRRRXYpwAXXcQIrIBJDdNtvx7taukfXEW+c446eh1gJ/8L0VSff/58p1ZQnIwCqFCuPJm108pn4Hm9uVddOLWVplPjP/Y6X3Jpq+dpxRmvFTs0NaLnGBl+uLJK6BVVlAqKwe3kxar0dbynujH++ta6Qlz9CsNw2BUjs30AJvNBrn3YiUN6el+Y8hgQCQE7HfgdcaMGz/dX9rEXq+gw5sUG+/sgL3rOP/MNbFJs2av94/kpr7cgHXozl0LxYAENzIqSBqQDHYx4cX0gL778A9/PNU9AUKH4aRwVEZuBLthFxJmjPDHpb76X4gymMqsdZ8D8vlmGoicfXxRfz5oB5IPGzusn3evfMKEepSxzXhpisiLpcl/wtvuwY94OskljaS5wTjrWvRhUDSvkl2Hwxpu1UF7Gqgh59hJzfXMfPA9VETdidbCm079y9QsdsdNErbJVWzxUqoS3phCykbwz3Tr4gzzvyZya2eFEl/2Ju19Ziyk3M/fvaeHAeu9cRoOwp7bZuwKVpLfhbxJzZZ7U401pyrGuNheBZMjdtsOOF/rFr09JijVtY1CDRtH3cJ+6DgqP4liZZSYxsvdOUQCg0jehGqYnd82GnzzPIG0Lyh8it9EU+1zS32QchCeu66+o1yUDVIRPd2+D5cBtl/oT60BIJ8BWlCyNHMhJvREjTXPTBzc+IwBknGkBjsxeM3oXyFXNjAiVSWcdqice/LqB/5jz6aYlEUnW1YrLZxjmR+vmcg0hbV+NCpGbi2/QhImdr8V+uqyv5k0o1ltFcfj+G4i3XQ+V1KwunnF0PaRvNFmvhnS8mZqFTVU2jUo4ReW83F/CoMjRSW5D+PQyaneBPrhRzjFqcA+CCMx5GUmhumBWGwPXE2BPUg5wxO5rnFq/wc3QnRAgKmIsz4xXwIkiP0Baykjg1saPtfIKb/NphLHfBiVYNsZbuuJUo6yU8EPxSMQ9y+SnYXIwKIpTKo78QCICKuQFiFJwfyN7uMIwirGj9IBKu3LipnvCVr+7DZ7/wpgzzGc1Gd9uDJde9O3SAS6L9kcttM3aWMCkOpZi24qznVrvsWYX9EReXl0KICx0rphCnx/wkfPsxLrizaHAyNFVx5Jp1aSCGCvshI+md0LpYOPGprAAxLSlRDuh3qaZ2m3+wVqR78Fb8XuzBEJgpv4MbaBlryaF87KAXmW9K/RhYx31ns+HAl1bxL3ngQnahFigpeKlzH4Yik7nzYpsggvUiFYCeL2cGCZGoJbKaeg8s75FObARTVl9YB7hNAKEdbfgWIefIX0Ly1spYdP8xqJlFfAl9qHkVot5eE78HWhrntZTCJiQQQ7LHDV3qaeSLZlN7kL9Jft1/JuhdbT1zAS+7Wts3TT+vcdKrn3RcQ3Vfo6qBDxjTmWqMn2y0aZ9X9+fQLTapKxWBFEl7quXf6ylNASTIkjJMkz/cUp6lb2D0XsyNEb+wMhMs08o+SzhKRkOwslukiIlNTRjAQQeQajLMsrnP+qElwpvFm0yIaEtwwrBfhrHb0Dco/u7WZSbUKMr5cdvVhSeiOYhcfhpSjjeNcChde2zO85nse+koxDNfgDld6PQ3Kd6T2rA3FjW9avDJJTOQmIRG4x4Ok6i48TubFlEqMZkg8Bq+GAS7aYPkS3K7v6u8wg2qwunVPHyvsGdXkCeNE64r05qLYaEftfs6otlCr4Vvbo6lUgBibfujp7/X8LF0Wdy5ttW0qJQe3e7aq0gIoOsO6jbuqEuT+snnpYRveGxydKhMduE3nIgYC8VPgeD6NJANwQ2OBSVZRFjcrcBv0++h4LuN+G91du3ROcxzOjL03tdv1BR0uZHjrpRYLrumMW0gySQKD2jH4rY7AZiyRs+Z7zy1Qu8OL8Mk+5yKYDQAjsdWZQvYidz7/O93X7X96JYXP54hHAel7l1IhvQ82apG6D6g/9zOCKbuG6TnQ5pdXVMW2E1z2RV6NwgWrJiUgowKWuaICp/ss5jScadkFqu/U7rqZZG0T46bOxMwg1qEj46BX+aHKAvccGVpRI4b0ecRyCDV6CpbPifPTqVEQbSyFTUo0RXfNDjIyUdW+Uh507AWX76oDmjcvMXFRd6KbNgQ7q+BSDgAgjd/Hb8A0cKoOq1S0skiLd89Un1iv9kJTBzF/eWdVrGLioe/KDf04w/1sEUfaCdoxH7zZQr5+fZL6+KpWXEQT4rQgZSYj5EB9G+gzTE4rPEMSHsJuXLkV1VihQFjw03jNpzPlNij+TMM1QJeMPtdxP7D5R4G9BAnb2OJA9zGH4eO/hKX010DXl43xEMPUN9mYISnkdfVLHJIh2rEK3LWbGh/C1IRozZz6RzjIH2yo2b253oXVIeQ4GYOPX0Q4czCUqPUCw6qwPbPqoql12AeHKk75kZBot+mpeBxFbQHXjO88JEyPXFkuZPBOfOGcIZgWANhQR0PZXAGTQiqifxH/tUdNdkyBQRUaRP0srT79I7qg0P4rzxmBYZ+qyV4wEuWxR90AkBhU7KjTTkSNrmAW7KwpsQMd1TAe/SVwGq8PWf2bcDmcpqPZEgeIfiNt76IGZCvSh0HJAsp/FDviEm7gl/SlKkMOCTzc4NwPLIcILXMsDJO1NbEPTcqGludBAFm01thC3XAGrMCeKTu/awZw70Lr4WtMjmBBVOj/QszbmnJXucFt/IcsV7QQpHxSDpMRpwQiSVsfdUN1T8imMEKqkxbulAV+ufV6jKxfZ1wEa1TKcGCNEYFsFMSoi1v7SxYryWuGer45C8B1K0HERBDMSnUjwqt/3p9+P8aXaRreH5TUjAwQikMerBid5KuU5esOaMJ3DhEkrj8Imr1Q70dpxBfezo4+6WC8TN41ruOyGQssZj7snb5bGCh8LcTiDqilHv+ZXc8Wnsa0CUgM0h+8NgP7B1PVjc2XM2ZVcfPS1MgLSh67z4xTSSZHvvmgkb1fTreseGrz7HJRQ7OlVxM+U8ZCblnsMi+3UjecuXtSijPly0g4RqoPON16WQaMXmXU1LxhLzX0J271xNpJL2qf+X7tY44x4cQfYi0Z119PXOOr797q1kGErE6J0zrOdfPABjyC1OEk/wWQ1nN7shQUmehwdnra1uqrJm0MAw+xuGcaplwxnxbxiVpsxu49ETDDP2fuK543qjjrYj1Z32zHoom2vecz/4v2Auvb3onvdpcsdma75Jo0emOfzfNf9HOpL15imZ/jEidSS838FbneW9BwyIg8x1UVbd8bFTVmz7P3SgLWHbVpliJ6N1Dgg4T8cEP2unQOfU3Jt5VTk+J+AFPIIPFYoTqcOnp+mREILAt68wx+uedwT90TJ1fCwlOeuEo9N60JgRhFKRg/YY0yExBsDBjzAp61NGBS23pni7+BJtuDcFXJI/eDzFFbM1cHy3Mp7wlSULdHLdR9/QRky09lN/oplXpgjyT2vMkHz3gOiMzej/Im1SpvxeOr+Nm7ZC5btOKt8Yhs32QiidX4KJrKt8PAVWQ4dfnfgsPTTo8DfG4tA+qL0Ff9bkDDktqKBERkeotj4YEthH3GRUkGVoHpFe+xiPT8IqD7dAMYgB+KEGfHXzkeWCnPsW++USoRC6c2dhO2chGFE427AlPcDSCCJDB3CIfPTP/zS3gkIm3ZanTv54xCbH6b82l84rrq6JokVqD1riLA5TV0dGugER6pcsYSzUDJB7ap2jZtBiNdXvThVjV6/st/9sJD1uTYacNSb+VEY5Mz9q8A+cy+FLZs0ngPvSfKu1ijKrCzrYh35kxPBEH1JojiMVKxJi98v2k8abenVGExEiF6ek8pNjppoIwwwYcygsAsPACxGlzYSSjxZup66dXy2XLMpbFqE9VriqF0jCyR7VkeOVfaBN2AUWuRNzWKf4jJ9Kri5iOBTkcezoyMkuYTtV8t25pipAsCaV9mJZwwGgKI4kJABPdznyi5Pd9qdDVl52nXQkPD5fDXrm+JQ5PsStUNd+rfkA77wFOKq5iC1SM46oQhgeLxN4fyEygpv+6gcY+LMxLtgjl5eQ09xCripLGwouKXHwim4rcsGuFEXyH6GGcTnBnDLkJU5hie5/4ELfgvCGQ8KYTfQ8T1mMvvi7HzYMRlGQq9C/q7eJzhuPMYyHY2EEUxkE6D5AFq2TZ7LTRR1/zQmTbcl2rxKXQg8QfcC8HVhSAY+gNjAsMF/GOMLTXV3lnDQTGSs4cSxSLB2nLB5y7isT3DVanBbVvztnlH6xtis8m2j+6yIFDSQp93frq9szPTKIzhPUZtepszU1URK4cvr90Q0HAFdE/o20hY+WwaTskhLqNi88VkzVpHi40lYKxappNDjx4MfKbNL0O+VfVvR19JexNlAyeCE/Jl26LWtyi/rLMhlDEqGmtA8iCCzF0TBg+s/8V0m+n60BjNKlihqxR1NYnJjRHn2btCVUxnYNjxcDKkCm3tdW5bdArDPw08YUhVVAR4bW0+fHoO8TXBmAO1k9atPO+NAi1zJu37BHhz+9lbf0BM5I8umsB1+MPEciZQi1gJhiDhb/CV0rPFcKI8+Zof0pA8ci6aEOeKf8NcYXTvkW224BnZGtIhR8sAuKU7W++gPkkenY5ESpLUJXglrPUgkLCTgBo8MooPqnbK4Q5ynNxygiCxNhdMGVH20SpKQfUfGHZYa9h2eiAvTOSZP9IKt1XobmjMo4tzuXYSTzyLH7eP7RBruONtPk5oyS0EBMZrWJYUa4VhhKX92P37ra57tnFJU8R9oYN30ENzoytAFpD+B0GXsdVgDOUXkziZ2LnpikU9AW+MvEAeOaY8Rm2ryCC8TxioYxWKvgJdr8/d58T6y/ACIDXSN7d/bT3lnna8bTt9LwJhO3ZRTZlXqcniyFLXeA/+i7pvwIPucApLp//mo/inunMpyuhJfBdROoRRMllrWPpQHH3CT07TH8oXPbm2OmJLMUgj3xT5d+v7cYBs63oIKPLqY4AeWvK1NOrYvQ9FuJC3Bld+/U3YpzwWwyjSxltmt8yD11tAv/Kmbv/vEH+8p2hXtQWrxil2xNUhglSxS/3Pv6oEzbM8ZN7OHTKszFKaN8pddP3sPFUEGNa5+rozmN6t+GxDRvx/mNQO3LjgAn0KjKyah1TMXsCQ5pVbiJuBnyuCYdK9wQ7Bqpb+rZv13hQO3W6ZsGsM3FCzfH502M+rgbuHhF+yFrl35O8+XmH8p7jFus110AG99t8n9ccyx69nyr+kF5IYAdPv1++eGMHMITqRTx5iUpz3u96ZMWqNsaqYStg5yU1nNKXqhxVWCLxksMQV+mtxcE3xlj5BaBGsGPaI/F4sGp/fX+KLOIOi644tikcrscj/b2l9hM8fdzK8Os861yMmD/t/5HllHc690gH8JWpSUn5G1NVrbBOlWwCE8lIQf14oYhHWZ09V4AL/4mwf63rTgRFbul89uy7oF0WMz7NLp3Hk4ESH+kkMVnFc+L24ftsKFoYwk4afwgiAgvZRti/ZMuQJBs0XKhQYEJfh+RetadYIushXmRSI1aQlZsVzMuhwendfy1Yl7KJS1qAIfz5AVmd0P1xAsmHGSc6K6eXDcRCbUxHIvbp5cKURn5l8v3Hx1loN1xYolVecqPh0GVghUe3p+kDnXYI/BOLbScrzdWksXN0I+gKx+FShJZQ4i+cz5flFr5zysymZDeUPHWKCei7DDWRUWbnmj6/N3buXwaLR7xBEbYBfmPeLZ129jzOZFJo/GecyZumy7IBYrdD2MnDrYAnz9QAhHQQhAxzShS8508/H76FMh9fyMUxfMfqLShOa27e7mviFKxMNkxp1HeRGnC89uf18j059UPFi49qDGbAKciQQMAfJAdsuhlsnE5dcpYLWbEmDRSbFRqJ/VBMRxKTK8lM5tvUt9Aut3Iirh+Y7PT2cS0bpkHocWPlE1/untaMa4tbsnNwxEMFfsLTUGeomz/o61S/+Kq71a5K/Np7hRyr5zWQps7r28oA3zua9K+PXODez3vcmQtBjWOwj2JnBnZ/z1s3xjnqsYM31oDQw41SR4b1rbLeYiEqcP6UjZSt/3zQ4sC4tPNunnhCal9ROdCpxewjThCG+V+nFnzOM5DtPy/mYNwoUQuMIk/1iTCF3CAJWw6WxMx3krIoxi4qCNQDi2u5lMDSNI20Vbv5Ea/xA9BkJVD0OUvA1Ci0YQSiLrBs7DNS3+JTw77BSVUusDtaTBhaqX1LEMjJdC8131+VdA8c0yjpftJnHTIT1zG235Hsx3yxrNmQRIQ7I0QeMCelYbXWjZvLiHa2//8hcCL1UYB/R4PMHBdg9iAGbTrfIrYN3Y7g4pVaF1DTPAWppm0etSzmbNJDh6V+2X8JryATrwQdPLSxDtEk+vBs0KCEeKkYaIOUYTS3P77T8+PTl0x5C5+CKFYh0D58rEWeikXrEHtnHrg0NyXuHK7frZdxR7VhV8P8ikumwSFMpVD8E8/ANlSeubUhomHLxh83k/HF0SKPNnsburnoZ67X5UDLwV9Bi2uIM+Vae4GSk6wEN5vkkbpzTkOVUS4GVhkZFDSXFCI7y5H+q7YH3EMP2xgTMoVOctU8Nzu4LjUjpYBFH7NgAq3RJPqdMyQxfpcG55uXZbB7UqbyKo3IRYlEK010IXNKi32R3pONji0IA0UnFGOy3iVcNbc1tiDSbnrFPxmKMvzvhzHYoYEAyZh5zk6Cx9itKP9gq+ObWhminJFvvtHZeo0Yhis2ewRmsdfErf9LW/WzIncTVzZ99WfpGtdWxRv7UnDoN98Iwr9LAoDKbWRV/tiPAKtkiRt5lz5RyZNh0yB8vkcKo9hSC26oyQtFbDac8QvYlydYn+zHFyMjwizLufjEKyPNddzwcRJ9nL8TWjCPSjxJg6DcSo08P0LFDUcZ4CADdO+1PYxHSxu/dVGa906+suZz853XFM8W/gpLZrfXeamdDOHG92RPLUFf7+Qw2ZIKrq+OlS9eHlwgslrfjYUIsuoeDpRZ54mLCBcUeWrGKovmhWZExE6xsc5Yo3TvH9mjvwImvm59CMrAlYzpGBYVmBOTAbwmTsG072/XdBxMchFaE/wxHeqSsgavKBCPHrVYa7PJAe1bm1NYH4WsKiYPcZDdS3/SDfbBOoLPKM+YBc6SE/btfJOXMVo9xeuGQ5rrPCgWXyYI2nVbQv+7v9DXQux7QGvabVHHMtypASvWGSa2TOF+sZhGcjVJMZJEc3Vu2RRxNPHvKeYymyWuZIK0o3cEfrjmCCB0TDOKgttbB5gIG4KBXChCVtaYfk18tCqX3ehE9Nt7zeX0Xw5dIwugZdWRH9smCG8Z2ugl6yNHGaMhH4x0f2lzb9GTB7/d/gC947SJvm4r61RjGFd8wafS+wuZ9DeRpR6VvZ3IZ8+tMrk2SVCdEaUWRVnpslDkkTMJiOHyrtgXzPdZjNCsBjIQvfq7mA3rVi/UHQ4Vww7djMaHLuI3FW6JJQEJlGhLg0mcj9G9erGZBoC2LNF+5mFcThXHo5QZdiLro1oKrsg3eC0kH7c5J8QspfrfoIVyI9zpY5cimeBFB6eHlPzC8H0eprPDr9E02mo7rH09Z0FsWuwKh9+eUwoha6DSWa8A4Ik/g15FU5hvHQKAxvX6Nw9NdnM5yrGqC0RefSdanlJ+oKizNormgckEELgW94aUT30U5efYvFsq/H0LXUEMROS3vJ6ifB99IhCJLKPgmbojSJt886Ets1JhsyG0TwKX8rR37ZPNiaEtSaYYgvOe87nKnvzqM3cXbuBNlq8IGuyq7JuQdCtf0hKD63DWDOc+R0xIgX8YmLbin/AVlRaf9sJcbuh54y7G77xaGp+aM0wMWpSsnLOjkuPvLZmFuLvq6e95GlH5NeCCxUg8AlrC4nDKMmt+EtuXhgm1CUbPJUrjnYTPVYFKvxB0UCd5q5j88+2ZJ9gFAfJb6MPm1kvOd03X98rU7I69f1OUPncp6/IXZVU4o9lul9b3uhmOoUQ5okc+yZi9SGEO0gj75UENIymVcXc2ODtSsB9WluPaF8JGJXPrgdSrWQKRmHPgqQoC4o/wkCzs5tQT0X2ybZH2aWwG8nMhROBvDpvmqEqIEZGLmzk/0DF1YAIUFYzTG0YLVUJ3tfJud8Yw/U7vJhWzcXJvbo8iueFEiT114mi/G5LwGeRRJaqFNglqP7JeonWr8QB2qy0sfiBi7w6Rd//M+dIbgnC49dHPpSUp7x83jOMnzw/E4Hdlx0uHPJFn0lQ7dyETXNRqV/R2UlHtTI9SO0YlFDe2rJ/UGP1sauZ1XqK1TOFoUxiW2UXGpBO+YGziEoRR5dLr+0PwmURH+rLSP/QEyAvB/lBK+/jFCjrq5cNpcLYhCS+hEQi/BoXylvNO5FNoWgJQaJsUUsMHnQRBSxe+FbGqXw5fKl/Y0AchgJbH+3WyZWhn5sj9mU4NEMmFXouhX0uswglr6+NWEHnBwo4iE+45SPMnOVIzODdvtGn1M3vqXGGwJdzdmYOY8sBfSQ39epmn7gLA64eI3Elvqic+YU2M1ca9u2BlzRz1LHwX4CrOqm9glJmFlR9EMwGlE3w+viktQPhC28qUdHhQ15RVzaeXOAsLtbgnjoa485GYcSJGRiRJb9SQNgZ+WdU8MpbZRnJ+jFFA4QelCF/YPN3DhCHMtrAaZ43r6fH+bYyzc4OXjDeibcPCxpj3OAQoIsR9tYKqOzERtLbqzPRnrUZpRWLL7VBswhqscQg+JshSzMjMvKmGrpYr9ufJSMWpuJh/w0dPMGyjSR8xPjyXSZeDhyaqNrDyKQf9nBmfxSkcVy5eMGFrkLqVU3mcvpZFjIPztdRe3VhrL+F9V+3pi2c2pZ2o8fYJHvfmAFMX1ZVOlXzaveZxfpxWXl1JggbJXgYJYEHng071y14J1vF18yX4G3xOsozvxBx8sfAWEn3u4WLKLjWvE2bKu0s+P1bKD1StKn2Pfjl34AoxQXiMG2XzqnQ46bQ94M9i5KvGt2abf5tkpMpaLTEeJfwfI4HAifLNxT0Bj9YASN4HfYNGPAqHIePgnt0AQipIUTQrIPUnclEmAK5o3i9reUBQZjnl4uCifkmkjmeSv3aN30x6JBgHXs8mXzAh09YmJFjWpHvYyfDmEjKDGY+5BEsuRb3quCIrCBtrs+waVmq0K57aOdmETruIys8IC/g7ryF+uCzNMlODPgLRJlXUYr2Exup53zXVr1+LMLrXW9nnmRA/M4qa5Ns6wy+V1D6eEvm4heaJQq4txUbh90GB8bJRMOwigFxCq9bgvadDGaKmb69j4Pb4xKnG6UlsH2FBnjexTu2Pv2PZOHSRZd5RsPBEUPhKhVX6G0NBVocVnwu2rdZar7p6uUrV+1l7jenBM3BIXxTEXQk5DEV9CFQUjvm0wFJbjOAiJghCD6vyuwfbyfrgn+n5bA481Wv9e9s57PFhPY59XvwKq5XyDwVsXMWE/f4d7C5AND6vVQOTgLlyifjgZtrs8/dvpJXEKKR6EyQlhqNK+AJ4cg4WqHT4nj8FRFRS5BqVqbMiww16mWP92wsP8J3xk5Ef3yKGG/JdhIGh66G2l0vTGaTMfcYQ/4viKWlamyPMp8MDL5kyxLbWW02DZELJ+FaOMPWdmkfMraHacc4CadLSQUUsQRGoAbJxXTGC2pNaFY8TPAaceCKlEXYUQZGyrb0ihwSdYgcFQ9NgxsoWJker4z2rtN7I8W5467zbi/6cijfSJdWg0e7arsi1F8LJkOnLzpCCJgdnsp77mYgM6bmhiqArULp5T4YcK1etfbOkIuf2IHdGenQ4VhP6Uqx75H+V5zlbt/rpAl4BWY9eFObevizPHpyJv402U3Fbc4vcoPHXDmFr+Ks0CQ1CnNqeK5beEPY28t9HjWLHRAIbXyEQ4vYT9MJ/URS95yrX4cHVqRRrSrQ4RHGnkj+wc5Vs8mG19hb5iWtzjTmRUCKTbnLadcLlwS6OgRGDSDOzkq+KocRlOQ0tqYdUCDLuEZzLrcKXqBOGGi1KWTLNNS50lPIYQC6RgPxGnC+0mrmu2ZrS9/cDPBWD4Bs0diGKMimVxFZlA8LCcHKHH2HmbtxQtmhYT7WtgpQhzMpOF8mxqO2Ha9Cy38HSf7MOqbKUYlFrBvGteXOK6M1KxIO4ahghvkcJV5dZC40V+cyHMdlGzdhQcSfrajLbF5fhVQU4RqcTUW249BikexQCb14eo9Z1rdFxPx50+jTf0CNuZX8W6y9f0D4U0W7ea6WA1dodWwgWcfn5i1FQXEO+dfCBuFtJz2FtLw9DRFm4T9YxJlXuQaq+hBJJW4RMxO8fRbSKWj6g26rYzbNGSdBC4sK9CycG9yeYM3xZrFxzymfWau3LuVuxrQj8nUKdnyawZD8FrvcK7BHcBolFArb30bAtIpveSPRqiG6GxhxeyArkt0WM/jUmXdu8rzVP/8lqe9I8HyCFYHWwCc35ZyeINLqTuL/bw9LGWAT49S8KDdvNa+tLuc5sHAHZ4NqdtZHYHr8C4QfQIHCJ6N4fJ+j0zRIoncmwr+rgqCGSm2NPTYTyBUVT6VeI0wmmIr8NLSPfSHReU1PvYi81F5oPrjwls4IEyNk+g9GBlFHFKxRJK3dYfQYnKOaXa0ylL4AqYIzUz0MpARzK7qTiQ1cCpyQ0f27rFSV8UpejgYYCDO9ap0G1ioWCVb5cD0+0qPusI/5B1IaIRzCnzwc5lVvPyHH1FdQtsz8ZaV/JgOdJKqZQ6SS9ohOO94/DOxZK6FGvjykk6rC1d+iyH5VJiYf184xWYhAMDXAatjZ+dLfkYaI4a0DM0H62G4o7rIOoqoDQP0kp1bLvgRiOEWZDSYGhj6bx9BsP1EBkYymqcxVRw+u1yjkqywi4Nv+rc8vXpzKPq5UdpiGktHG1ygFQNUpgmc6ew6gylMfRQ0BJ1VqvBTgeNr/OPdacLNY4hbOshmXHJjMWRn/LNpnq2QQhjUcf5nEyfYgRIEk7PRr45Eply+1V8Nia9q2mI0ls8EnuLVtPCzPBRkRomULF2jmY2QH6H5ACGrsNBeqyPaBk228rBgjhKc1VNQC7m5BIXmGeRfVjSDmXU31apstjCmIKQrnGakgyy48xyNSG7fDWmQPseSraCV89Q3K+r6uXoVcT1kUogh5FkfbD3HGecC4pBd4UK+jUC54Kf32S2WImeRqzaGEpJdV4dLGHlTQu6KsQPt7mUF4EpN8VnVBk9swluOsLYbvwQLfUAMky9jw8S3SnSbo1epdSKTdPiym5mBWNQZmd5PqAWc/sHa9IB5ItEhJ9SECC138RXCAavFGChjnKsiUB+dZu8BXYNGSheZE3CQ5MGow0Dl0hwFACCYDxW/mxnztvf6YTWDNFnmr0cyHmFEIxip5KukCBZS6vBYI5Tp9CA58b0TpcwE4XvrRE8CAzIiUGcVB/72q6Es4u7cnwKN4Qf3lFcSnH/Lhj5EXNXqTYQicqqKhP/47oM6O3YAw1HgGSTmBn1kSbClVMYcaDvR0h9TuYx0rLEjlYPJf5vspM5HATF/1Bp0e/66VjVwC5waBY7PXM6djNhAe5JkeWk82EA34cM5gpbPiO3qVu8p9zDr8NwKPgS0dg5fUS21FvsOF2xEbTyNj5N+35MGf+bf7E0mnzMDLHFoaQsXhGTK7I3NbbHCmTADrFqaTzYfXu6RGK/UXiO0B9de0rFRphi+ZBO3Rsi+HH3X8B5aj8S9NFjODP9Xo606agcttCZf6LunXDED3OxBiAr8kXho4fT7wea/EaUmzuRiNcP7i1mYM3utnVp44+uvuiI9DQ+KQg2umFMXXbRIji5Cn+H6uhpATwazzQMS1YLk1V++RIogiPoNKl6E3h8gxqY7QSdHjHDeaPaksFl/vrCZPKAZczRUy8imS4Szcdu7noGwqmHsfAE0g/GAfnIwYluihJBdRg3xLOTEJ3OeiY4NUe0S+HT/Z6P0Mco800iEogPiJTEFBqWmyFJn1VHWxoMSblE7gcDXDAMOXGUO/BBydx5CYOoulYzd1u9t/c8Slg6BuW4lMKgvP+RWoq9R1JAv5/NLGAz8yVQJaS+gunIjeyxUbka8VRYmG2d+/SjPwsDzTp+7XyDHKTJ/s+ERJSPYQC/Ttco6VF9rFZ9xv9WjjxrqkHfdBpoXA958Zc6p2hmFlRNXh4zaZc1egeJOH8oiL8esoCpHbB36sd5tt1Tq/lF0+CxjEFh+IhmYWRwF//9N7d0WgpTf0nn5G9nIywTRwQ3m/mmP9M9ZXxGLY4YmuBnQoQvE3rF1TziNiy4BSRQEtYVeiQl6P7mUzpmXdq5KzqcWNpt4q2plWQb82gPxCt86365bb10Wpo4uPlLczRTCE2Trs74FAr4RpVr62cACCxUi22niho51OuhNmyj2WlFhX5hS5yTZgpBZOlL938OHxzXqk/yTslSvhQ82h5OW7RgQpST7CIvPGpyVNpeo7RNs4nX5E9S4mw+CwdrM1XwDQCfkyzszrW539AyuRSqrS7YKI/yVOewnpqAlMQJQT4vXnEgszttMaKLd5s/DcSujbC93Y/OuImEbHnuMgmefW76Ut00AFdi92Lko4uTf6xKwxU+1YudcgN9FvE5zlR7tgupFaZ4Bj58+e7sw9Kr+Rpl6OUAlfV/7BxvNG9z/4bnD382K4Bek4fdhXtmufnVytUvYBxNlHdBrHxbqnMeZwVcSbbKmKFYs4J+9Hh/NT5cqTXDtTRgOKYjkr7ANPiIzoiDiIpPjbxO2w7wMRWhvDZggDgVwqrkGuGK9plmxW9RbhXuFFke6SlIjgtHYMlntIKNTlp2JB31P688XJmXGqJyiiltEIRAvvgvam/Keg6a3aQo8rTMggRJcigiMEz06soo8SPARD3FaNcf+ZrC+XPJKzb5bWwtW7vEtnLXl3U4Ka7jI+wMgQBfyFMfgYGn4/udqRUSgFAAisr5eBtA8sp12xST8vbXcUPZoGyJFBPqSmbPeHn8a8/ggRBNZpEzHLbaSvfQbzKxy08J2T1QjEsF2yLu4VIUkCNK1sqdbJSWOimZVHHXB1Cr/a6ZN0Kv0JR0jgHlEFIa8h49LpdtNgNaf1urp/dNzOnbwXv5PBDc3AvYQC26NWW0yRvrDNR/829+1qA/9C1aPa8HxAuB82lCSF/JXWXWAuUocK4VLerDTuZRgb52uPvKrfcK7xZdW7LNJhyX566a0EqUoLBnz+GsDAelNLLtJJ9hQCtu171oxybHOI6FgXJSecQg2SjhEWkYK+9Su3wZGhum+a16UFQdj7n4T2s9mkp7MvjippK3KfKupxatz/Ivggequ9G5l9Fl/DSJfmz21WPTULiVJtr2bSXTkHVvj6oQR4nDRmX5nrFfHsg6kd1nU8SgVdClcg2Q0WT7nPYuiM/mjHdu8o4ck9xgVw0UtGRoziRNP1bWm+U+rGHEUxpuYO28xKoQ8+7+lwM196nnRm9LzV1kHmro7/DTOdQvSqIEsf19XXFeWkBgvfGow7oZYb8Xn98u3JV9o26vyzOJA7R+qojGY9bRfY/N6TXnMSjMfonjBZKPEdV5+MxscM0JEVWmFtXN5ycAvPSzp6CHTafHi2f7g/fPQRTzoM6gumTl41fX9q5p97AVU4+Oz6jTi7ZEOu5qTi2sj0c553P6p/9iGqT6GoM9KCnhK68evGZcsoNVTm9IamNPT355RzkuHQFSUCXuso35M4l7QGnX58cx4uioOTKgEllZ+Ws8uLRz9Jc7BQTziQhkyI5coTaNt8Wuj2yQHFASlxSFhBGcp4+iiE94UN9ejbFoO1m7IGeH8obAXzuZaYgbfNP/UWNy35A1W0DJc1yv8yvxKvw4Jzv8sQnNXGd+XaSQZHOUCXxNPbDaTVZ+E9j0meK7c0W4INgpsIFwFaQUZUNZj9GEmL4r8ttUVwMHyKwoFyD3D/pgWpSM39s0KWEQvK74C2wL12KzqyVc8KOtIHFq5sq0f2BApGwqEjw5LsjfNO04ORFa7JXa2qJStXhrfla82rAKQJG2vtyE/se4+9gosNFs9xUNfT7u3gPOt3Ot3zSMneWx0ehNPzVr/sx49nf04GAJbP/auanxnroGzFKwmdN8u+8SGUmbsr2VApanxMxdHGCdXONpbdJMT7q9UPDVtT+PoUgS1WsATgLSKujyB4+ah5NxKOQObZQ4o/cRFHSjKTSMgwQmRrs3QqOnJC/pB6NX2wNg8n1paG7puUAjd9Or4nBPhd1ovcjzO6ICPd1SoxKhhynhQgnhF+WaLbL+idjzMhkPWqeqrZglGHGGu15OQ4lHfkSiQE0W9zFZ5tMWiJOyIonzceYT2FsSnROxHoh4Q+JmKawUIHadJkBexIED1+gqiB8x2RfBKeger5u+OM68C1KVpZHGpeE8vVlaf/rBJV673Q/7l3TYiairUJeZ0pxIegpeTM4JnI6p7+EmI2muvjh7rgVEePhetJNlkuzcj+3QFyjFY9Hs1QmOXwvkntlfMC+eqgJpYLog+BAfF5ule/VwRhCI716FzpavpXJ20yAxYzPCeCnC54Ho4Sn6FGh+8+tRrWY7sSQSC1HuHY+E1nwl0x1RXNTrbOJZcRYAXa9eZeEAiEPGKTxurQPZbZEuZzqJEBsqGO0gsDJKngQEI2VLQ3L2x+kgKNDVwgLJV6Ryza3ivUGd9a4MckmP0z3cFaFQtByBPEyZujjx8mhHFVoHQZPUxJnLinUEZeUSJAxLnUK6pzqiFjvxxGZ1py9xmm551HYr18oFhQuqAhdAl4n74X+NV3nVhuRmfSMQbrvy1HTn3JOYI+czCsYFWV03qS7odSZV9dvWbFZRErJ7N7PfNz5bwilQxtDy3VOGwC2yn4OdW602KTZlmCdLLRkdorgGd1Cv2OX+6hBTzYLwHELek+blZMrDUgGNTTl8lPIlteq1f2rQo5OEG3joSwR6k23tFikxAeypfNWBp8N4ssLvUb5Rs5YLUI7JJC+8Rlhytc5+qNX5caX2fuF08ibCYI+ELrhEF06RnCdOqXBr6HKE2LKjLr5RQcOsboPJbqQe62uoBnoHGSkSODtM4jQHo5gItPDmWxvrL53FByCBq3xNP0OC4Ns1fKx8hz8OeQeOOO00AeTGPf5TmjPClwdZ1HnmV4xn30yYFqidanvKf/nwI0UUiWwbReFl/o+RxRyK/i+QULlDX5eUKAVbZyhiVuQXGjIBTDDiAjM2go9k1c6npo72Qz75FRAHqV0tYcCdcDZUt8xi44RWiLW30RA09zqaoB5hO3EMYQeU0iWhGvbCqxl1BBxRo9qWfbTOO7mf8xUHHaAvMBdfo42ozXgIFi0qxNMjoh/hGtqqtD6R+SEq7UdHr9BmoOuzZF6LhFoSeOah8gYm52iBGLioCQ6qfYPys3wV8WvGTO90wVQr7G4VJ0KOPzgp2uC80+zRYVtkAKzE33+d17Zvf9/iuqA0NVG8+LCgnyK1LFdXGfeRKmbtHRLu+xG+9JHR6SOjPnoGI1y0mE7e4H7ck7N8kMRkrf72ROpKG77/AAUyF2iKvzaAIzaVl49lfXt667xtF8UZhVEdB+VGbPxJg6/uBajmwv77XO0YQ7LFaxJ0I8aunG2AQjj3gfQqg6FuQ6Ep7E72zizcTG2m1Oq2Dcxor24l8gtpR1N6UUuj9dEBRbFGHcXm6KxUWqHsY6PftaD/2MyJfdk13a7aohKxSzBPgKh8wKvOZPBP0YGvnxVmUyEeGHC3SLLn/5keyZAiU9vWu1c3D4YBNfTW0/Dd8yi3AE0SWwUg1ZFLhsVf9FAkOUGUmqkvq97YCwhQd5D86ZYKJDcU9oa4daCtPZa8iKl3VS07fPyxWAk4sMRywgnGLG1vwEC/ztwSJQr0rA/vOAtCvKv7cgLTcsu9bWO+M7IldzE+f2Kz/NGBqJfqYhPCin7De4sR6zxZxXcHJXPoNOdmcxgmQ+gbh2WQB8fCEv5LrF08caqbxj99sEjw8i1G2gIUBShs5waCBbmkyWglGC1Xh4pAiooKCJoXhJH3XbIBfH64GzW9/IGmqPnOkMJDAly7YweTa4vhP+NhpJp6q1AfHHXwIabugqG3TIRQN1w7Xbm7RkHvD635LGZ1TEGAt2WqzuDqOj0pDy/WBzrRld1bNCxLpENg3c3FqwdFTMdMGtBcgLVbRACh+dOoqHwOqeTZRdKVZzHlnjcNt/KlIkuAxutSuHBPDbRb3cRjEFKb4Dt79VSHc6imBVuwEBxk+/AYQD8CwW/O1U2oa8W0/ebQ/GGP9Je8nSlJndZj0CleFNsNxhCzF40BGTS1s2yvMt6m37iFUv/mkWpXDKQqa2S/BSFhlYyV1At6w39UqyH98XU2ZfMY+xlgj1j4JgMTKTW3mnTTyqeDDaJT1n8gGdQlfEL/XrinGdpou7M6/SGpl33Ttdc45IE/d1QPGNJQ5I94oItvH5ieWgm4NUCra9QJfIA62NzidTOH3PQib3FpNLEfhTYGXEqTf3cBPyYTH9C6/oO9dAlM+TUxTEF75JL2CnVOQyxH3ooAshQLkPbtCfvbWQuPiod+wexDrkkedwDoDHt0gcK0yZCCKF8uU0JzoWm2KXE4tbgzepRR3hbAZfH2f2OXK93zf70BAPJirRjTZKKBOxEIagSvITdTnoSYj8RFtgWFxQK9A5yaruxAAVMYHwXHKOD9t6kvUNeLIPR7jO0MvY21UUPAJ9Avtd3CemPa9e0TuEFnk5guX0xF/ih3ZmMdj8jHEWmR7hbVoJEu9qagm/FHO1RkHlZ75LwQTJ/PK+TMlOi8JbbhyAsG5E9BrH+3soNzVrve7kGz86IQgM8QJvbOFwIUTXDimfqXR99s/mE5qNnS5MgT7mTEVL6ZVpBUvI7vCmQF7Y2KWuCbnv5id84GQymo8ie01QrNhzrPW8h3THG4ngbkAFgo0UqI+zXgz7WNXmt9B9Mfj6DUFqaGdA8KutC1dxyP/xNYt/Gbmtmg251kVfPU/AX2z505YescTYmtmEvLOepU5o2YJlkjsSd4xlnanefOAUvdQBSQo4HHwnmHB6nHmjx8PR6PNUn0HAZEcbp67saKY1qlIPDtrUt2OvGnpot2BWPO1RvK//qXy7GRThXLlPm7b5zn8mGPuVd+zxW2GbdMQlZ/e2exiia3fJyUxOt4blZ2WEK3aEP192VtHVvYUNk3srRZBUmGrJ0mGUELchPyNJICIkseBNnbS51C0AjFpwKeocKjl+jGBlCWv2segOKr1e4SlYPquIt9rsXalqr9vLj4v1CZpd+JKknKT1s3OHE0cKVNkV+/0pIGsuB3I/qyFBHlT+my+8l/pWTaGfE+peB7hIo8gOxi1830J52QOkKcGvseTxPIglQiLyDbZYQTV30wY3oJDiSwcUxFpmwC2/cXL5efy6x/R+PGvxiHsUk5VXtZtirwTqLyWnJkg4/uHjwbW06cXkc2ICWtOMydw0P8MtqT0Uh6jE8pCYceaOpeCo3GTboHzp2I1i6zGOU/jgUMH6bId16fTm1VLVwUiuOkoxIhKnRKI6Krxe5ei8qCSnAIvGw0wH3/xHEBp9mZ5gMHyCq7YPEkjDvAT97ZLFoJFiQS+UlC2lUx/5w19LO/5yO/GzOISLBZ/ERNg7O6w8Y+LYK0SQ2LvEnTCzdUAnMqww7nCaTJSthxNfwFB8hL/NA4KZ0ifKVpALNeORnHGLH6u3hfX6mm5MeCu6zjSxELTlsgZPOrfCvzKCMjaFTwlTQB4XVa1UkFKGHyO0iCdGh80mm8Kkxo5Ue4tCgFpp/GE/Nt5KjKRDeGAueXs5SXUatKOsGegsY8vb4EmqaK7tho+zp5G5qS+T3ofIwefqi4lotOdji0zCWiqIqoVk9u3g7GwYHALqjZoppMiC4IatgbOpN1bjRfs1ME/t1YeUES1J57gk96B0mnkOJHstAaDreyq8IjvtpEpzr8alqy0t64C83T621b34ms+T4bbK3NJ+JjNe0TWkAPy0Djl+5LfyN3i0XtLIiIk3jtWQeH6h+C4C5SpshT7QgAlQEwOrIFfwujcQfeH/+KoZKEfqSk78dHpRoc8BO6EFRZAdXmZNMpTPeqTfj5pYRDeGBFME80kzDPOtisNY7oPsMdItNtdBbgf5qv5WTxYY0v6U8AbBIANVeH514oWOuza1WjYBonvbr0EacShaoVS9/rzHt1/3qSwcVoHbLEwp2mlOQciUUTNhh98WX53t7QsPy87XwEEFiunxCeWzuDU8iYovj1FamD7CzuqMRRvURgzXLxVIkkoIFWGa8Hz3dHIaQQkMObHpPGefI8yREX/ClJFQdc2o7WaDCOuaK9EVAK87CcTiWJoNsSOcVD66ttdPjAtZZc+nd5GbkLPU2WWdsUSjahzu1T1r7dj4bxWI5I4Ylom0ZDpFRDYHcya55Z5Fw7L91UjCLBj+BQGre7S7iaXNIf1WWekDYaqbMGrass5YWQjRz3xvnR9BJ7UOXepFI1RiBRrwXqLYtYXmdaSBKwtgogv+WQ3xE9JsKDae8J44Xxnd7ivmjCrCs5wnT2hq+u+hX38XA2UYh4wnjD0xOBuU+PaRUrsUzMtizdLNxGxKCFDpa2rHdJQ87XD7NHMfGjRt6CBaI1glRCy/lFg14K1fv0lisz+tZWgMeA3WOWwz4h+GF65KEQUEu2Rj+Pi7RwYU7fzQocALiUaHQBNfK3BXoNAY+8M//bNPc/ky++tjDQ8JVjEpWfuhej8vYQslMO9Q8II1mja3rwyKBf7bcW3SzW+ChRMyunIuteUyUInU3RX0VZEdsB+cruYyoRL5LoBWJQcUGIW04O8toMUr3X1zgi3zw5wQDGXcqPfsVVv6v8WYIw0gmw/xk/xvlokwbFxnZmd6YRNM5K+GPXe64nKAS9B83PqTlCKseBLWh5nsDgkT8yuxwua9dDKjRl0zJq6iu6IFrPjb2Y9JwVcc6aVjIUq7I7jsbRE1tQ8kcJwcR2Sc9Vvmy2CDwrzRzS8qj7Ux+0si31V4n9mMF8ovCW7Nv89f9sdZmkm+IKc+5pQBDeBF3B1vATYaIesHfCyv8zjnbUYyeCAgSN3JdSNIkYR9VWHPR8vGdtbIlRCWYzi7W8o2jOAQLnuCCpjSULPrTAcxJc1zXnu1WQkpMhenRZZ95Vfn40CDbPVGg0Ek3HgZar8jd4mA95fJiB+MfbYaPTSsEtabfchD9oRLCq6ZgRUe0wtuFY1/5cAyA2Es53btMx9hmAr4TfEgtq0QfkvikzT0FKVIN1BApwN2ZCOdeyMNOu4bOh89BM1ZmsAhdQdwpse+OiIB/dJb04HYmGa0KnEKlcZnqTQtgLw+U1hLegPmsDCJIZ13l7RWsRXfxh5EExq9TBIVfoZ6eSkKx/DvMBlBNE0xw7MMa11AQWk/zuGF3aZ9Xmk6TKMCjJnVyDNWDvVlbzXBJgyxmYg98xAiwqLwXFJM65W5yE4E7GzYnpdLLLYzdRa40BhZAzBAPZXvOpu72nwvSm+dGkDdOOBBMqajVOTbqri5geJFdQpdnrTJiuJ/KaaUXddeu1tZmserF8p8+rxyVvQj6OL6pDQI9M5lwFo64agptKB4HLqyIfoJCFTtFrMmJHDDIueVPhgUUOc5fAEnQdQz0GMdjfd24LQNBE/wKSDJJ+8hRkYdlEJtVRjscITBQHAM9MVoqhoVTGbgqqi6pljYYZ0xfLGnKgdVfqa784PaxPloBJbOQMXaL+XC3XHIENi7a4YOGAaJRDZ/QbvZuXCCQB06g+NJ4GmyUsO28csENsV27EUxjpZgwbw3E4GQ3GKWElrBHj+oIEXXx3rTV5v6KRz7GW6EGSYJJLCcWjMsVP+jR5Il/7Dp6YwNxVHJUHqcv4fjaTOhq7n+nr5AKXBh9jdG2EKKOhA7SMN43ogAPWbNCOiauPEiu8kUggjDghXYIXXuvvrD5WMzHjZDGz9h0MjGFR1HX1MaWPJtSzg7MQF+EfFXqUwKbJXeJrHYwsLTMqe5iZmqVshiTEtdKpL47nYYdUjRh7Ay/Qte3a18bnO1WCkThF2Ii2QOJS6eSAuDotBABzZNW0AKmjBLZJ+he5TPZQdJpaEts+JFiUZrWpdGzIAv5f5BlHgcldDMR6SlHIfeQvwTVz0W2uUVawYX0A9s/qAPar7auIGDo70XmDk8JRXZYu5qE6/ffT3Nd/r+t7xvZvFfW/Obcye4y9hKqh5+p2foNIxvjRuv/NkkKwj9/bkLEQMewjhKNWVutAsF3n8wR1BRh2YoQqN0Uymm/1cqBkyMuIPxpxOx27p6yEmoiOeJzTYKTQcHp3W1B7dT65JnHKSYbq9Wzd8t6128mgrIYTLtrS7+Vd/8LzqLScYLt4DdHWCaZJhbOM95imhoNuYNiPDS9MsMPurJHu2mG9a7XhUhZ+Ei+wpd+EDP/PtDcmgMaCFTWxRken4LnC8P3XhAmaENix5r045RhLipgtqoqMkypRS0oW3DxSBnTr3ruqbxIkDEm0u/UYyL7m9aTbooqqe4/tAkQ38UejciBDgvrqN5/R+OFRTKj2j5kKpQbC1kZLYXdDO+B6CUMug0y4W8pD0N6tJ5DQAn70/gz5Bev7t0ULTxz0s9u2IyKXwEfEoglomlaUdJu4+uWZdp/kqw0C17nvl6eICtVyxKf8TUAptkkwi4TcZSubXLGTM24zZ2R5TcWB/o8+HZtq/j6YVlncdcBIuuj6ao4S132qMFR0hSRIsldyeyIsbqlEuf2NiIXoOm18doWIRFtvWIJxa3p1nHKtPEWczbsBivRJ4adWwoRQ6SbwYeCLYTLVQQsKSffQ5yI5Heu5hQv1wgoyiEX/bE8wSyOnwb9CwN9pF/4pj08rDlWAhX+qUS12mPaQID++dxKcC4iTSyHDrBXXPgrww/JF5vzXF76Nvwe/iptsb60zBEcQtLsZMyzb6k+T4Zu3QuAEsKqA1qwofQ/tWe9SSSX0jr6nsVbpN/xZXhLcRn8THQzkh8FN7klENbfOL9GeXN0/xif45jh5eeE56mFBa7oK0k0v1EFTT4HT86rWSnd/YCFL43Ibc5laX1gHooUPvMuRlaeSKjXNPfLw3gE6i1ViefezvUf9JpWeW1NFZpznLD6bIRANO7QJQdO4tj7LC1CE3dnRz3zlCAEdcboy/XgfOmdIJOjCnk9GdebdCdJhamNLs0XtfxyRso0w2oQ1Kzi7aaS0j5eOu0WXFVAxjYryjME6ndhRDEkiuu60XioSXiq1T6tia3+o20hxEn4jek1hRittLo8ZjcrDi6tgVe9zjhsrzK3QuFZRn9ayTG+y+kHEQ/1UnVl9nirv4YJGj0mL9tEBIPw7bhLQS9HKDCfteMJnAGHxLHS/2gRljyhSlcj7e7jSz1biaIP2rj9/z2twpjFAr6qwe+a2UiM3AL7bWCLJbg0dn03QACXmopO6OyR4Z6sL4A3tBt8J3qrRv2rM/bZIDOUtSd3k2OUP40r2Ffc9V86eUu0OPchOWNTD+3BAc3JdR4yIpajGzT2GYH+m0y85Credt3vSJ7iQUlzluUBQoTuK31/iVbO+tUYpn3KUIyOhg7bped7IjnFqZa4PIhUS2cOMba6WgO0EsoPt3M7+JOSlZIxcUjEm0b2LV+b/hl9roTBDr+QrNbzY+OlDXRAOCgC3iPIR5sAyEdKxE0Wv78kfpBwb2aMmO83FReGscvt2fFD++dr79pEafOEVpc3pjp1U60V788q9YaK9+t86WRZ+Djj2w0VaLhHHSmIesZFRA3K200U9r954htdifptyX0jX8APbGXYzJ+0YbNyvpeE9MSxtDvG19998U9Jq3lpTS5VmcZTY5OZky5XX2v7+qq0mljHK1JG1jSiP5fnUP3dQvHpBd6FulHIGnS6KlPmHEuNDaDAdpr6vwvTC6I/uwk/e4Tyjslm0wK6XtkYxNCKfj82C4rjM55n9JhFzI9rxhvMKK+bGO94A9ApXYreFAxjpiwXisx5ECXLUlOcPyfUSO6FmiNd7krpDNaJo6yl+uwiOO/Myj3n3amoJg1f8aj9ItlCcvK9ZfVA8bYcHxRKEgzH5voLllAOYeXqHSe2GyMQmnMyM2fktTKHFBvT35lNY9iTBLquFxZQfrVO/eC1A/eND7DnxAi4cRh8naPCkF0btWTRBZvD9FppF5eUN+QPL8rMMgut264PY8uXapg030aO8Gdv5/AdGv2CrWWKdxChT/Uu1CR7pxSokokfDRKRxhxDnVusL+P9VkNtdDmMFLkDN6GB9axppE39mP667tLHbYVj+kUOTjXEfnKzN0OEZnER/XH/YDwRWQIr4zR1KEDcyxdA0t5QJM45diAyOTQIwTNnwDdgPZZn5F3RHe33bDyI8CXGdKqkD0DUkS4B/VG/MOTa7mXxsNK6mlGyQ9pSYo9CX6WTQGoOL3a/qGuNNxfbjXDKzR4PERHvO1khdaO/ouFQT7Uhk8/W/l5mighcpERLdwyFzHai+5x77Ix/q7ijGu8bTWQ/LiRCxaJwE00A6DyLtVosTqBgx8n49Fnk8d42HjU8hiUxO7dUNbXh1iFmG4qGwpw9vKL8Hrg1+64DfVuC+4levSNJHSPuW9NzsAQyhoFx540LWip3afMoS78Lw+ed6zUsXLKASEoK/Utyf1xfBnyzPAdxgKXMg3YiK6hJgcyaujE/NLJAJWdfV4mC8PhwCIg0EW3sUAQHqrsAs73w1lgpLGpSoORR+v3w0XokXn4hVI/yjX4cLARdhrsIQjTnCf+gndRDla3Q4Agke6uEzbfCAZdQkFFpQqjoLJL5qbnyrgkFTakXnfSgxsRKW8l8JXmlB4X3k83xZ7p6pMiAqRFTmC2xDbTdPAEQYelYf7ZtaBEVMy7VrOzhWktQV0ydtUd1GIkrpD1bFtSMZH2w9CFHwNVsLFXcRHjTxSdeoOIbIEpj4tZba5k0XVp8tCTwZJ4Wi/2LxnXVQFtb1sPO/0iYnBCCWul1OFRRFVePvdthfLQ2z8gLpmoK0jaAc1aIddZUpFN63g6V5o0rd8ShNyIwPU6Wuamh3wQZZfGZhx3BEk2y/hkkn8zf5h30OmyoxQet0SZu3TaW30cuR7QdMt3p1Wlb6aLu6Opu5a1hv2Jqv8aNDNgYmMuJ28ahy3gTgpHICDtdmr1TrmcGq0C/mhBbIZY1UelyBKUEDIGtjO9wM4VmfJ4WDwtr/DlhLA+QaV30MR90QzYmwNn9vGJHR4gSfCp0p+la5bDLANO8IyW1XKksaAUXNEdoI6t6lfuEpkjmcPIrKrOEVtswvVPazXCUoPnqiaS/5V+VoE+WddZQS+yUnab8slYYqMFPnGwhYW1f5pvrcudxXNBxWMlozbq/Dfj8ZqO14Wq1hzFiib3KbJBNjc/MpwSEF9KMOBh7rd0CSV2JuVmwmtdtzbtXOR+iPI8j1eOZ4MT/a42QvgcQUyCzJgRIJ+luSmM8fGulMFczWMP52ohKWJV/SdIwUh+FtyWVegqNd2eOFPTSeZpSeiMdTbO+tazQhv42mlDqU33jTyJm/b+rkOCrJ/UQI7DxOfLaEsB3HHMGvfmP3mbG2mkQhJKr2tIJZQgawQ3DEqPLAmXJQbkcAQBI6vlNwNRceOK7DZrFDyf2L1dLon2L6rrohFURQywse00WDF8QqVmdL2/kfbvHsY/WSXCNg+GRgUxvxEZtWZL35+Z4IYdRLTh/QCjif2hviCqJbMfGsTepynuqRaMEgRP69QnNrFcbYmtr7Qo4Dxh2uobeHaLsEKFIg4jUQEsBGhfWMGc4px6aTovSuaK5bgJKOzj08gc5TUmKqr24QoyIQUNwiPnUlFXBakL8ft4pYnTh3/GyxxNEttKVlxcSWyK4dD8/dq6rH8U6VeNn188H5odbu408ga94DlnZdOKpltHuAW86vsLmtz13Kf+emsboyj4QEkOWXngCzS9xN5FE3CFA0nZlEj2EQCtsgHTmizXBy2+t69qx4SlNSA4Y2UCBax+qmDK05kcQb7w7KgKUc477qUShEQ0qP9Yhubi6wfVYTyoSBx+jHvtW/+bkxLLEy5aRH2jfTfrTuANDuVbSQZKK3833pOhDQrvdvCB0wOYVfvpH9pISdOhAHPPqh3ztA4F2aFVuFI6RDqGWXq6lPamf4Kc2aUeff7sNKoFo/pHhwMKNQP8UONWLlpc75keSM14cLHb8QrkpF2jqFnkXA+zkZ4RYrj/lcc2xdBnPO1bANnMS/rbnV/cBong90JjEh7w9DbnIWz71e5MUKjxXtDRj28+FvuMhM1CuDZX3Xdtuk2/+GJqgF3aP1bcJZ/7zWKi2kg2HTF4s52yKd+vfUtA+HbX1UO/NcBhqhlZl9Yv2rStIqDy9wgpzjPxGs5WCqJqYdgOkizhbsHRuwX/DJqIOcTsmreI2DMcy/fFznTAsLyZf3Kk7PiT6HIaFEpNttNIZtmE0XxIXTSinClLkdJQ5RGEOCEw4XxH9qvvS86MGm30C9eVLG19DQ9Pn4ICAOzheHy3IcpP8JEVxLFQ+UTUviGPDfumwBzrRY2q7CFtSTsLInmGBm9Y/K2+P1em9oRN1e1601C90KOB8wdolvKE1j6mxokQ2G/2yo8/huPw5MZy6HY4iTF1tRFeayxKasr+alX7p9LZ8apRq9rVRf6ycwc5qW3LXG+ze7V8mcchqT4evpvlqLBzAZYqP0Aie/9duUrTElDPZULnhz1RL+G0NGenklCY5WDf48MAwrzj1qXMQhyp7JACiX4tPPZmthDtKIkgCrxAYwzJZt+ry5PfNWnCkoON47aEyJG9cJpHEpsP8eHpdDosBbhanvuUrasis4KLBIAIy8y+UbreUkYhor8Ec8ryIJIgAuHCpkuwrgIvqYhVbNaPw3xuuicmE1Ua154yvkLvcqP+85FLUdQ9FvLbMCQtWCD4mDYb+HFBoJojPUCkp7Xk1Z+p4Cjdll6isUL419qudhRzd5PX3aOFPl1sccjD2DuuS8tfC3eo2zB5vWVcLv+r/+PnnfpHXB7+HLxYo4a9CAjSYFwybGmg/u6Gb8rdUMjejVWy+WVWKwTg3D99C0hM6qZJ2tpIggXLkNIFD2bWfX9gjER9RPPq6NNj3SbKtiLqP/tk+g3DM2qm8Qj2gBiaZjlnoDJv6mxFptLV3o9Yqxjctf3puVuC5NoPctgJskaSx/lJrQR9S3cr1s6E+EmKVxkMMRz5Uir617sRjz/ulk0DKzPZWJvoUXUZTKvVimj3VSa624qEg8GqpsLaWqSY+oaythVUfP+S9FHQUkizY/4pqR7RDSLUB9Bu5i49qjp6QXAsFZOacS+GV0QlEay7WjRaCih0hLxePGa9kZKp+mif/q5jxz+hFKX+B9uJRAal/63FK3bpVS1ZVyiJ/yfaV1zrks+ZtQ2EEJNSZBx4tG9WEhEiDXv1L0GiHSOB72Nif/WZTuvFCnoIJPdQbcRyAMYLI3PBEyKZuoUs970IJZ+NpSOw15ynECMmwcA1kJTy9Wk4y3Qe2wsD8alOqqhzsDgbFkMm6rlmhDjeC8bdsOZCLi51ydmSjCN/9IdFh5eWSdi1ioJ8fe6lczQ323fhYPNXAoBDJyF4zLgborZB4YwC7hrZyDeJeZkCvllpzvOA0PRCwWI136JkvQK8YkG3GJol5SphgScrqtJ8QCpK1K2KzMEP2HfpLqkz5PvKgqy+vK2jIIT7Q/Yiu08vd4EOnv7E55izp6X5hJB4oSAfkhE2Az7ihrIhk+r4AbPjGxLY2+PDcouM13qyUZ42nmDbqetXbYt2LIAnrpcen6wLhK0eGv5tTHkOj7ROo6Dt5bAttOANlC0HmmVFIBMYNj8ECZ/Rtz+5XL/7gUGugpg6kA4wE12t5l3ndN1nUsFxFGsMp62I66eGc7mdhU3x+ljugfpHRnNl/WG4B/q5Dh+iqx1+rpelo6NRnEw4uKQdT3PXAOGCr/AVs1/sWlqlzBnnnEgkNDTwFjwJCcwGuu2E8pA2dJXBau+veGX/60U3aMIenM5WCNYbFOi/y8j7NocrbKBGpi8jwUej1tXwVtyZvDgsgDuS2cKK7/8fwyW4IjX38BpNsxD9pog5lHAAvsmPNfwXjp9qNatqwNpuqgCwISL0bBdLbft5HSRXy3mDFbkhHs3M6O4/KNjI4pPxqX7tFHjY2We05lfV0UynRje+er/3XQLze1qY7q/34JYYb0IMg/8+H2ySPeGMcFgmuyiTNjd7R1VKw3Yq5TkTjskPOUEmhPLyeSYgNYIS6NgjISlQ/MpWeNSGv92aM8qcRE/HRqP+KFhcDPvHGnPlAWufB9Zd2rXxLKIN1K3m2uEMwFnj2XxM8J5T0tsgLH7GUmFDMr+xIUvIEMnrhEPPJ2TNOdMTITU7zT7La+PPNXV05xkyljSg/UmCteWGHG7iv1MD/hWWH/YfeV2fR6vJid0zTHwU4ePkrDZpczMG1/YSdHQQZY/NKZMjnhVTV3l0OJiSzGooHWj28j5JIcB5c4fZRXH6t/xsIAC5pehbzMoT7eDfkQCu5XDkBAjCHZ1Lb6JHJY4uN3NHCru0w4qN6/usSGCTfmhQnoShXOgLQcePOtzsySRP+VCv6RxsY9QNbm8moSpA472SnqiwqUSdInO0hf9wYk0Tcnuvr9+YIBjJftCBBTB2DLOjSylWH7Q82DqsDptiOAO6xhtG/Qz8Sns4lcfDzMDu/P8ynHEqY+v57LeehKOpmZnjaSr5EWwFEk35MNJHYIEJeSmXt5hLj4alMQo5rQZA66eMYpdUNMWK0CjW9mNtWiAYBbyP70ZA/CLb56zFQDOgw30MmgE6MSUIyIUeRr0mwCpMr1EyQwYoyhPI4nJZzxqrOaQigUSLD1tZHsZ6Z4eKprYXWsHRKb47ZDt4/bXX3U60/tYcx79Tb4VVQlwGmLoT9X22eUeI+4Wth1nnc+7lNgKyWoInMY+xn62tRU2Y95273+BqLhgprE6/RYb7miyR3WVt7578VZXN6yRKHAudLQl0vJxObqXBI9UyekBmUCOLGSHckWvWCi9TrB7zkI1hyIGLmzN2xriTWdAl0VxUOCSg9qOiDq3gI7jMtcVFgZEgXMqIn8rnRLc3z4D8BOtpyiL1F4kWHRwLUdbZxgp+Lx60ioltnbMGTZRmnaZXjLts3lkajEwaHYMVa6EBpWlvCMdTBITUO2VZ8JZMDut9DE81TuA0Jr6WJrEZD65pGgy9DAykZVJMsP3cqmSopcZo/D3+rlJyFgZ4QrCkuyhGpd5qbTLpYJdI67Ysu9IOCUjyZLwPqiaHdm9dgOYbwzu2seHstseMG2lH5yq6MLvy7G3t7HZlIztEmkEGPAv/jUvutwfmr5GKkiB6HvyIL5aoJrJYP+ZGj57LRYcnSnnLnIjYbKy71PKlLvtgPP5lNGCIq6PP3r2TNnwg5rnkXcAPvYm18ZtkL8m0hZhc9MsGeuFnooduXPsVfymz+hmvY1cF9/LwksPSXcSAsAIXzUaoaKgW1INrA0NqhX/OsEan0maoRt+TJl5UQvBDSq5koclGnltAHqNNzsoTH8Vx8OON3ycTTOxWBhe10fXBSqZ3tjHVjmkD3TouXn8cJ131iJ0T96t4jHdvcM+wmXcVV8a3OfDvimCTC0OmFyYkGh9FPF306XiEJjygUCYW/ITvGso7xST2CP7Sik0dnkZc20jJnM00ZGIJbCkt0SMzr+iZ6pgUjSg+9FVSxZrqBHv+RO1XYK0PWscnYaBWVZS1b99Vq9g/J9MkTx/fpR1Es6yF1TA8Bc57YCrJbHJDyQaoIHK1U0zrbOPxzVLHaMegly1lkA2f3QYhqHnVQkjm8gFyTtxYCrlLA6FJrLApeaSaccBwtYNHX5L0QGIwcDNEEM/7fieShcwmcfOyi8vkyQSU0V8qchXtAHB/zM+oF7eBC4/l/kAJ6BWFBQuP9aoQijyQ+nv5OEDqD9WdwMhxjnOGCSIdKZn6078VdJbeb+UNGepg+XEZpCD3+g5HoMr3Fepz/B4jvTKiUG/x8oGtgzXzprzwNEaPI9lAZ76Jw2nR4uWgtgSYFSt5ts6jfEva+X7Ln5hzMrse+jhjKPQ0qZqpBIg8M7Hf3GKqIncRZ9oLkqZO/8oDZdsDV/rg9RTJZRNw8+UsiI1DYKfkuXj3pO9Do6qZojGQjqlwtHFMiw+P+DwfLdPFv3qTYeSkrmf43sYgl9ThGGZ7zPCE6C5ejJPHeJD199qERybe4lpKdI+lL0fwaezSA0JxyQ5LHgfQ0rUAPvw8o6kP5rTpq5PNd4MMNILS/b4zkWaesMJVYuqxSRWuV8AYYOGtQexkUNVjLD0LX7TaxsG5MwK4WHBKmM217lyrWHffKp3RFsm2dnQYA/x7oPAlIKGVL/eOWmglzWL45yoFBvab1CEuGITE6DETVOe+maxvG3cnVLPXy1oVjYhfU+ICs58xtMqyyvrJk+T8IQwlLFyZZLvTB/CwGh4kLTHR/SXQs8mlfC6CpwtLikHs7875E4QpmV11LPrCYeaqhz5uUThu0tmFfrRbgGnN0Ik2vntL3CFIrY/f67SXstKfL3MqNSvfYjfVEo+A+yluVx5CHBpG+GCpDceP9KG2Lrea4L/39sCfY9DvQM3oRnryr+Ae35J+6AjXvU+iWc95r3Lm6Ba0/cdcW6HjK6WUfk00NyytYbLfKwPxBvQeMM2mxt1Mz72yrW5ansO2jVsgFY7LISRjtvurhJcbwO0igSA2zTuD1basPtySWTMmPwh0tuOcahb8fPOFpZLMV5pWFAR82dyel2bN8RvfnNoiw4lzaCcagSsrWgNtDatju1aLiFai9MhPBkMpF5vPLjQhzF2HEM8TKOJG/UvIzQ2OSAiZ+To4ySD5kE4aU15VC/WRNhuPx5i9QoE74+IG3D4hhKcHdbus6UWHg/yCRGG2f9aa/C8XEKidP122vz7kWXLEIDpq9YVsPyuYMEQeLJobsrTLd4V5hPuBtCAAdE/7k2CYHgA5NjTwu9z9o0CQzfo1HTRxUR6aEHs63VyEsptNxaM6CeydESdLrSBC8f04Y2mfUyVw1dMr+/mOLRvMoA8tHBlusyUBjIp4RAhepW0abaSFEKcVsx4Z5W3Mzq2Djc1t4/qpKXBe61gNjkab2jDwOiieEXlHMiFeLnsgyipJmsHNKjjr97hgviJHLuGOI9rFUqUXtqzFCC8Azb+oQj06i5nhQ7VAd5LOyl/UMjJFZf9kqBWXZ/W+Um8ljCRWbH6HmbwByftCnxXAbxIpcq+491ngRQI5LnK9GhCXI38BR5FQNeSTFfSVBolDXu3Pa7hVVPTgGZ2o9Aa5Ex/+nVpAgG91i4p86pRAi2wk6DcDE9UOfWjI4N6eyW/sT8Qh/nyO2s+GyTacYoHxR7BmgPSUvUI07pUnx1rv5vWQacIxUCbXjtVjNVG/juzOIn7zTOGUEtvfxyTf5JQ3ztPYZ8DpiLrQCy0PGoMbXScoEVbuR0sSfLxr30CkEdXk95DZYzDWckF376IjGFGVvWJmZcfkKzwKQSRg9aa+8f9jsfIKMCGGLNSLW0QpPN97CPmdw7+x9x4dPtIoB41kCnauQBWxxfFJHoaNW6xCK56p+eG5KClQ6L+2c/W2i9J6wMZKfy0oBirdBCgMxbVxJ5A2JNhBsKTm0d5pUvioy+PdpISaaKInvK4YTGy9MGSOS2tf+ud4+Mgs+aBvPXQkq3JAyDQxDfCHuNMqB2JlsobmZLUwJ9UMxiPs3C8Cf40rUpN11uihQHTh7G64Q81qrUR4Mg2BTVpdhMGUNIkYENph7zHdBT6Qpc4SWghyg3YpBj9F226Hkr7qqtupR0tdpZAErg+xvND5t71SPHktzK5Wp9aHlfu1FedlExe8mdlId2rn8tZC0w1FT14I1OlUBAtRsckkBUjls8WuJIlhrTtiMk3C9Qm1IsOI+onlq9D+d6OTMKpnT6DGKSrqimO/o1otBTHXaqXZcRtrt7yEtIB6WyF0M3y4zMWUR1/xuDZ8ugPFH/US3IdlGuu/YYrqRontA3fyzOJxW+kkHzJ4hca/UdCcCSx712AWwsll6qJ0sTfgjjoaUkhL8unLTcVeJ3gIO0OCuEyCsWM9BSPvWgQPLMEkAJHovwiRlKXarnXYFYwHYjl17kKu25VutAMU4GfJKdpceCu6/OUkwdKk9PdcKVY8KkVUlAT7wiH4do4aby0nHRzfexln9fp6O0062vKgO+vxVYYmVeJ4MiNrbWL9T1t+CANNex4qsHB7Qso3+3NeuY3wITK2QrdTC3emL8XNAGIYQDdgdLyGjRbyGniQo0jK8PmswVE48R7D0/adJlbeKqQTVN5Erz/CX9ls6jjyfvtX/rS1EAb69gYiqoHT4H4aFaemSGE0+RoaRerKQ9bpZdSL9tvemo//WjNc/YKZQz7i4S5BLQPyUIeXgvIIxSgyM4EfmTMp6cDtRw36U7/3b3dvchCZ2ch3Trqm8nVuTvXj6nsx9Rw+PFeZbUElQSTA/1Mg477UKeG61RK8hEgnKGfCP8DuO5Qd1d9oyAOT6T+/YA+0fKK8HCGLLUeMBSsRHoawt2QJ1W2OQ2ngAdoBQuYAClGF8INQa5PfeuA3VfmiYvrjtBm/5fyZbYaRpRe3TquLAKgsywFPV/pEtXnrUowhiyoYxWo/kydho8f/5p4B/wQHte9JIsI18nqxvUx00BCkezIBomOZzsUNdhaZIxXIhXSUz9iUcxloopJGn7AbXmLRmUZm+PPATiW9SLfv2XXfkOe+ZjAhKW0WuT35yl3ZgUSLJP6Ie9qmKdtrc4Ss2/thXkW6FTieOPupxnLdVaZJdcuV6FrBpLRrz6EYH1pmDiXFeZ+iYFF3/0RVPS6pz2BwZQJYAKxd0NgvHBRO7Py3TRPD4XQTb5h3PyxRPzXrnYur9OY9i4SeQqRkkqb52zf2K05CukeT15oJUPPuk3G3VmsN9/g/R/7W3HjNqtt3+8PCiKl3SiH1yqOgOwqd/AgepRBgjWwUbPt0wEa4SqhS/0h5M+G6lJn5vKl10r9aFKuP4F7rp0RDW9u6CJqk7SbjDvKrmUkn2n41ZwjQqN3rm/81L91FK+vCouoEgAJBz01s7408JKAmksy6aMfFHGy5GZ5vPfsZSzi41T45/pHKxRaqjVkB8cE+lSKG9UybQtS4zceYNabPukRUi7Y4i7PHgKHlRPZgnu2C652/VDRkO+av5vhOQo8ZsvO4w9LhQygXdVatxS3Ubk7A7UsEp5Syf/o0HX1sDVv4qaJqkFLWNVhqn87eqSwFn+WV3zVMLRviuhoJLBZIQaISWwCJVPIbzFTjGK2QeWpfA0XTzAD/t69X/TTjOhQnrLe/amLhw5eKVnuYEZbJs3QDEjepTIR1sreZ+tbYApO5ucoetBvRz4gCNbFqwiKRt07O7C6QQ0sT4sDKaj/GbHU+keHgNSIFwTKBBCdxtRi23MIrKetiNfRaMY17I4CifSFtLp6JBJwFQE9ikJUfBcN8ApJTOoM4ojslwueYbeEDnef2OqK/Jl97PHP+qUfPQ/x1iBsBzR9vpCcAh5QcEINbyzvKrK9kzK+AMChgXrBmAWJHrYisL4ybIYNBAfQaFfE1jCF0uj8hFAZRMRCIV7Aby7Vew47XKalA3PlgEbpXms2h6yyryRQakq6yVrpNTiAELmY85JNTKrxADHnemUxPwSUCtJ41OKU/ZQy4ulanvakJw15JGK0nVwu79n3dNnNh3PaQd25PkqekjPhYYOTksjL7SuL33H60FE3GjW8s587WXqdlyLIahw3VK2iYKWb4VQWPScp5znr8MpJSM04o46Igvf7o9GK7Q8IsfqyY9q5WoaDivBxkH4/D8M+OQswS+hmPH7eNlMq4x4kRLtY1vAvTm8xoHEuxdWLcuSaqT1ZR6twHMbaV2VbiY41TkC7btFQyP6f6RiR/qzIru7MlTWq24w9jmeRmQWQs3y8jD2DmUJ7sIAjxATnHPwfQRfcXAUUHBFcW6g3aBCGYchBQBRRiRdwbMJ0uB44jRPQ21rDO1lwBHSAN5+h3FqGatZ2Ghb6EcbOGPR5KFhfMMkM6ctdE3BdVVOcgUPYkxvp609sv4prGtbfqJsKs3Mnhtmv1O3243hrHz5kAG6ZaBY33nySGs9SJB7XDZXQJQ9FHf63am53Pt1bYy9Y2OWDVp1m+LEvpb0NHOLQ69NZMePDd57w89L0Q0ylGNWI5WaY8sBaFjxhO/lgrvP3nPuNmRVhZCD4Sgoj8AF2f0Kv20lBve2tkVkkY06iYauMfOiuIdU69tKzE87wbDlJDh2pZAO3imNh8uDx2okrRA+gZA78wKOkZ2PH8vEHWVRKCY1LKIPitUVSfrXU9Kx6xMQbSh6PivxOk2qL1thp9L8BTuVzVkPEAhSkUfKVoMlZZLP5lA+xKOt6G/GOogEqKiLTap/Oz2pTW1pimln8Iclui5S7ezGgvEIpx6ltOibSRaS361sM4RBmIyGmo4dBn+N0K5V4km5yDIKYtU5EmPgfrtG/VTEwzvLURnlBdXUkhkvZcGRhon3QmyepWvKel5tmz6otHX/TGcScZOFcZSemJbUxyMpCXcq1zT2llat5afItmmlhvIKeJ+PvnjE0/+6FC87feQw+k3TOSjz6m2zY92uz7ii0Ge1QVaPpxRJCr6biIgevQbuOpaWlTKpbtJY/90v26t6s9Y0rXylS0jfbTs1x8Xyz0E/pQD4+zV57TvjWSo1TteWj1Jcry9mKWkBQhLmrTgSqV9j89PL8BrY1hYDkPFFcQEFvcMyhZxE5cmaSYRTSQ12vqtpDPi3JJf4EBY4XDcJZpNJs6OkygqH1RXlpTo1O9Pi8ghJDi2jYAHQop1ugaDU9JFBIQ6y9eG5Ng3IQSo3u9khvMn35OMYo8H3NMWlhfLGK4NeS2vJLXYeMrR77jesYQMDxcyxhJvV+kBxVpPSYiUZo8ldAXuihXUSnsJ6VdEpu6qiZrJQBCTse0cu7O3NzV6PanWJ1Alik9yL+t7g2wHgvRtvn2EcALwlXg54cTIFheOMb1npsZAR5CXAmrMf1KojHC0Nd9cNJ8u/akGEaOgdUmtITqg8dBGin6UlQ+MCRhWAr4jD0ZUHUkmwqNOBs5geHhRvrq7ixCAcYYOJQwFAbczjmxLeBKAml9djnpFKa+5QG9RsLRu2b1yhelHJJHKIAWZN3R6VK5KTlVvb5GsWU4gg+JIiAOiKkaNjsl2fVLM+lE4w3C2DiefIDtQoayQw/ycDVRW6yEQ+/G67rhmuxLQFfzmYpBluZfA+QNKt4SxngwQI6a8DduxNYE3GUL764XSU+GA1MKRet84iKGXmANJidnvnTAOEEruD8B0liU3zzTMuizmsn+46ekIMbNn8Oj2MbzwgGHLsG8dz/T2vFv6MXUdW4o46JinscqD1nIp2EtWEFuejCE76U66qcZBxa8UpEuKYH+lc+uuTFhMyRYl6BAFrmXV4fLyUsdJW5RSjYBzRJPfUS5/7ZqsyhJnU1XRAvMZZ9oM6GlbNjgALLdVHzpFSvlVtpZuOMP7nomE/uuUcvtAiFDLPMrUpPutfYjVdpl1yyx00ahW8TN8i2xEcRgUSi7a9AZ6QhWd8iZDXHNrrQ8+wuEIh2+/KxcbrLWPOc0sPSEo9kpvhDMjTBg03WlIC3chr54UjC7Sx7zO0OBJjfsPY8aW7WP585RRtj8N+hxx2KAOAJG9c0k1luyrpA27I2A5qlipbhOcMlSBH8xPQ9yevPdkjvupIkwxsh10xT7xOBzNs4Tpnobzisx0aiUONOzFEQM1SVGztU5or9jZ9c92EhOcLZr/M3OWNMt1sujQdhWU2uQjRicTyqIFToSbzM29YsQUmNfE3Z7Ie7lsng/35paWLPjHqFH/Mf0iSPC1ctDSH7/vrEpWsQzPYXmroQyYf10eX1WEeuuBJ7Vrob9bukssPwn2DWCpMYGl9q2WmBLzeRALbe6ccnGVhQSeQBdwagFIey+6UYsaQMKmCpDPInGfqYW5ypAeiUp7gAE9fAy/4FcoyXSRXwwcMtdxsSDJbuAsVxKfYrHAZwSd++uPAXn9bFXju6o3985yCY048ACPa4PxoRazaeBzGuQm3O2wHZ7Au3GAYuqt1GoNL/UZUX5YUT7HXj9/+ZSKA0y0J5RiN1ZPEwrAD/3YnLF4xbVHYK+XAJwJjp1PcFPwxjMe4xsZN0YOckaYGHy/G4SSStd2Zyc4kH2IRdUgg1PbInWqgMgpKhfhepwMD3yRAvbLN2nW8K/LWBx0gEsqmhmz5cdaXd59ULEQtu1WmsxAunnFdMM1zim1pH4bPnOA33gMjPd2Ph+a8RKubUJtTLygiTyMiiYbxIlLwQEBh6S87IYBVS8/7ZmSS/gYkhesBYbyYvisE1s4DNZIZqMujwAiuF9vS7BnwgQNTldyechhyZIJ74pDDdjmE+CCp83vlSf6Av1q2TPRMd+GOWA6tYt74BbSSWXqCWYPTZORWYtSZ/TuZWv7CvJwfEJhMCYQE2t2TLG+lbmBtqphTNDXF+2Hy32Mpc+TSoEalRWWEySk4QVx2CjoV5yzS9PJyggsqRG8VjQJB4uuGWx5FhD8tAXbZ/gJ3YTy94FLfchJ0QdlVKcS/6Nwm2B+vJkiHpPiuBJ1bowCJ2jLFO0mJNqSBekKQMaF8NcjaN5GUTs94lt3h+TzyzJGFL4W75nVjl3QOZkdJoEMBhS9On26LAjMFtShmpDwZix6TSH4lwGkYQf2SPf2geTUnbol6+bDo2qCnMD8LFax9ZmpHMaUnvqUurNjWt1S9SpaDIrGPeF+dj5gSg9Eoa3OlYGUer9WoJKYVzZdosFpi0xBNwhwO+sI3dVA/+BTtb62jAvMRlCx81ndNxTJW2/T8S/YYTqEAzXFhK5b/n+k1mlarncnBF1De16ACEdUZfKvZmHZV/M3XwVybxTMyRv41pKB+Py5/y/cEbJR4qgWPwbtoNQgnskUJh5dagjcbg8gz9dhZ4fNNKBuAqzTnOYPZv+v11VxlkOJXFGeZg5yblV456w9wbN/wWzWpeikcqJkzxsT/ytK/ND++/Bmrx24iO2mGPtfIWpM/BOgd6EBAoM+S/dXvcV5aEzDT19jIn2tZmD4aVNYluK2NyW2q3QuptYCeqA5SenQyvBl/mYxI0AE5XcwVuOuy2s46NXpsKZunf8aScLq7qtjOPfYXgJqbifPyzDgQDvN8qITkECTz/e3vgD33CkikgsRlFhsCTHSluLtIa8/2Q5sDLAKcArn6knpVQiyT4T67T2/69x2b8TlmIjFl1cnSmVt2nL3JQOgu7wUntmsa3a+3G0kWT9aFmqTAhx3xxmnQfNAIpc88ZhfbzlTB0qKsZ4zQRKBe9LqABxoa33BcNSQioJYdNLAy6HObrDTYEnIw6hkQfV815o0MrvJ910rZT77im9jz22J7/WRtUYmuszD4w2kQXiGMOVLemDlu+yed2m4vXL53D6PvFxDAv5dGAtAFkXeLDGX+Q3BUPIOLRmTU2JCBSOqCjwtLN90rDj2MkBhSvHbWmypwhr2vtPQcTk1zDHoEONkPVXlsWKd1A4S0r/zYXYvE2CVVZe4Fe75iNv0Y+ix94xlIQhag+Rf7/TLNeNC+I1txgyUQHkQCV6eJbXs3MTKtfaakWDIxEiVqynDJVwKGcku4DNBhQzQq6mfkorXIfPDFBYkJowtjINI0ecOjTWlcU1YoSiY3h/DTFidXJS0QlnAAezG41mWU/Tj7W5xVYys2t+VGDR/bAvCZAkNk4R+KbRHA8zMhg1Z+sWMKJpLAukX0qlIUYarbo2pdj53wEYNHJXSXYxqFZ23qIeCCLi6MBwVYfqZCSqW6DV+Fc8gItI4fVnhOIRzVjdlan1SLKr9070X4aQcR56oKiqButzwV8qhfQwttouBOJnQcEoWi+Jr29N/W1TGHiKKSL4CC1G+/TBkYbBXqGF29pxpmtJfpdGQoUbIC6udKitcAu6Pv3CxlFf95kzcQBtGke4JiOrS84iKMVGjMjUjaxivIV6lZeAH9mMx0T524P4W7V7+ErlQdtwCLlSmLTW35pFI1G5dCOuYFIkVpfBt7skn6xsr9NTjCrnzjQTkqZ81fRHS+PcS0ml/jK5oMVeDFSju00ZAcFNwaj9aQ27PF6vvKm9m2OZy1Agdmiit/TJBz1j5qQq3Ml7hMRWCZXjwTePKUX9caplLCb6NyQZ/9/fXysbwibifqeOQ9e+baPrEQIEdRC+6V/P4x5+Ru4hj7JFlo1GdyE0yPSz67PWob93wqOhUvKe/HZvwdfu3xDb3kHnGeLEI8e3R4+cGMfzrAW1lsDH2QVkjvYPLB+6jL3SB7mNhozMtvQzwKJtBLd5CM1LiywsJV0xQrW4AmE396+UjJL7NxNM8H0mFCqgeGxBFFs20wFLh+KY/sXq+dW2kFHM8VuzVhw6JMuheOSQlFPgPKaRswgKehYLkaR9BKXs6N8ysBLd7vEhK87gTd4g2W6pyp/BdxQeRyemjLlClE8L0JtUpmQDgLOb84unThydhJE16fgwUVDV/T3qdDp6vjcMggARE12H9kZgkePzVM/qPJ3pyZt90vZpou/4eri8nXjn+3HG/j+apSlWF2DlYiJsvfzWxJ8tOFFfIW3SztBck9tJHGhEaXJZ0Noly2j94Qmk7FRtO+ZgRuomsrcqo9YMaD/uzOg8NGU802+3IOYWRTwb4jtZXN0BiRY4TaNvRO1l882xw2ZhuMXNiivpnzHjipxdnfpQaJ1BqJMkBDrVEug1OK0asmUvYYjXEnJ3S+1IejECHyVvD14JJ9Q/gPgkffQz1738VkIFqbE0cGB1ISBWttuKieIdy5kg4LNtW44gRxmw/BOUPO+rec8b7NSMf54ORUcNPwpfW27a3UFO7wGzXFdw1BJBMuP3rpRZUAA/8roDTwr3zI86Z8hOrUwPD3dkl5hHHm9MFn0rnVh5hJgPX74nwtVHVoDI+i21kCsEBGMOlq+7GU4AjR+1QfPS5kri2ddhj4Fh7rsMoo1fsw5dkwZPw293f3WI0BbixgmpGZuLv3fjS6U8xo7/IvySVnf4oxylPdAEBtMu6+feQZGcxhvOPGXtRexCpNylfyvfTr9ee1QdT6+gDFzOCGfHP+AkqN0vCT9aZHwFfovJN+GhPuYdpqfVoL8REbzkJ5jeqbs/s9Gnb1l1nytxMvHHU5g2u5sSONWKLO6P9woF6W/01YiCcENdOX4d14U0VpXiK0YA1ssk27NllgX8PEKlxT/yVdYrcmMTjjuWyvLQ7zDD/tQ4qoOM77h0M3JWS7HbWh8JJOAEU4WEYzlY/KW/HArv+KMPPp+KzkeVyPvu6F9mA0L06QIITi7UCKlkv6gn3CuJXVt4dss2Os+L1xf2g/nPBEUpPBsPyAyXQklUOHja8fGOdCDkEuNhC/8CLT+yiJKyNm0J9z85kQeZTen5rdn2CxRbQ2sJEZlPqP34PfOrN66wLu7s8FoMOHxwhultNMRzsp6P2KTFL+IMlZ5s2tHzTKKsGlGFFojlJ8FegLnFarTo+HOhwwvNmN576znXJTrH5RWP+oxiybCbiHv3B3MTNz3gYCZlRUCH1hyuprDfuvHmx/n7pAKIB7GNJRGw7fMBtZe5FuWOG/EdYv1P+wCcRgxneBaV1XA8+t6UYSgxftmlboxptpc8dm0aDxi96wJPcNJeXYqyThzch92XsbJ57DhoJgZPZbVELegfDQwqgJyYbQerEzMBRENYya3WjqbFur65KtSm77j0vrb/GxItD1nmqv8uCFyr96jl9zJHPE5GkGqUX/kdMqQbUWshwb+WZot4J5JzyWbEf/W7G7p0VGj/OWb825gTfxC2sW9gJo3HWyH0xCQ2KpvE5PVYeP4vl2yA1jrd+taCzDvBXmEy4cH4k0aDnWPzlT5s8jpJgOcD++u+rP6BQzsDqNlaL7/kMTbX3Zu8WJ6tfZ/BFa6XKyBbZYbtOaYMWmr30TKw71WmtwvJgUCDWHTkSeUaDAJTfDLILFn0MY4MJTVPNuwryUWq6raBVxOdLQ8/4CXKKs06YnCOEW1ms7fGzkh/r5+sRNebO3wfrk/CnykTcKLWKUbfSPKprtXcedfTnR08LxQBUqIr0L3irsEEQOefuzt6r9DdX3CtuIxtqykWPCosfS6Du7Ha9MvcqfoqweovWofQRa1SijVJyuukJiz36PxJpMpQkdRuG3RfvQOMVdJW0D0GH/hX65uYEW82g+rpkLJRa1fmXxbbLlUCvIBEMd+eCYyqQKsVes9MqkCmSl37u6B5WkLhc6USrhyjanUWU0L6vDVERiYWScGWSOxKAgJBv5p1isA2dyAi8okNSIOvywRwYUYihMbzlruzlfjymodMW2AYX8jyvnKpWVVes+77XJzaOhOjmfHXMybaF6RqDoOVh0Ax/dSupDCL4rCR9323caw5fP2RqeK+EhexC9fUHCZ+QjJ54cXE3EyfgIYU6DyAZ/iHUsmr1uW9DoygcCjcVJVVc48vx0/JvPrv9r719tfT8lEMLgwKieaFYbfy0nEpmRuuPQBRwvTvTQhqU9L9G4+Pp8ktWH5qwX9WI0KtMNbhfhqUhtK2ezJjJzbHORWIV3q/9MfneOi3Rg6ZWwJjUR2G7SW4zF/Z2lm963GBW9DgxIww3xylv32Bp+wiy03D387sUZNHYBSm98CQgW2cyrExRfIyV4X0fB5c0MguLfPl80pMUUL5+E7YJUoQZ0A69TvcFPrNcq/9R2jp+wXIHCubKgqsRuP4xFRgDBYSykkAsWBSyroy1Sf13/98VX/nEsVtW3JaqPmRHovNMK44SM1EUkQk/Ia5qvPUNcnehP8SPHCfHLgKLYc1XRy3MbYs2I9lqCPUO1h+w0L4j+GIukxurofAP/O7z3VCzyh2dsZ21RqzauiVWCdNHoCWxWCvkCFou44xw0wZkAnRUlA0+wIbOnmBn3Opop980dVmEoFpvdqqOWg2uZIDEOWU1ekdf03rPFQInUh2/1ak72M362R7FUmASiOkzmWyinhGxMvJJDnvstmnGjPTiSeRdoognNIrdJOjgPXEtNC3NntQGnLGi31uCivahYLyEGsUXy/V52ps+VZiVWcoJKG0F0NBGYGtR4Je9ETSsAdqj1vOuWF7UcJL0wK6yBS4/rhs1YLugTQSQxyAhywU9/FazqeRkpIdis9HEPsSCXjYiaJWrLuAoMn5uXmdnqcCD30OmheGWXAkpgMu0OqrW+WxRZhtXQjT+wzHPaI+d0+knNqLyINbBTFoWKJ9eVC+PSuJ8CFlDpyRlGB54YzCxkggacxHc8ATs/6lRbJyFSs+FtXo6vwObb+6cAX0o2ZZDS3Godi1mJu5Z4FgPP+OwtV0o4s2ICgYk7+dZYD8wL5F4Mm4bmNivkQcraAJGdrTg0cdokdaIeeMhTc92cbueBMsyTC9rSVvPc5rYkNccsTuMhOQz/TPif1nMY1TafFzD8moCRAne5jFuG5PvkStIAQG1yyF3WQDsZir7lCQ7V1i7ouErRaCP+V5vYU8T/Cy4kiHnIJTmPCnGtwqmDKVaESn4N/5sLTQnkuyJ9QyJSZyOESDPvW1x0QKDrolOw07ujYwuDPeMMe3MgJYglVTyBBQii5+qc1jlz9xIDdE0SBrDPKwZqvkpvP6EIaCw532pJki10kmJ0wXjb39d3t0z23gFXn5bixvqoz7NYno/5oYPr2XTZxGjHjQLHMImclpRqEvWETHDND1J62EIoXxV1dTNf910WoY2cCjm/Aq87C0IzwMW90ZXWfqyT10H8XtKx3fO1H4b7H1PcL8ZpyRYBZWo7NO0qv05zaMr09lYwJRWcHwXenCs+A4GhMeDiS0tPvlT64bkoRWj8EOUq34SKBG5uk3QLMmB+uWf7pA74rZpj+hVYx9eyFxjhUwUhsZ7YddG7Qw6JbsrLDQNgey5Hp5ozt/5GYl9AtNW266OCGjsr0sEl409e3wu4nfOh5i46DfeDFsK2Y7pBFBiuYla9c5jM9bwRMITUY4sGviXKrCatU1fNdl6UBtvqIBGHWuf0uMNaNG3jEsitVMDjdBWuMxfk+qhSyYh2oiuFhY4l87gJsaz+IxxhnH5zMtHuvUpfbN2/YsssTcJu8HvQJkVH67iMcfgByOGYIsmAehdJPka8nmDSbT3fbRoforRK3xipCOcTYdJStZWPOweHkykPhj7eWRQzyrrBfB2S3BUuyIfiMZ7JPPyE+jeC0vd9VUpPyMox2eQY1c0Y6OsbuBX9MqNZidjaK5STjjoHzqThOevklMyXq132ZxIsqFd+P1QqitYTxJa8lECOicHjKkOng18mDlug0EPnI788PpuUXhkHnmkxkUOClpryUvVcryPM1g/Dx1ieSUw00ipg5aZLLxavdh+q0wOseGOnj8546iCdOCst4BjQMAsreMrhKhoWWNLEO0dAXEl4UOPVww1YofzAqEYmbCb8Veejwbn9dt1tXj4uF/iHWUrDehelAG4rEbC/CTr9U/jZ9KgQAOi+Mcg/alnSGJjwQ/qrc62p6ygTp3Iv4I6504Nc2gM26+9gi1tv0phjMTHvhTvCXiqvNlABjBV45653uz76vfu9H3ThO6had4l35wCE3+O8FkBFvyptAQaCc3nJgfcdeNVtF5orKLwoQw2IQb6ej9+AEXO+23YSyjsXSjeGYNT6GB8+M35DFpNV/LoSAT2tQxofhGSUVlo1nSiU/PJIi72QaBOIcPLh0xaFXv/BZNO9AxnAWWAWUosdmL+4n5KfV0bPwuOH9LrdPYc3fBib2i/ioR85l6t2NJsBuMq0O/+ccdJiFfp0A8jS+pl8jhSaATB3wz0JPZxJjJ3NGSpAFq2KT9p+A5OE0jS5WlhwnFaiQyFcY02IFZvjGtgI81cUfy5G2J3heSXNotQDvgRMnX8WCTyyuwCzBzFb+vWjxfJeBRUdYopdA87aFkxHwQZhl5WftraQKBIG6bk8nJ5Hz1VbQU2p/HeUKF5r4XcvG0tstjCUvG9/QviIhrV+sL9F4OR+eLCyAxikOnZAJArYM0LLnqiDLDV8/DA6wZL+fuaYnhNln4iKQgLcn/1t/dci/Ns907aUEGWKowb2fsD0mUQDl1cq/NzehQQhvvI48r2b9xemfgdOBENvN6KRCmM8wNhzGoZku3u8bJkidufrPAiWC4sSqUy/P0gRBcsIx8TTiayYqPIn+WoLzz/qLa3wV7tC+6P0DDQ7GgArfsvlDtx1bQs1gsNW8RNJeBo2rmcxDIcSkxsSaPqD0D3re33HxIm1mN0LRtfaQqTB63qUA4nKW+EJWM+lg8RTIQrSsCUqAN5joTTp39/P+Ri7lcDSKLWnhb7+LbBuTPduZPAJ7t1S/zvuRtKUv4iH3bYPeiLrig4+6P8n7FhkE1+Co4QbV+aWrS3Gb0Nlh5ArI97XZ44bG085KHBKxIlBrH/jVAsQV7joSePou6gOxJkVOJ/YnBXcEvolQzwxMDm5lfkO0KSonL8QHFjgoSWAj48kPmsc7b56p/1GsnkxTe1zTGn6HW8aB9lA0luihV0WZvUMgZpjZZ+7WhXmlCYNLLb+WtSCDLIBq4gQehQvtZunOgnGkW8y0wAq0BI/5r5Gfe3mjiZqKPIsmAXs/yCHcMNjFs0eCtGUpQJN0GhPx/t3ZOhfY81P+1ei3dLE+k04QnSSzGouEWOzrGiqafUvGw8ix1DuaQf8lmR/GApeltos06AhggUa/L0oQ/mBDIuJqUGnprwtfDXX/edu6yBTTFQdXAYlqmDo/M6wZoUIO6JmqO6WoJCpFozP4eI1D1UBybCRtWjGJxPo9ejqxulIGWds7uetK+Xrp5zu9LqH9AfmqTBGLicb8aCkbhC5aNSmLHZY9jbDKPrVectMnPm+h4eprD0Feyy3sak0KQv/WHND6vLUSYXEhxg3CdY9PEB3ElsRVj130mzikajuFhLDSTV7iM1EGjZp0RMylrRUvIB5RaeWoUtEcqv8dZKF4p0uHhJiYmWgfLTOC3WyZyM113DQQafzfKuVUxwQ9CwMDDfcQMQl1vr1SCwOjmr+bgubAVutpuKEG4y1HMT4Sj0HSscUtjK5CPESWQlYxAym4JOuu3F7X6L7JVn3w4tzB1rilxmjbtUSOsAeVOz2qo3VeFn97xPK57WW6Ubt8BL5xbUS5ECFdEGpQOBWb9YjzIGdoVxDLwHPFtJhh6gVJZmhVtSgMDh5gWFHvsMToJg9wVWG7dzB3qTylRWdP1b0UDrtoX6e1cDu33w72VeUG1FrSady6YkaOi2ejgcVbjhhRY5sXPEBWmwOpo82Y2JH+gaAOIZEk6BsnkODACa/vdSYUBwVrwSL1K4x4bxHKSnI0vU2N2ElLXoUjWln/UcDi7t8nOToZn3iRgWACLAuQVS2OWHAX7PFvy9bnDZLGj9Fr02o9IAMCNGvg2NW8EMv2gamO5K7nc54CujBquXPZPjenBugAotlw28fSBOKvv9iAATLWj1q6QnZsoCS77lmDuGE0stWaX1Ekq5u0+cJFNhxTJ2Y28AQzeQzmWQvHINo7TD18LbyEYVBaXAgqlG4m0efYTeQszOPB1fE90APwuvHtfK6csu5/HiVwJqhn4bD593RMK8lawMYt2vNePcydH7FlArYMZBqA9ZsfVBMtZX/kXLIx64P/1KX41qlPFTA080zfREgTcVqAXri9zgzf8A9y9n1Ags4vPviBu1k6IrR89bxijpxsqY58ur5sYLlB4nRofVf4izkhDSZJVfwy5Qb5B7m/bD6LnPq1GftXDQfOs0Q87jUWfIE8MgsgUhlKccOEU9NAtZEmlKoyEluV/+Eyr+CXa3uJQ54NbJynFoRF+9m8FyaVtlEdBO+/tmc+6ur2+Nr09R7bKnw/f2MFuRpVOvWoY+cVlCoAZE8QrcJ4RC25k2gyjBnReAnMKP2XDGJWoiokNE3xeBs4iErVAcJNcvW/r9STkpP2f/KAOfnIpsv5bFR7nxxj8jJNRdPmKmZelTGlppEXboPEdsGeIvAnJ8ywzBP77uLXIYtQOM+dEsmUM1UDS/N9jjULz693wsVGPjfHNAvkqVEtzuaB4xTdQ3jlT9Fw22FIR8ec9MUD7KhLN+Ifd7Fskz0fgkY/o5f83mOLvzN3D7qzITMgxWFPgURI1eRAyHfHERtx4isXF2ZdYdNPYKhHApG5IieuFq1RKYYAMm3+FMijNkn0Iaksi7lk7GseUwAEgdJYYzERCead9lkCY4z7TeFZub/uN/uc6ZeQ1yPcKXk3ZJ6RQp+NqpU5gZ4/CmUkMDHU+pQ1MYJFFrKYkcnlVYthhvIpMvokWDCMF6C2qENrNc0oKxNWZ8VXul/YfLRfyOOQQypiQh7BRaeGJOvWgS/9o9V7ZQV6JQGSHNMPIEuwHhulFGN3qCAo/NEDj0vz3yDxAmCSK9Z+wXxjfbotV0k95zphduCrHBwfFKYCyJv8lsjNzFymYIispBrUqi0Hy3E2PijP881OCZENf9BT2CCtno3x8ZXOCaFihJNXAGssu4V5znfeEOfBDxjf1WwHwah1rfrMVc6egMubulyqm+9qdPsCHe1FWw7a3Mr+nJuUiF76t+iME30GvIbP9BbBSxN5MqvbEfQFDjXo6zDwxtubCbL/2eiI3ga4qFj9RqtFp5jjwjNitSX+6mQU7YmBGmNh0Hfc1mqubovVwwgfrJyxcxaXBAPD83cSuz6rq/dCfnGQA6ubL1kwllXlQk/fzA5YettLLCG1E8uIxCjV3hf5AcTGkGPbu7XvPAIok3mJUjvk1dgyt4pjAuGd7dR4ffpzGyUgM6rYAJqlLSHVwcL7TvR1kbhY8z9J6yKBmFs2+nu3AqoQKUTSa2fB9aV1hbC67dMgGKET+MYvQ3hsjO/Gs/TML4DaaBCOagKnPTjY+XFrswP+YRAul0xu1XTWhYziuBeI3qm8m/TvzBnIHMkisd1PCokSWThWCVHr6H+d9R7pqii6BcAZbUss/T4Cib/vtZLKzOuoNWLnanWETgtWWVbcMl6+xYGeDqmDJWDshybZTpOv/A2AWsly48om/hcUhqdMNEYG6xXTkDNn2qz9WvOzeIuyI6nV+0IaHO/mhEyk3z36t7OW4MU/zZeulBqFzSMdBWoQIBzyIPNHmuiPx6bHUwovkEbn6bfnDEeu+zJZqvEoHd6mlNLMhV6R6MkrxvseAAA7Uts8SBvAAo1ZUsFvB3LshzXv2G47ZLE6+5FSgoRK5BH6Zw3Dq5ybqVlbrelTQ6v9yiwsTZyp5p+tDJslOq1HK2qPxOKf3dE2sk6S9ClWeJhNeRPKqe+of/vjHFHP2ujFQqTwWwsqtVUkox7DeswR/N7WScsMJGBlEN5OR81O/lKaAK6za7TEktVgSBe4SexKG22RHly40EZrgBfoX4MxLM6jKZ7XFfytl/naVY99LfUYkajC4i1oZ6IbC29qbzmIFiOms1rDiMnSeC7eULwFXeLtNm+HoSJbXSUg0seY/A8/QJXjdwBRdnaS0gAqhFcP5tDb18X0QW6bT0d/haFChQ2oXYQk6x34UlXLiqBhb7u9IX/FTGGaV70Thr6cODNNGjxi/1u59lrv0Rjfl30CfntcF1e90F3krJVp2oA2TKxd0RTH5ULFx0Zk8pP5svGFl34dovz6qjRuCfG822Rl5YD0c1mB8oUseKgpvW098gGhm1NW3X7EVN3DKHo1U4vsk91daFWlPIyQrEGvGnmZtBTpk/v2eHQaj9fSxtG/YwNhrQ3slpDRt5fy+BLkYRk5PFJrTPcVIitwyleNgdgCJvmUil77Z0BlcITsyCpd1FoabGMkhv1whkFJ+KA6wdVRyRE9u7HNj52E1FQpE+0PtpVqpNJsbv1VPhTNwvUH10779E6TDpkEmeGloQ87I/RorAtvmC9Y+9iFp+3bbJgJgsIMa5Z+i21lQrPeA7DmcTflvuzRAplKkK1AdjpKXd1RRSClgVyCvkieATXTETga87g509frvWN3JQt4ADOdsRau9OtxfkaI4Zq+zkxVWLToTB533VRM7txZPar1irt5GLGTE2cV4EugJnPItdPTk/7psJBvRYDDjMrUEdodsuZEjZOyqvr29CYf8Z+iPc5v8UO5ca8XJSMirFL/JTT0ktvdId3fMNh7pNd3jPsnpG7ujg7vgwSeIrjI2zMKWCm+Vtc0ThRnoeuHIPC5XXxeQqkuVabEGino0qOCdvwSKdcNYH0xELynmcYDfLXRYePU4T2VzDgqxlzKRjSHHZ5vAMOapl9oWtUTV631W0thPHaskzoj3hut7sBLJedMMZNoHz5sdByfWAOYRxg6v7UT3vWjZOliSTbfhgBN8dEpGASMp3HcXIFho5VavCVHZUxIvuBVASpzXkrdYdgKYD++/L1nrmAIVWXyRMTmGlLgTaLbTFWGFlPnOL+twQ/B6vCZSsLBLI6/rN5M7Tmmz9UeWVShic4h0hGTLw5jNBqH0z/bMLlHQAnF9lTtOCBAmfahUCY1W8SZFAtizRvx0q5dnaGcRnFwJHSbSb8nhG0yJYqu2MlUVFwH+FySJYYwAm39fvC1s69RGvg+5OjI6o5Slo0FIglMxd82O8h9BVAmH2F0B/P6mixxu2wUb33Q8tXCqEu0IgVSPwU03EPXLtkWNDIf2zvRLznRLNyR0SPkuTGXupyYT87gc5/SQXbIbNPMzWCkseyJQatDTZMpTGOF0dR83M9MR2rt6MkVWHan08XKugPa6dPsppe8Q2ajgN2uIGuuFSn41Sx31u6I64P1T1nwGlDvttHLIik8w3B9/9fwVkKuJPWG4arrIEV1egJ9FQpBIvaNFD0OBcxtGvjOGURDOvFmUWZdQIqdMAlBo33dod+BsnFD55XNGTDqHIACq7flhgocUBF+vGg6VYAm1oUk2qMyQT2Fhd4325U4LekoQafqYrplaZnjvbVNN0R9R1Hkpz1TMwpUJVGhZhIsbYrASXtKg/V6+GGJIQhuXdq1xFnTHDUvLr65gLimgodUVcJ+0rmxXBi0Qq0xZ8DaHlo07p0TlCiSDTOE9nQ9IIohhI5dQ4VmFLM3kHP3aKXxhMU8NJeifn6McQ0fdlTqGH5Ep25Kj2dvtKbtUgfRBLkxTEv43epQk6lKDRkW180Ufi3DfcYJhELD8AovN89RjAF9Jng0f6m0KsXY4F5rFSvy/tKCTfH49gu85Dg1/HSXptl3LQYgsqVsC+xgij/U5Om1VEUElcdZbB9ARtyf0SzPLbuJUTegPm6NX03kfsbmdphFLPDmrarres8dkkcXBdqzT8pGbqxQ4G5K8ix7y+eoQXri/WVKlZ7wY6ak4ipuQTR/lg2HvWaZZ3haxbt3cuQwwQivmY+mdD9x+oRRN45kVvvWM1Z2U/BOH/HN0z2pCV4XY06TAXOxFMSxvfTDUA+9QbV70N5xKarjK/Fvsd+0OoIA715Bei/m5z9D9ba5+/OuFkmYiFMaDcHP2UqRS0bJNk8ghmv1utazcwdGtC5i4ho+vz/cXlgPEGVslOkcgbhSzI8BDgTaS0ip6Y8fBV6oojVa7euMpQvnb4inWku/G7za4a2CTaJXqN1jeSAAb8A/FaciJHSt1Wps8c1tKNLlxKfCsFi6fXVW+i3w1v3KygF+1U5nuslrqexAqHmEkp9OXUIqM2QDJxY9dBV1t75CTK4k/gkD7b75Gf96KzfWiWK4CvDU4xbOKiKhKbAFCsmqJOw600IoorbAifWSpU5Liw3dL1q9Va3SV/bDIFpl1NAaBDKMdBYpNYY/w+bI5WAwsWhwwKhIpkTrkpH9sAdMohRnT+Iqof+nxvTFCSmCFWp5YWEcTMdpYg0DcKBt/3UdEmoEs53bBplJ1+X7C5EEx/j1fiXo6tzKiTxdlI0bkpP3AtbpBhAKgfGgSCrx4nIRx/ksRnpBzQOgwbUZemXQbY3B7smx1x5wPIK0Yr9p8p0tErDsuulvZpsaKRZiGzbfB9JK1WCdabCIIbJtMESYWnOT0X+Mp3JJC7SXjGnke/KcxCdx4lrInZzG8YrfA6xa7arGhTtm62m47ybRfY0exAfGyPc4phK/BlLXpr35poOlfBc8pCPvd93kP5PlTYSzzy2ALmLgRCmDFmn/9WgTVTD/XVJZ60gxlYkxahFTVByI/NIzzpB3F/nxFVtFcysgInyXSRfEpLXhvpU3N8tx/eVa/Y1eWa/ryklnPMyg8ESNQUVIYwRveayfLYmNU6vIfjCai4XJHaZpXSZhYcQc47ddTbmskb7gehLLCahSs1GevuUqWl/gqrHwqfXoQzy50bvTebvT+tWJjTO+izFPVdc/8id6vhds+n/g6I/eu4+kKERSIUHkv6wZlNDr3WteahzQIDp7qCAGevb5RgawdVADhJ1wiC14Keeg6A2B1V48eK44EN1Os55Fw924j3G666YGAVNpU3zqotHPcQi86drWCoSxSl1QYqvfJPre5+fvuMfJuhwuY3YTUIUNtmIqtQrqs2XIkar7HfKH023DXfor6QbEsxBhbnvX5EcJzaddusHFijk0BURm+9GDh+UpqztcnY7OB0mQaxpNDo4vWgrmuvSc8gB0zcVurT5zLEoO5jLblmXy/dtCZhzkZVxzgOMqEf8lv9IQzipmI3n6VkKaYMFeUCo0Y7ZGWXLWXDIv64xvaryLKBX79lUNxFllCQZxmu/d2iIsXMtgU6W2XZiy1t2IzsvfOnJX7sr3CnfBVOdQiOwcsPDbxJGFPI85JpwLUy7VQe2NRPDsjntpeczw2pR0yRUJeIDwzvENM8PC9PP68Mduk930yLrVHYJem94vXf06f25iHCsZQ+F0L1vxthIuoGE84OOS4dIjc06mbfwReOqAqTjUvM5oG1jo79WTBlnSIylcllw6swunno/J1pkqH6bzze53deUF+V2GUuBOQouRvfWe/YWs1A7v71rcyUURldEUv5/CXlqMwBb6TB7tLRxtBNH+QD6lN6bOHTtxuzjLQTM/02uzTP94p1VZ6++e6Pf3yalToL/CymcU432WbR89QI0AWLq8E3Jr5rVSBDpBEIfkk9+3Gf+00MVwfuaUJkQkKFlndizBCJwN501423bfZOPypCqzhqFWZhKyS/kVZndUNDcKOl1h4d0owbw3F6CSi3BHKYU7lMQvkrE7jTXWt5+uOtH8zXxJYTOrPwgfi2O27VDB664u699Zn9z5B3iVIzIkfiUgWxYa6qrc0IsYwy7qv4suJgfuvIIONQTa24jHAWHNBFiKoVe3XW0/V/3kn16A2wlpxEKl0eO1gyqC4fAiEOpNJg2anTY5DDzTbEFCtWWO1fBA9E823tKhK0tKWFRMjqV4Ek+Glia7DLmBUZNGxoVDj/g4B8jf+dsWlhIM/RyQPT0CJO7y9jAxutB9uNnHbekDSOc1iohxT4iU60zn9znTw2hZ4RN6nwYf2zwPSI8hOsZp/kP5Ns6h3OfRoYWW4CB+iLpiAcWTkDwbgIdXMY/N2jKQ/5RK7gw5CsE6IDwvEt3Yp/nUiGmlG/8oipoWeipC9mJCOAmgm3tr/FI6XuV0cQxZ9tTKrpfZw4BNwC16+WZjwDb9ruaNHoE+DPxZ4FD1Kfq+ygJHyR4OnnVHFTz63LPxZBJ0W+1SFk8aYDPlidrWerVFltZBqq5pIQ2Z1RtAVefeWUVeCbYOVfQJoTPfKYkjLxtnwvHUjncJxWfuznxxZ6kpyHQjFEjVD3lBUB9FMUJCYXEULwSHxfZLrWWW77QUaAR1MRLtLDRgSAyGybqrwcjZsDs69IIIwQzdL6pLoieTfPXp6ThQ+b1aYu04DW+izqm4pWGviBEUSuQekJbFCQAXoI328eBFnRJ/zhdFeZHLDeHx5695iHnCXGH1uAYMGtAxJk+xJNvFAA5kPeFNIr6toT2ekt4AtinHQ7s6XOpVCe1si/Guw1FKXY2NgYBLSs6AF8d1fvjiGLdrEC2O/HGszOT21SKMzS4Lz/nAuetLxkGA1FGucw1nrqQzHlI8Tk5rz5ef//Bg+MhiZ2oN5Uxk6axftGd1O7MlTO/MRQtDKZ3PWzSDA6ATXREKnMl2LxDObOtgwqo6xBGX/Nyr5HIv4F6JDBk0OvO2e2IonUiJvrxkMU/7a0cz24PsbwciG6IvzpeAOXOQdIcD3MW0IzXCOlYX3EbruBjGcm2De8iDBrCUf/CPA+ODu/btROv+OzBSXq61Uqks3pE5FXl0vzmFvv6ryUfATx6RQtEdmFJy95F9rBzGqJTmzhBDtGMkcdt2GmxhYUgVWMuv5rbrBcsCnCTx3W7KBDagVjKl+EULkCE8lx7A0SMdNQwgBkMoxUoDiIjlDERb8Pp+sV+Va+MhRa1Y1IEHhFU6+c36xnjhRMlZ9/IT+PK7F5PXrXQvB67e0hIrfmksN2YHdNg8VgMW4Zk+z6Fmh+3DrtTcwgA+bONVnrslXj2VrgdDzvNLcKOKEeexX5obpCkVVJKhxUSLfyaDCqD1oRrhxpr0a0Tl+LsqcXyf80kx7Cv4JNRa6jspGP/PlrBZhqYcvJPDH2U1+zTzhRpDw/hxt1YsBnmfkbl+WivW6+X3kJvnHAI4Prt2UT+3GW3wrs9vNaeVMwiFcsIeP+XH5mafZJhANwQaOnfJAnll2qe1a4PXSkPtk53DEnmwNOQ4+4faHcx/9aOZMPXU+WYCG96ZTnTCojjZYlNFJH4y1oj8bmAjHylUhw5u/qTmMn7nBuiv9FcvN8qJKTA8YNb+PfhvoE34mXjjywMpOGeWScGcBMvpqqG4gZzDPuiqEqB0mqJzqIN/42N8dmKxVQoCYc8r7to24b7MzeACoZ0rYy2wN864Mc1EvfolDORy2Rpaj7pzNmoG5g2FRI4IOtjhb/RTc0gDHINOhMXnYS3lioaAIP34Lp8tJ9lhmiZTiWEiRkHrynknEraa7sPPxOrAtNml/166LLEEVEb40Mu823WO3QU5Ewf+wUUHudWWXvf/DGwHyHlXrIVxvfnNHqHoDNMzlUcq0L1kXyVDlukS4Tm6F34NsrdhxeinM04evpLpSnD1r6tJUlR5kBPD0Kkgf700w2QS2g8ussZ5CPc5P4GVSXJ1UDfB4UcFiy4Lo2Uax2oqByWqCX9ZykVKwCWKOCeejmrQGbk5dnaDaiuIRaR4Dz2Ip6bMmewTWiaPX+gYBe+Css6l06FDL5trlN8oV/onA6KHs45lf9U1t/xnePTBVM4LgtNz+PQgdRZq48xP7ACMGkqn7/n5W3hXYllW8sLzVEmqPoQfQCDgEMUg3D7ZQsEJQo0wfmlhJIvdNKTlFn+jobmsgILXSJvYXhnPqCsXn82ECtG0eIUJWphIcOHdX7VnLq8qW6ep9/y4q+B8zJwK8GLhaUq3OMqdPEEt2PBWoKi7SX4QsNVcTO3ruFJm8DZTBuTjD0XJDdYMr4mw1tzqf3zDOIO5oBLZPWeCt6Ni7QLgHeVdXv2iw9kxXyIrgPoguLiyWylzjofotIaomTuFlHq4tNmr9/4ngb4tJfAfYFk36bhDpwyRSh3T6rx9YrNK38vR2yMpYE1Y0HcWAy2uwJkfkkcTzBK/f2rxFTtr50oUxfq7CP7+Ah7Wk4mbh4w3jWx+aruG5v2oGOFc9W7GeQBpwaZElC7dIqpsowRh9KMrZldMfx+026JBiQisT75WJXadQ/bxuDxYlrct8q+MCCp13VhgdyX2YyCMhjLhHiynWv885CzkcDKKirPwHDNf02Rg8xHc5tGlSgHe7UWPzWuL1gdqrEEZeVMo6q4HV2urhkbb0jTK/E9JpXcNMzB27xR2koS1w6MRT6mr3O1dePVLPHbn23QkNTKnM2qiBzGXhPbW2qRuqxw4nCPFmPmW0fE+NUW8JVfJ6NP+A470arGjvRvSA77aYU4InDvu4ej8IgNH385Ndw1JL3YiCTbZoew45anzTpRYruC8ucjfjOlSAVqSceKLULmao6DKfsIfmobegW96pvRNL6XqUgBSScQdr8GxANW+50OHK+c2KI3gP4zupsqWHFFbog2+dBczVQJTA3YSERWDjIsn0Km3IqizKKzerlFQUHTUHQv6en2kyoBRjlAdlH3YLHRSCmX3tJw+LrCF4lCmS41eM8N5uf4Z9tAZAa7GfktVVAYFe/QM6qoLLKsTcHe9WUldTWHqCJK9cMq/Imk2hWtGndSo4+11HLsWoHCIvyOZhbm16O+yDSWcXjZReNmx8/OxjODH4hJr0DopPc5jFtEr2V8dTMCagODqidUhWlJ+y/2Mf21OBEBWCdkrzwMgAZP+jssHul2kbtPOan1uwKwcCPd/N7bKc5o12TzKoMFFYlMXKay52uTLObBe3qKgy/N9OnC9hDQlAScEMsQugpnUOpaYlukYJ3loonNFWwY7cxX5pintLapi1LrJpqXzCKO8lH9z0g9GH5yiR3uL5qjVcvnFv9GCKpbabTMNELXuH2F2Yivienee/IU7TWkZjC7/JU9nkxPlNn85n/V/Lir5vTycmZ+A4o0eleg8Xk5J5HEg7jruzD5yilCoaEj4CcxUqaewHiQfGj8QTGkEXT1IPPLeU3j8YrLJ7hoScW/3hg4C+XzScGC2/2CrRo+8ywaDPfBXAegpqXrNGmobJOaxYGwpcvyoiRDhcbzVVMxBOWTWXZoEk1vzEG8Zfrnatdv5pZmKpUarTftWqcAYjVMLNdsJLueYtS9FbeC5QrgkJuB9TBIX3suOIYiQ8RgcMHbHImMlR4cx1Sevv30GXOPmb3KZV4KBWgnbGptNQBLkchfbPIAdQXSvmKFHDjo2LptMB0Xnmt5nf0U9BZyJNzwbaii79Pi30MCRAaFIfWZgeYtBO+AdpLkCDsKVjsxrmvj8TPBFsBjeDAfMGQArwXksNM6pVa8rK2dvvHN3p19uFBoW+HU0XVvEESOK3+4biMshKM27pvLDlbjrlvoq+Mvb1YwLmVrmbQjRnwmh2Ak5j7gJvb26XfeAEZSvDmoqIGoVJuGlEcLIBCwBA9OWflydWNGs4gSjVe+gItWMpOaSjhf0XgtVk76PJy2gIqSCAk5zHgKMw+mL//hjq2Ozj7Y0p0tiw0kEd8tPdyCpruz52xVNUkMVrcJrdePl7fgv0fba2BP1f7yjiGt3S5z7njtCi3WWhurhZ/uBz5YXHGUeYuYzsNBaPLbkLq1PjHPNRv0K5Glp9jDngASxsCxR32P0GvMCehTfxm92jk14y9jzpfsK/YFgNep3SeOVrR0N0c5LmcF+cvyElJpM/uX2BcCHVzAG8Cz5E0J4jw4mh7ARli1s8Wemn7dq5zR4fQ84l2IhoQmsbc0xsYPVBvRiV/OyAuzrKsn9c1iojp5SuausnHGU6J1oT8tbSYo5JZtC46Yz6AIb3S3hw0nz1MmQTRZJ0T1rVBfQq8g0SI0dpEJRDPIllih9bzU91aw1lHArUu+u0TmWbgNJ0Cgs4SuBuKMls110pDEeNluvIBolJCvZadQieHFdX3oQJasS03N8/fgwtfXAsIxWunz5kmRO/L+fmaa+Oa7j91LDL0bd718gmqT2+SHr/zzypSVZYvhURlp3j0qqG8VuPcZs/JDXL9lwRB6qHD4qNObJYqfMnyrCDqms/SuNR4vT7d3zNIM2J1chaSyWHOv1XzcIs8DyhVEfX+FgWymfUNWB8S9leqTDVEk25yr4TbV4I16k25HnxP+VrNUZXiDNgixfyXXoR3UtS9pXt+WP7gnzJQwtrWtBjGEHiyWNbFNh/UwbH8BeWBzdSDjgPyTmOSwfZ76I6UWNoVY7fmufBeTescPJFLb6NGg3Bimo9BhdZpArotiVIeNbleWyPCEPHn20UrXpiMxl43HMgZGFhcvwbxVhzEjgzks/7X2ntsoQQGeM2UsAxXTIh3EvDw99AhOjeXiPST/kZ4UlkasUSKODeISd2DoiHj8tqTL7E9Rn9rz0Yp++TOP9PE8KMzBLqeR+UxYASyf8ImKAFbYyfXCwokTzujgiqyg4zRULgvpJyZqH/TI7MLVq0tBm6WV4No0REn9wokEcMc0gHY/BFQ98CvGdono49+3qOUWR3lFe3fu2Qof02SgcK2xINSWnPJEjO+xwBjRFpILLCFq67LIv1H0a1GGCmQW3MSkfqoM6L93U5nu3wGpOtSQhGhFLW94p9BqzqDk7y/PneY+dbS+/FKxzJ9nCUqW0bgj7zBbGHiD7jSDgAxlCvEwrHBy9MBne1Zw3y6r+IclZRfIjZhy24+C6HL/9aFpB//MCCiRAY8x9259qpVIuo9/FaS98wsLkXA5XV0VMrbvTuiFDnh1OOIv/GSHHUDkvub6icPWahNa8TYawBipaChM2hs+9ePzreOmruwG2E7Rl5QO4+UWRW/R/5CG2+M+rkceekOPhGeKrGsdokl9+OiP9cYcCW25MOEhHJZZShHbjeynv6PmJSbuwtfoBnWYU/vtWnTxhXhL1y0muZmCAPGPi67W5vaZ86zmWDYq81KNhxCuiyK2C0EcU+lOgQl6Fr1SM1Mf6s46CMx0eQ/jKe93DVAxU+KETRXFJ8wGQ+d6F5Z+tq5oLoZVef5qnq2XCGoAfpHfMlMWFQU8b6tP0L2wokaL5fu20FTmd0vink3CRrsQvONaqA3Xcr6iprrc/cUWApIL0t4vXklssABEALzWe2SFWAHiWniudp2FWItBak/qpaQmRWWxfqgMY8IjJ3rpiF/bRj8/h3WFDqv4X/W5MKRxMWqzMaCsCVxIx7cRa0sZlNdZJPJ0HY+clccpn96hUQgXqeyuNlzxH420EEct/wEOZIyeGSDipt8ieuxwwvAskUCxnovJoa60HDzTJ0xsdpRzAUZUsPK7J/XwzYqz/gu0KJsKvI8Uoc+sCx5Bnnnhr2OMhRjEqIPldFuGVFFRNu7VusdgJjh5thOY4Dkfgnbo588gkXeWK9ZkSx4Z023spqh1J90o1zn1xZTK1o5Uj64JQx78HJH8dcZXIxLCiYBvy6u0JNrxNwso9wdnRd+YCCnkYca8nuh+BnypvpO9h2gAh7eVQB/o//Q2nMWAd9JdHPeJfuMSyaeBjKqGhLZFdAtLb1BrFKUgxkZEbRuCQpD6z5UX7dp5KcLcj3QE4oMDXfCQzQNOAxf5NPPTYDQ04jb9mZX88FapMvJm3n1w8TisvkUM32/L7Wm3FL1lsjA/Ef1WqA52Lp6/NHINEiLxA/B/dCtWLsQ4MALMcbRrWIzDeLED+WE4oMuFH5I7sOw5RySkVhE54idgLUl+VahWrDgWVC0eGGAc558mt0iG18FQ3eiBJvulPgiIKoLIaJBamn068uA5fTHkBMMkemD78nmLaD7Ns5EsWATvx7hY4BrHRZoqROp4PtdPz8KnhIDsETno84C6FOXLv2Kj4ofk5FLS+Psz23bx1BtjXffuCptCHJLbtJqtgNueV5P51so3Wnv68mOkyHXsGzE0npknRExZ/HVa56Umrz1NdF62Usebv/DHj6bHZfSyAONHu8FPcWZZYFZdLJo+3VtlJ94MVywkeEnSi2ft9FW89rMqRJ+iZD5kyYKB/hsv/5fdGIwVP1zMPWPBWOnd2GXZ0aWuERUuuKASzWe5rpQ4ObqfhOzz9FFbc5vl6V61XH1ppENiHeHNBELxw466lyKirHb9Nq2c7zcMIUx0Rs6FH6l4mjXXiMAsu9vnpwuC8t6TWgaQMQYyf0mJt+1c8G6UXoHaxYKnIBZluJb6nDr9ngSi5f4r20SS46FUOy2kWbx0MHN0XOOSKh9lzAkQ34iDQlNKBqtfG6NCHvodhH2T4t07G5Pv3017Uk7DTCJFmdU8i+5srhliLbxX61JR8qI6+ZwZJCnDW3+NYpZeUkq9SxYiFTrnYcIIa0eos+O4gP/fnwx05W9xZHyDkgW9iab5wX/dAcyE6xs9HBXdQob0Q0ek8VAdueyEDDEiuSI2ZSjkjkBFBi0xnjGaIqm/EIdb+wWwShV7ygMFVPW5cGBrhQ8T8N7yHJpg6OFaIFh4JXhVQQaXN6uUsljajUePUoeyuDdfyfVlEQYoSg4VXAC7A5ikhAKpiCqQ481Zz4uGYzWx7FyPGttlcKW7I7c/Q3EoNISdd5Q6Q3S9KKchP6dNTaheKDTE9tcrq6se22mAuxBRydUtrSFyLUYL3dfFFqVptW4LNVQGxmZHXhoBgcKfnXQqQwKQFpdYUH2ZKnyiQ9xoZePc8bA8FdyL5ha7MpTYg+gfXBu0PMeMqvWvds5GdDas7MEei/MVK5menOCGYwOE1WyirChKe67WNcDm96zhvZhA6bD1YuA/Nqnin5J5n1NrCILNK4T9Pvf8VUSwe7eaIX8f8phgJ8jg4TUlmsrbUkZZsao4vqR2jUQNJ6G0geYCGeW23YJsMf6Fit5JSLL8noMqHj0rmeGCDp7B2229Aa1QgAHeZUM0LgStbA8wUb00/EeuL9Q1SBe4mbGBxhHO6zZPHWqHtNOshWrOFfQ/m60yGDOPFUxdksdQyQI0n3y5u/s/SwxLyw42JpN5rOT5extGOS6ilR5KZ+u5vG+VWCQ8kOs5oabt263IsMUCBKXD/+03oHlhs8Za4gAr5D1pXUR8DzWc9NxuKUvyjOBgoUzfsV7gzPBn3S1X4xH0d3NJ1XBzpQD5y1zIkqacyQnMg7Fblhyp/bkHIN+51ERiTeqLq5HLjGkQO1QnW/ZudKN/9xaVncg45+iy2mZrq8HqyUNzBIxfNuqe03KcVpSTUZiqTMjUMXFMjDci+ixM4p6dCN/kuFNT3bgGe7SHStHkstAu49lYs0wRyzUzdL6I8hFRCNRedzablFdGDl47nCPpkm6NRegAWP5c20y6kBWvuye1GkPx+i2Uxl28K941lgeL+Se9TnfKpOA5c8/adPN9KSsgAChMgL7SmpjTAK2CLLEcBOYZLoPhloFYgNINPxI7ZgWeoYiNpaBLbwZLGlUxGMuy4TiwYJK2kGx5tDxQ5qB9JxKY6J18OLLh8d6DYx9dE9d7r8+EWrTlzgxc4tKrGZYwB2vbikxLILzcuS31Y0a19axV624EyUiLBeYeSXAOn1rIfAzbyA5HUomDbeViAFUGZFTXqCtwpkUl7PLxUwMkcLGQjitYLQQ8DT/CDH92nnRF+quYSYhT32f4dioGqg0NVCp1yusVioggbgh63ITGdB6nynG4AlBi3NxRn1Y9dcBH3O5bMWKKpG6pVTnCFOv0N6JkWBX0XG8XI+eFOfPgRwMHd48X3tCwLqbX7L0r9noly663XuY+mPnEcq7j+BiIUm3ABg6xrxvP3YRRTTrcfTNLrpf6PoliY8WaFUeMnLyjWeg/cFoxv0wuYHmM/Euh5bUqL8yyPsFsuEl4ahnents/rb3GGzHgzqyfos2eRm2w4TXbcwDVMrJWjuX66iJ/zPcu5HeJ9QfwS4ndw3ganijOpE4Nb1/qFj5BB5XRD12o8iviGlHdSBowT0deRGDzVCIsL0ggwZH/4xpqyXjlAn0QB+QjWn07jARHEmz9T0ymbtqHFaaf5gZzwkL9ug7H4uLIguhQp53j1KCtIM0cSzO+K+RHEWHUiH50bDlBF2DmRf+LemXz2knImBYkXNqYhjvOW8cC32/M5ShTA95MIcLAvqK7XMb6AyVYBvijGfN3KEw9HZa4cghrSrxp4LM9tRHSTh5eFishhV8tboXVUxOTqRlZ8hdV/sBRkw03IRgnnbLuQZse+qDYCS/VmIdZYMP09wB4idNp3ibahUy3ISknQeM4sjj5caP+K7hOy+tBeMUg2lqLwQVC1DkNJIxwlZ8XlqQtj3Er4qwSjqqshpesXKiSzEESuSjrw/HvfrYs3X3uN63HC27UhbIlviMZIIYFs3HNl+rZOabewcVcGmV7kk66w6h+iYUaZ47xmaWzZvs8ySLYMg2sjo9ukajOWSkp5eZUzLeS9D3DzyAX5X+VqaWQSN5nVJw1XovRNRG1m8J7hFHTek/qmkid54ybi72LfGS9WHlGEPIjGJm2Q4mS9Wnogg3TOJPrDgDkufDyCCNpBZE6IhEFxbufdBdUIzYG8h6gJjYiRezujTJcjKdWdW0IOyunayYZA5ZkoJXU6Z4toVTsYrnO0s4OaKp1HPxpEG9vz4984HNELRTT6Ylp+Rus19p91edAFegOQKONvDYrQimrLZAUR+Fz7ibVn+y/V5mYqwEqwraR8vr3Asd1ZLR0/eWK6uM5GVotW0y+YaT00SNx0RejZ0EO7RyXHA0YSaQHQMDQOWHCAl6pDsW8xDGgs8KhXEEdx5aC6wN17oMXtHeKP8VaaxDS/JLJXEFkvTOTGfClRllkeI7Sx1k9mz+M+ODgoZ9BPJIwhXUZNCtFpiT96CB7PK1M7RLfbkjLLV5Bfy3JaoBAmBV3ZzzJ6fzn9ZovR63V7LIyDh1mfj8At5Wpo3cbsvxHDnEUDv4JxKmHKZs0M5QSl0R/ZDavZH5o2VFYRxq1eALx1UiLoRzlAnZitOoKE1d0qeReEC1BuVPeH7bVeAV1be+1E5OMGzXS+vMOnvhZUds3G4nVx9Bnp0MaVQXUpL7aJEBqDrYv5xVS/4N4IsGtun16JtFwL2ioVgizdnrHlR6xJwt8nD2Yt4juCz1o5rWB5/UwgEjJM0kWIcOwUkemb1BCbme6j3jdNgQFoapWsA3TUAKMMOiCUXHoBBpCKTK++kanDgZ5wBzGG1F3Wx5a0nBMlt7nz12xGZRGYK1dOvFqbJibkvM95hj+1UdLBCaQNzTRi0E8rRT0TYgiORcJf1MebFkxsayf6a+hMyqouaLD8RZXQgJFVi7c+w3Ap3/5T0OAPGMmHJG2pglUonIIFoRelmacRf7IXhUWrR+uDr56N8g81Ids1J2y/4l4aAmmZTPHiaACxNRBDCfMTGSESzpBqbqW+KhdNVPG9ep0Uk3JlDyE3N2+IdItWOqkV6GoxU5TsXrBxlFS4vDKUEokzgYkLXyiuVXjJWyNAjnVsYqRFtCxLDAAkhI0HIj5DXar+mnr4/FB2mIS16d2nnJLDSgg4ezHevkERW68eOggcASjKcge+EZfCH7CeFrWJ5DNesX3rJDbtWxK079mzu6e5tt+KmjnOutamD4gtV/mCmd4l0dpaObR/NL2+K3BoqyQHp9/aGrMDW9SpaAaxRAjMHmA439rhAfaOyBzlN19X/QB9QmFDtM2wHhhKuC0XwIN+ijQvXf/JrX8PnDq5vaEKb7Uggbmu46MtO28DnRaXXPrZ/OGJwFErTBdrS2QHLk4EV/IxDk4O+t9TIzB7EUvzeZosaNpnUqVrLWIUvTSjY5zrm9Ppt43iBj3v15BmH7ag9SS+Pa0jVUnu21Zbl9GiQFTHPUhJ6bvV/Gkc0h8LKmSoNjiDSIrEFtcnUrvomBJnxUUb6PMVNbYs/YRCyzDoi6d9JTxY6hJ4cQN/MDNEnQnf34xYgC+4Qf1iDuHg7ERcRQExFmVT7JPE25+lIrbhxwWCLkzL/yE9yN976IVjyCZcfaBmktI2RutuBSm3vqZ+4G5/4grAa3YyATiP3BKRLz0aSkH+LMqJV4KI17Jvlal7aGw823sWbHwSozxiGA6YHVk3wsDSu47jB02tAMHZIeeTy4kxdQvVv1Zasb0aO8vyV3hqTNYEKCwqWzxk4l+VwA2NSUcaOv8ddgE6UIyFVe9rIHW1XZB/A3bGNvrWSGAutsFahOS8EL/sNdJUPktAXtmRgyQvDqRK3ulMeIhJWzaaWGXLWrSlEzcLixF+4eiY3+0Nu1UPkuqgPPzwUUYLZ0bqnTH1UxkCUVz+AtrLsKNfAHGnCav4bs0Q7qKHekOS1WDeXyqOPrnB8MF56koDgAFvvDtXvvTE+onyI/JVazsc0vRZ9pXA2Uf3J0JL2pC7qAqHsjtQmc9laV5lzDDjePIz63YzXT1fZcBELNrFY7qbcYXlhuUFNWG1cExmu9Kn3F7WYU//jyAO0wrE9mlU0KNuZmXjc8xZiLP5eNWaFRx2toAsJcyEbOc9HOjnPQ25RSIYS6CQ4iDEMl7OcQ58k4Ai1YyeIKHY6QmlDfGJLSV5cqtrzH6BdhFBf9YHW7DWNjKmtfW+hIlXbF7FXRczzTx2TjiNnUG9OfGltFp0juuQOcTXo0fT2SPKcvHAO1Cw7eFJgM7zT8dnQyMS/QsWsPB9xZHMO19lijvu1c0nNaPqj9DjBrvUVv2ZrD08A2n3pg2qvR9UbVT6eVuOJG3JUjRQmwnSOOso7CR9MB1DIybBkbvnUc1T3o2lHqJN4Ezhr3rajxcfJ+h4pcPvuitJBd6vUkBNVOGp6pP2kK+5uG026KauFDtPhIvDJoAWWTYDRnRSwr/9Q7Lwqcl42VC0zAnHTe1Ryh56poS1UeG14z2QMP6YSlG87CIPbdkTIzxlHTpr63yoxIXARVKWkXZ6/yp5g4yuSRD+5BgvDHsb3oja9EUi8Xog3FOPFsDE5WtydxBxvVdvIDVZ/Jr6FRulTEz14E0ym6LVg3nehnmeKCWN3wqXKehhX6n6UXGD+w0qWG4kOafnQMPxP6WGzd5aHHuprtY/LFPQ6Umbcy8QnZODv6YGACa3v7vlEfodPS/lQdwgZbPHxmDEq16giyGOHibM/OP4+qbr9mT56cwDSal2GxVmqTroYxB6ZBYBInOQBfFwo8aM1kBsEO+Z9FzVDh+MMXYKDOwkL+YIB89Zzo2l74n0oi4fQ5ULItSrmpqAc3kTq/SPleA1m9Vpi3X4K30dyOAalLfXANtCD15t7xWtyr9n8xnlfxxqt7wl3cYKgXrxzAsmtmCbjVpgSpFKkumlzKwJj84x3nY/rIwM/3Z9fIw2U4tnFgSbgPdo8zmmIYEYst0GP7ONmQsKU/SSOp1l0eoyxZJjTcAhrUVPlNY6AFmH9Vw/ypByTNHbVVhQsj8BrevzdswihME0EK6bbJBc9igPCTg+ebQkCGv4kaoZZj958Bxm6MeAkgB+Ua+3d3VCjB9OvZF42zbqr9059IWZVgLr2FQvXnW0bXPnhxWd8T1/D8Pq2xHFrlLsCvZ8D/z6V4W9QT3LY8PEpeAWzY22JAfu+BupUMJ71rDqR7ICezZ7UHcxjC6lXI2JZaoxjz+AdT41+A0sVOnBFT+5ww9D+jGEkc5gDhwH9be3v9SJF+VY9ice6tNdcnC4c0o0T9bsxyi/fXy9XiPK0IYd+1amFrBG1iTAZxo/QGbHepLvK5iHEQZTCuIpF5dx1UaAMYMUkHIxcLjDDeyBsxI7lZdIo2CucsD5B9AZn2PVY46FIGwYjOB044GtMy5bQulFP3ci6dBQ12eeb2FZ5NjKJP/j6uiQtGw09jLBfB0GoemepinFzQIL/mlHMg2Ns6B2w21cFk1GUnb5IyePsowp+tXNlcfYCPGFBrSryhdxoX21DhivvQhWKkMB6en0gH+rXcD9yntCVc995QxxFnIrc3lI8GYRpiPmq/YbB2xZ9afHb6cZrcd02vMttWoLmbn9CUKaxPFz8FjBc0VH+rs/nOvXAlDymoqGrt87uW3Bg4H9UDmo1Ipy0CR0oXsqs7MyAs43dYOeWmJywXRhnd6b+omr1wJcyzoM3cr4q59dDdyiMw0LfX4uQuQey+czGyGBto1AJHf5WDijp6dHNSTZ+BMP72XckDGoBYBHoWVirVkMf0+0cxY27ABdefTNT5Uw0W1LTmbIydF/amycKm6w0xnwY8KZWouQHFoZ33fltIUs4mHh9enh9/bSMUav+BDsrZhGQnrLA8lBIBwxZCb3SOTrFlpGe/2h9gqNn7swrB68qZsY6+omzoIKvEpG8i73dqBFWAXZAexh2WrugOvddfZLpNW0BbL/3doiAmYFBM5xwPjVqCaSYjNMBn/b8F6Odrda9t1MvWrAEyj1pNGfvkaNbF4ZslQeHMEbcT6rqPhyXy5StsvmJuC0lu61WDJME1p+LBbHG838/WQfzyacC4CWmyISjlhAcADZkDEu0uAV/A6m+vFRy2t5GExEjbTXDEch7K5KYreNdYFHmyPpvYB+Yu6QTkCc2hBe/RkOnZ94c5qJjSi8CVEw4scvNTZwNUWV96Kyr62ru31xmx0cD0h17OZBXHHUNAJuQNDyaEAU1AUWjCTGYQX92mOW/DZMvtdzWdkkdypNyCE5PUoebEWzcSk6vXPN/GEQuINeBFZZrbif7oYPtToov4UEoXQssJ/WW5Y7ujzGjO0uhyJVDo/P4bQ2UJaWB7bUfCTLVQ1OXJBnuUbqwMsxGWMdhFwhoRVXLgocTRqMGm0ndij4IJVH7aA3TRJ/5xPw8htLl5FkQwqlLP4TR/F5olq9U9ppI78rCCht42xgi5T48KGKTeWlSUxHL96io9IFU2nNyLPPpH6OY0qxgMzI/jqv+bD7J850HbswjXFyyhH6dXiUvwtNQ/J/3dzvllgPCcMqiLc5PTW2tkqybC9cr87Bc4yu0dOulufeGRTfNSkR9hoc2BURqw8kOB9xxDK0Qlh2jYVoXmPQoLpx/Ze+yauHJkvSraI4peo+JcFfFwTBnNylspVLnanENJdL/fsuBeSIWtwmQ7tz6+bT0o9niR7Pgyg47FQ8k4qHCCrgCF1QkG931mW0QRGIMGrTaipt7BecRj7mM5rwWkcrfJbrR09hRgXUPYWA/J9MQW97mTpbpRjsJOmS7rTmNOfARvO1zCCG5LVIykmzl3TMrgJyrExxk9rdDjB+g14V+gTNQlInJ2gifSl8w5/vbhDHPLMhSrg3+RWcfF2AqvMxmPXDSE3kPmyvPcC0c+H2nirGhmFkKr+jBDnX3FaGOf0VCzmhcTVKjGNkzNyWIBE+OysuYLESbD6l2xadZlIjhHFYPPh/ZcAXrVtfrTFdgF0e9rUNtVCeeSJuEzypipJwRqhXDlf1QNl37nQAKDDgIs57bjNr9gVaEkqmZ8kwQ+cfSkTH3bGaj22eFnYD3aPqeGzdC6mXSIG4hMEd9pAmzfp/WBOzhGrZVxYYVHDoUJW7E8eRus+2kNPK+MVqE5owA8iugfCN3dbbtszOqXxOF7KZq8BqiSqB2rSEs3hf1OHgM+PFvJyfkYQ3pL7mzZoPjGZXy794OYYvFyTJ7EbXWZPCnnKsBj5iI6dTzvtWLxwWeuVJWywpDsYG21tyOpk3Kvs5rm7NL/b7gqXjKvsQsUTE3R+iIB/f409En+rhBkxrloZ6pVCivU/gyHlVDrkxSmqoCMVZhiLG1m29umJ+DfBsckT20QzB0piY1/LmYBNe8fijRoYZLkBpUU6Pm0jftkOW/yfSsiZlpDFvXKZyYzrqv9XyjA5lgTV9ofwq7G/IQMj+22QmTI+sfTsWeQIi9NARMaApQTUDqLvlO+F+TVn3xtC9QHdZn/Xw76xXuKyaGUUSFf9eHagJK9UHi4c4TQLEK5XcePXPRhDsoU0h8lrWcCaFETAEbnPmNEif2gL1BtENanLaaPwTs+o5XfSeI3SmQia/YzYluErPoOr5Gy/rLhuYPNymEdiMAcIJ8Kwv8xSf09PPWlMQgiJPeRpLMdikt/mdm29fQUqhZmOVeimouF1m/zlkPcLxgV+oj3E6KptQWkVKlWx9Ga1TddxEmk8MXJDANIDH9LMt5RQHNFgjwlpbGcsvm1IDWLPhoLZEFcSffhLojoA/P0nd1huwIe9md9ooZUbLpbXApAPUKOPiD51RD5W3GPVXaYhLPi1lZL+sQIFgP7wTCtE1pPbtvZ3UaLVCKI5cu0OED1Fja/H36Q3i1vW5wCz5xWBidtYjEJ3wBEUwYX+g3ybruQA43Be0tZEiWtmaP0m9yYlgTLNpWcuw08PhSx7XDzEI5nzpi39Zu4F1UPd3GE+Doy6uovAAZf/E5xDFWi5K/aVhWKnxQ+qPK26LFWMMby6/uAK510du9Af487TNYoPOkAduy8hKOjYnIrsbuL7mYoKcnL7lr+0PgnEJKtXhqP420LgFk0+s7KoIhQYJ432jMuhjkQU9FAzWBG8TwRoqrAN2x9eD0GcLAD536c03Qws9pneh3XUfADEGHHFJ8BRVxvS3JLfuEcjNDbZ4x9X/rCwlobQr1SCLRxxUYZhy2Iy5jK0oVWw67IjRF/2btlqd6uaMTXYNtWPPjc4iKQuedswyrqF2ViK+PkUWkl3mFC9quwjJH08/RJnRQq28lCGSLna82BdpQ6x14Aj5LppQr8eDqQ/6nKcYW5igrBIf/viIINXt1IR2PpcLLzS138mD2amGH4dGyQ5TZV7w/75ASHtW3mdMl5o7WJFEiqQuo3hfg4PjneKOqQ89YPwFJIR9o2j/6cNK+amsovjOlaA8Ldgv6gercy/HwBwcG0RaN+bb8DfYEWM48K5JEKF9jl8inTcVfbSt+++uV203gC8AgJQmvtBrczbPZnT+9rinnlmB9tN+CLdybuytrQKT1RMdFYa/lX+1/TUOHrSwJnmHBlfkezFBIVRDHIdf5efH5BtillVt2YWEf6IqqnC6+OIaa2ZEWGKYlwLScODbxqlmmW5S5eHAIvjzAwBCTglLefTb7QUg36ILJ7LtHUs32XcsQrCNurdlHZRJLPEEck9gACG+ZY5QNJSYrKD9RaUU5q72FNyimaNBmE5opzYndZR1EWfIHA2BrPcaXHCp6oAnAFC1Wx3v6FGO79bP88QrSeeCg3+GpM3exteLFRAqR+E984AEQpXdSiBT+Oio1ub7W/SnaWFHE03ikAc6RzBCjWDrfrJpwE72iYpedUbHeJqfsN1Gfm4pS3qUxTVwDYCSnFZ9UjHaT0J9ZDvr9tuE/iut27bepKqdzNwMvtDcExvb/wph7zaCPZsNVzjyFBXDI4rHMwEmGw9S4sI4IXzDv+4aPvZ6zirF8A5GM/cZbOFDEhfG3/L0DzTFeuC72fP7rdTtA+ZF974gteuysvJId+ye6dsqw51GHBfl2xqd+hrONVAWB2pum3PWBqppz6XpoGowNTGejM2bWSJVlyLs4BMe6C2Bk4o5rG2staHbCGYjqn8Dv9oZMZJAVguIIA8BnE46F7qn0H33h9oOU5kUDbRy78M702t0jZMVS1N/FYEf1NZd2qa/4TfgfMBBokBtOJ84l2qbv4u8zexrIRc9VzSVqH428FbizixFjvOk4',$data['user']->salt);
// 		echo $this->uri->segment(3);exit;
		/* $this->load->library('pagination');
		
		$config['base_url'] = 'http://www.newpro.com/admin/test/';
		$config['total_rows'] = 200;
		$config['per_page'] = 20;
		
		$this->pagination->initialize($config);
		
		echo $this->pagination->create_links(); */
		
		/* //得到所有的get参数
		$getArr = $this->input->get();
		$page = new stdClass();
		$page->row = 5;
		$page->num = 1;
		$page->total = 100;
		$pageUrl = current_url();
		$pageParam = null;
		foreach ($getArr as $key=>$val)
		{
			switch ($key)
			{
				case 'page':
					$page->num = $val;
						break;
				case 'row':
					$page->row = $val;
					break;
				default:
					$pageParam .= '&'.$key.'='.$val;
					break;
			}
		}
		$page->urlParam = $pageParam;
		$page->url = $pageUrl;
		print_r($page);
		echo ceil($page->total/$page->row); */
		/* $page_config['perpage']=5;   //每页条数
		$page_config['part']=2;//当前页前后链接数量
		$page_config['url']='/admin/test/';//url
		$page_config['seg']=$this->uri->segment(3,1);//参数取 index.php之后的段数，默认为3，即index.php/control/function/18 这种形式
		$page_config['nowindex']=$this->uri->segment($page_config['seg']) ? $this->uri->segment($page_config['seg']):1;//当前页
		$this->load->library('mypage_class');
		$countnum=100;//得到记录总数
		$page_config['total']=100;
		print_r($page_config);exit;
		$this->mypage_class->initialize($page_config);
		echo $this->mypage_class->show(2); */
		//print_r($this->session->all_userdata());
		/* $listresult = $this->public_model->list_pub('test');
		print_r($listresult);
		foreach ($listresult as $key=>$val)
		{
			echo $val->id;exit;
		} */
		/* $j = 100;		
		for($i=1;$i<=100;$i++)
		{
			echo "INSERT INTO `test` (`title`,`text`,`create_date`) VALUES( 'title-".$i."','这是内容       -".$j."',now()+".$i.");"."<br>";
			$j--;
		} */
		/* $newdata = array(
				'username'  => 'johndoe',
				'email'     => 'johndoe@some-site.com',
				'logged_in' => TRUE
		);
		
		$this->session->set_userdata($newdata);
		
		print_r($this->session->all_userdata()); */
	}
}