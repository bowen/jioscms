<?php
class C extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}
	function index()
	{
		$this->load->view('admin/index');
	}
	function home()
	{
		$this->load->view('admin/content/index');
	}
	function s()
	{
// 		show_404();
		echo 'a';
	}
	function blank()
	{
		$this->load->view('admin/content/blank');
	}
	function ajax($c='content',$s=null){
		echo 'c content';
	}
	public function _remap($method, $params = array())
	{
	    if (method_exists($this, $method))
	    {
	        return call_user_func_array(array($this, $method), $params);
	    }else {
	    	$this->load->view('admin/content/'.$method);
	    }
	}
}