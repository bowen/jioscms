<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
	function __construct()
	{
		parent::__construct('admin');
		$this->load->model('user_model');
		$this->load->model('public_model');
		$this->load->library('encrypt');
	}
	function index()
	{
		echo 'ass';
	}
	function user()
	{
		$user = $this->user_model->getUserByid(1);
		print_r($user);
	}
	
	/**
	 * 更新授权 前提是你得有这个权限
	 */
	function updateRole()
	{
		$data = $this->_pub_data();
		$where = array('user_role_id'=>$data['user']->role->user_role_id);
		$data =array();
		$roles = $this->user_model->getAllRole();
		$permission = array();
		foreach($roles as $val)
		{
			array_push($permission,$val->tarurl);
		}
		// $permission = array('/user/a','/user/b','/user/e','/ssewx/serxde/serwcx','/s','/user/ulist');
		print_r($permission);
		$data['permission'] = json_encode($permission);
		$this->user_model->updateUserRole($data,$where);
		// print_r($data);
	}
	
	/**
	 * 登录页面
	 */
	function login()
	{
		$data['title'] = 'Login Page - Bowen Admin';
		$this->load->view(ADMINTHEME.'login');
	}
	
	/**
	 * 处理登录
	 */
	function dologin()
	{
		//先判断是否是ajax请求
		$obj = array();
		if($this->input->is_ajax_request())
		{
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$user = $this->public_model->detail_pub(TAB_USER,array("username"=>$username));
// 			print_r($user);exit;
			//如果当前用户不存在
			if(empty($user))
			{
				$obj['type'] = false;
				$obj['url'] = '/user/login.html';
			}else 
			{
				$savepass = $this->encrypt->decode($user->password,$user->salt);
				if($password==$savepass)
				{
					$user_role = $this->user_model->getRoleByid($user->user_role_id);
// 					$this->public_model->detail_pub(TAB_USER_ROLE,array("user_role_id"=>$user->user_role_id));
					$sessionUser = new stdClass();
					$sessionUser->user_id = $user->user_id;
					$sessionUser->username = $user->username;
					$sessionUser->firstname = $user->firstname;
					$sessionUser->lastname = $user->lastname;
					$sessionUser->email = $user->email;
					$sessionUser->ip = $user->ip;
					$sessionUser->status = $user->status;
					$sessionUser->salt = $user->salt;
					$sessionUser->code = $user->code;
					$sessionUser->role = $user_role;
					$this->session->set_userdata('user',$sessionUser);
					$obj['type'] = true;
					$url = '/b/';
					//判断是否有来源
					if($this->session->userdata('logreferer'))
					{
						if(!strpos($this->session->userdata('logreferer'),'logout'))
						{
							$url = $this->session->userdata('logreferer');
						}
					}
					$obj['url'] = $url;
				}else 
				{
					$obj['type'] = false;
					$obj['url'] = '/user/login.html';
				}
			}
		}
		echo json_encode($obj);
	}
	
	/**
	 * 退出
	 */
	function logout()
	{
		$this->session->sess_destroy();
		redirect("/user/login");
	}
	
	/**
	 * 注册帐号
	 */
	function doregister()
	{
// 		echo false;exit;
		$obj = null;
		//先判断是否是ajax请求
		if($this->input->is_ajax_request())
		{
			$obj = $this->input->post();
// 			print_r($obj);exit;
			$uname = $this->input->post("regUname");
			$upwd = $this->input->post("regPwd");
			$uemail = $this->input->post("regEmail");
			$salt = substr($this->encrypt->encode(time()), 0,9);//9位数的随机key
			$user['user_role_id'] = 1;
			$user['username'] = $uname;
			$user['password'] = $this->encrypt->encode($upwd,$salt);
			$user['salt'] = $salt;
			$user['email'] = $uemail;
			$user['ip'] = $this->getIP();
			$user['status'] = 1;
			$uid = $this->public_model->add_pub_re_id(TAB_USER,$user);
			if($uid)
			{
				$user = $this->user_model->getUserByid($uid);
				$this->session->set_userdata('user',$user);
			}
			$obj['type'] = true;
			$obj['url'] = '/b/';
		}
		echo json_encode($obj);
	}
	function getIP() {
		$ip = null;
		if (@$_SERVER["HTTP_X_FORWARDED_FOR"])
		{
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
	
		if (@$_SERVER["HTTP_CLIENT_IP"])
		{
			$ip = $_SERVER["HTTP_CLIENT_IP"];
		}
		else if (@$_SERVER["REMOTE_ADDR"]){
			if(!empty($ip))
				$ip = $ip.",".$_SERVER["REMOTE_ADDR"];
			else
				$ip = $_SERVER["REMOTE_ADDR"];
		}
		else if (@getenv("HTTP_X_FORWARDED_FOR"))
		{
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}
		else if (@getenv("HTTP_CLIENT_IP"))
		{
			$ip = getenv("HTTP_CLIENT_IP");
		}
		else if (@getenv("REMOTE_ADDR"))
		{
			$ip = getenv("REMOTE_ADDR");
		}
		else
		{
			$ip = "Unknown";
		}
		return $ip;
	}
	
	//临时测试
	function abc()
	{
		$this->_t();
	}
	
	public function _remap($method, $params = array())
	{
	    if (method_exists($this, $method))
	    {
	        return call_user_func_array(array($this, $method), $params);
	    }else {
// 	    	$this->load->view('admin/ace/'.$method);
	    	// $this->__show(array(), $method);
			// echo $this->uri->slash_segment(2);
			// echo $this->uri->slash_segment(2, 'leading');
			// echo $this->uri->slash_segment(2, 'both');
			echo $this->uri->uri_string();
			echo '<br>';
			echo $this->uri->ruri_string();
			echo '<br>';
			echo '不存在';
	    }
	}
}