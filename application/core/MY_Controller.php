<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	function __construct($c=null)
	{
		parent::__construct();
		switch($c)
		{
			case 'admin':
				$this->_admin();
				break;
			case 'cms':
			    break;
			default:
				echo 'default';
				break;
		}
	}
	//只支持两级控制也就是说控制器不能有子文件夹如果有多级建议用admin_controller下划线分开
	private function _admin()
	{
		$this->load->model('user_model');
		// $theURL = '/'.$this->uri->uri_string();
		//控制器
		$url_model = $this->uri->segment(1);
		//默认的路由调用welcome
		if(!$url_model)
		{
			$url_model = "welcome";
		}
		//方法
		$url_method = $this->uri->segment(2);
		//默认调用index方法
		if(empty($url_method))
		{
			$url_method = "index";
		}
		//这个是判断当前范围的URL是否是可以用的
		$theURL = '/'.$url_model.'/'.$url_method;
		$getArr = $this->input->get();
		$pageParam = null;
		if($getArr)
		{
			foreach ($getArr as $key=>$val)
			{
				$pageParam .= '&'.$key.'='.$val;
			}
		}
		$user = $this->session->userdata('user');
		//不需要权限的控制器
		if($url_model=='user')
		{
			$otherarr = array('login','dologin','logout','doregister');//不用操作权限的方法
			if(in_array($url_method, $otherarr))
			{
				return;
			}
		}
		if(empty($user))
		{
			if($url_method)
			{
// 				$curURL = '/'.$url_model.'#page/'.$url_method;
// 				$this->session->set_userdata('logreferer',current_url()."?".substr($pageParam, 1));
				$this->session->set_userdata('logreferer',"/b#page/posts?".substr($pageParam, 1));
			}
			redirect("/user/login");
		}else 
		{
			
			//超级管理员不需要权限控制
			if($user->role->name==='webts')
			{			
				return;
			}
			$role = $this->user_model->getRoleByid($user->role->user_role_id);
			$permission = $role->permission;
			if(empty($permission))
			{
				echo '该用户没有任何权限';
				exit;
			}
			if(in_array($theURL, $permission))
			{
				// echo '允许访问';
			}else{
				echo '未授权的操作';
				exit;
			}
		}
	}
	function log($obj,$exit=true)
	{
	    //false
	    $getArr = $this->input->get();
	    if(isset($getArr['debug']))
		print_r($obj);
		if($exit)
		{
			exit;
		}
	}
	function _pub_data()
	{
		$data['user'] = $this->session->userdata('user');
		return $data;
	}
	function _t()
	{
		echo 'this parent _t';
	}
}