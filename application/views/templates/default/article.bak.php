<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-transform" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link type="text/css" media="all" href="<?php echo base_url();?>out/css/autoptimize.css" rel="stylesheet" />
<title><?php echo isset($title)?$title:'jioscms';?></title>
<meta name="description" content="&nbsp;" />
<meta name="keywords" content="WordPress,WordPress主题,原创主题" />
<link rel="shortcut icon" href="http://zmingcx.com/wp-content/uploads/2015/05/favicon.ico">
<link rel="apple-touch-icon" sizes="114x114" href="http://zmingcx.com/wp-content/uploads/2015/05/favicon.png" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://zmingcx.com/xmlrpc.php">
<!--[if lt IE 9]> <script src="<?php echo base_url();?>out/js/html5.js"></script> <script src="<?php echo base_url();?>out/js/css3-mediaqueries.js"></script> <![endif]-->
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.min.js?ver=1.10.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/main.js?ver=4.5.2'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/slides.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.qrcode.min.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/wow.js?ver=0.1.9'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery-ias.js?ver=2.2.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.lazyload.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/tipso.js?ver=1.0.1'></script>
<script type='text/javascript'>/*  */var wpl_ajax_url = "http:\/\/zmingcx.com\/wp-admin\/admin-ajax.php";/*  */</script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/script.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/flexisel.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/fancybox.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/comments-ajax-qt.js?ver=2016.05.16'></script>
<link rel="canonical" href="http://zmingcx.com/2-0-release-the-begin.html" />
</head>
<body class="single single-post postid-7146 single-format-standard">
<div id="page" class="hfeed site">
<?php include 'common/header.php';?>
<div id="search-main">
  <div id="searchbar">
    <form method="get" id="searchform" action="http://zmingcx.com/">
      <input type="text" value="" name="s" id="s" placeholder="输入搜索内容" required />
      <button type="submit" id="searchsubmit">搜索</button>
    </form>
  </div>
  <div id="searchbar">
    <form method="get" id="searchform" action="http://zmingcx.com/baidu.html" target="_blank">
      <input type="hidden" value="1" name="entry">
      <input class="swap_value" placeholder="输入百度站内搜索关键词" name="q">
      <button type="submit" id="searchsubmit">百度</button>
    </form>
  </div>
  <div class="clear"></div>
</div>
<?php include 'common/breadcrumb.php';?>
<div id="content" class="site-content">
  <div class="clear"></div>
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <article id="post-7146" class="post-7146 post type-post status-publish format-standard hentry category-works tag-wordpress tag-341">
          <header class="entry-header">
            <h1 class="entry-title">Begin主题 2.0 版正式发布！</h1>
          </header>
          <div class="entry-content">
            <div class="single-content">
              <div class="ad-pc ad-site"><a href="http://zmingcx.com/begin.html" target="_blank"><img src="http://zmingcx.com/wp-content/uploads/2015/02/beginad.jpg" alt="Begin主题购买" /></a></div>
              <div id="log-box">
                <div id="catalog">
                  <ul id="catalog-ul">
                    <li><i class="fa fa-angle-right"></i> <a href="#title-0" title="更换Ajax滚动加载JS脚本">更换Ajax滚动加载JS脚本</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-1" title="杂志布局添加单独的侧边栏小工具">杂志布局添加单独的侧边栏小工具</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-2" title="图文模块自动显示在最新文章模块第一篇文章的下面">图文模块自动显示在最新文章模块第一篇文章的下面</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-3" title="增加视频、图片、淘客独立分类页面模板">增加视频、图片、淘客独立分类页面模板</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-4" title="为视频、图片、淘客分类归档页面添加分类链接">为视频、图片、淘客分类归档页面添加分类链接</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-5" title="修改文章目录索引显示方式">修改文章目录索引显示方式</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-6" title="分别设置自动缩略图裁剪大小">分别设置自动缩略图裁剪大小</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-7" title="更换图片延迟加载JS脚本">更换图片延迟加载JS脚本</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-8" title="添加直达链接按钮">添加直达链接按钮</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-9" title="添加加载动画">添加加载动画</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-10" title="修改调整部分样式">修改调整部分样式</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#title-11" title="更新日志">更新日志</a></li>
                  </ul>
                  <span class="log-zd"><span class="log-close"><a title="隐藏目录"><i class="fa fa-times"></i><strong>文章目录</strong></a></span></span></div>
              </div>
              <p>&nbsp;</p>
              <p><img class="aligncenter" src="http://zmingcx.com/wp-content/uploads/2015/02/Beginz.jpg" alt="Begin主题 2.0 版正式发布！" width="760" height="300" /></p>
              <p>距Begin主题 1.8版，时隔三个多月，Begin主题 2.0 版正式发布，虽然中间也有多个版本推出，但都没有发文。</p>
              <p>如果你第一时启用新版主题，可能并不能感觉到有什么明显变化，这里就介绍一下主题带来的新变化：</p>
              <span class="directory"></span>
              <h4 id="title-0">更换Ajax滚动加载JS脚本</h4>
              <p>之前版本在文章自动Ajax加载结束后，只能手动翻页查看之前的文章，而新版本自动加载结束后，会显示一个按钮可以手动继续在当前页无限Ajax加载文章，该加载方式同时应用于评论加载。遗憾的是不能将当前加载的页数传替给正常的分页导航。</p>
              <span class="directory"></span>
              <h4 id="title-1">杂志布局添加单独的侧边栏小工具</h4>
              <p>为方便在使用杂志布局页面模板作为首页，新增加杂志布局专用的的侧边小工具，所以在启用新版本后，需要单独为杂志布局小工具添加相应项目。</p>
              <p>建议用户使用杂志布局页面模板作为首页，而不是在主题选项中直接选择杂志布局，以免首页在没有翻页的情况下搜索引擎收录相同的翻页内容，这也是国外杂志主题通用的方法。</p>
              <p>使用方法：新建页面---页面属性----模板---选择杂志布局，之后进入WP后台----设置---阅读---首页显示---勾选 “一个静态页面”---选择新建的页面即可。</p>
              <p>不要忘记为这个页面添加与正常首页SEO相同的文章描述和关键词。</p>
              <p>同理，公司主页也按上面操作。</p>
              <span class="directory"></span>
              <h4 id="title-2">图文模块自动显示在最新文章模块第一篇文章的下面</h4>
              <p>方便为读者推荐文章，也是为了使用排版看上去更加多样</p>
              <span class="directory"></span>
              <h4 id="title-3">增加视频、图片、淘客独立分类页面模板</h4>
              <p>自动将上述三种类型的文章，按分类显示。</p>
              <span class="directory"></span>
              <h4 id="title-4">为视频、图片、淘客分类归档页面添加分类链接</h4>
              <p>在视频、图片、淘客分类归档页面顶部添加该文章类型全部分类链接</p>
              <span class="directory"></span>
              <h4 id="title-5">修改文章目录索引显示方式</h4>
              <p>通过判断，在右侧滚屏模块中添加目录显示按钮，之前版本并没有集成在这个滚屏模块中，并添加提示，方便浏览者知晓按钮用途，刷新本页查看具体效果。</p>
              <span class="directory"></span>
              <h4 id="title-6">分别设置自动缩略图裁剪大小</h4>
              <p>可以分别设置正常文章和视频、图片、淘客等文章的自动缩略图裁剪大小，并取消缩略图高度限制。</p>
              <span class="directory"></span>
              <h4 id="title-7">更换图片延迟加载JS脚本</h4>
              <p>比之前的延迟加载会顺畅些，当然这个脚本在主题最初的版本中也有使用，后因兼容问题换成其他的方法。</p>
              <span class="directory"></span>
              <h4 id="title-8">添加直达链接按钮</h4>
              <p>方便发表商品之类文章直达商品购买页面。</p>
              <span class="directory"></span>
              <h4 id="title-9">添加加载动画</h4>
              <p>通过JQ脚本+CSS3动画实现的一个比较流行炫酷特效。</p>
              <span class="directory"></span>
              <h4 id="title-10">修改调整部分样式</h4>
              <p>比如QQ在线、顶部的搜索和分享、赞助、微信二维码弹出方式及外观样式等等，很多微小的细节都进行了修改调整。</p>
              <p>先写到这，更多更新内容请阅读下面的更新日志。</p>
              <span class="directory"></span>
              <h4 id="title-11">更新日志</h4>
              <p>展开更新日志<span class="show-more" title="显示隐藏"><span><i class="fa fa-caret-down"></i></span></span></p>
              <div class="section-content">
                </p>
                <p>2016年5月</p>
                <ul>
                  <li>更换文章Ajax滚动加载JS脚本</li>
                  <li>评论Ajax滚动加载</li>
                  <li>评论表单上移</li>
                  <li>更换图片延迟加载JS脚本</li>
                  <li>正文添加直达链接按钮</li>
                  <li>添加关于本站小工具</li>
                  <li>修改在线咨询按钮</li>
                  <li>美化分享按钮关注小工具样式</li>
                  <li>微信二维码及赞助二维码弹出样式</li>
                  <li>使用WP自带分页函数</li>
                  <li>更换正文图片幻灯JS脚本</li>
                  <li>滑动解锁提示</li>
                  <li>取消文章列表缩略图高度限制</li>
                  <li>分别设置不同模块缩略图裁剪大小</li>
                  <li>杂志首页分类模块可选择缩略图样式</li>
                  <li>增加商品栏目页面模板</li>
                  <li>公司首页幻灯在小屏设备上截断图片</li>
                  <li>登录界面添加注册链接</li>
                  <li>调用文章目录按钮位置</li>
                  <li>修改文章目录样式</li>
                  <li>视频、图片、淘客独立分类页面模板排除子分类文章</li>
                  <li>视频、图片、淘客分类模板添加该类型全部分类链接</li>
                  <li>调整部分样式</li>
                </ul>
                <p>2016年4月</p>
                <ul>
                  <li>重写图片九宫格布局结构</li>
                  <li>重写公司主页模板</li>
                  <li>调整淘客模板缩略调用</li>
                  <li>新增视频、图片、淘客独立分类页面模板</li>
                  <li>为视频、图片、淘客添加标签功能</li>
                  <li>解决内嵌媒体播放器与滑动解锁冲突</li>
                  <li>增加只显示标题的分类模板</li>
                  <li>首页添加文章推荐模块</li>
                  <li>首页图片布局添加幻灯</li>
                  <li>修正CMS布局添加无文章模块错位问题</li>
                  <li>分类页面添加文章推荐模块</li>
                  <li>改善导航菜单项目多而错位</li>
                  <li>添加自定义菜单页面模板</li>
                  <li>移动导航菜单添加链接到页面选择</li>
                  <li>添加首页是否显示站点副题开关</li>
                  <li>添加视频文章形式</li>
                  <li>九宫格布局添加设置显示篇数选项</li>
                  <li>视频文章缩略图添加自动裁剪</li>
                  <li>视频、图片、淘客、公告面包屑导航显示上一级分类</li>
                  <li>添加图片附件模板</li>
                  <li>图像文章形式添加自动裁剪缩略图</li>
                  <li>自定义阅读全文和直达链接按钮文字</li>
                  <li>修改图片延迟加载样式</li>
                  <li>调用页面首页面包屑导航</li>
                  <li>添加滚动动画特效</li>
                  <li>调整部分样式</li>
                </ul>
                <div class="section-content">
                  <p>2016年3月</p>
                  <ul>
                    <li>修改移动端导航菜单样式</li>
                    <li>添加即将发布文章小工具</li>
                    <li>支持短代码嵌套短代码</li>
                    <li>修改分享及二维码弹出方式</li>
                    <li>增加转载文章来源自定义栏目</li>
                    <li>文章列表添加直达链接按钮</li>
                    <li>修改图片、视频、商品、公告面包屑导航</li>
                    <li>修改商品列表缩略图链接及样式</li>
                    <li>修改商品独立页面模板样式</li>
                    <li>修正与IE8兼容性</li>
                    <li>CMS布局两栏分类模块添加缩略图样式切换开关</li>
                    <li>删除用文章原图片作为缩略图功能</li>
                    <li>默认启用自动裁剪文章中的图片为缩略图</li>
                    <li>CMS单栏小工具长标题添加截断</li>
                    <li>修改“关注我们”小工具，直接在小工具中设置内容</li>
                    <li>修正部分样式错误</li>
                  </ul>
                  <p>2016年2月</p>
                  <ul>
                    <li>修正与PHP7的兼容性</li>
                    <li>修正淘客模板正文点赞分享按钮问题</li>
                    <li>修正企业模板底部链接样式错误</li>
                    <li>修正淘客模板图片链接问题</li>
                    <li>添加公司主页独立页面模板</li>
                    <li>添加作者墙小工具</li>
                    <li>添加作者墙独立页面模板</li>
                  </ul>
                </div>
                <p>
              </div>
            </div>
            <div class="clear"></div>
            <div id="social">
              <div class="social-main"> <span class="like"> <a href="javascript:;" data-action="ding" data-id="7146" title="我赞" class="favorite"><i class="fa fa-thumbs-up"></i>赞 <i class="count"> 49</i> </a> </span> <span class="shang-p"> <span class="tipso_style" id="tip-p" data-tipso='<div id="shang"><div class="shang-main"><h4><i class="fa fa-heart" aria-hidden="true"></i> 如果认为本文对您有所帮助请赞助本站</h4><div class="shang-img"> <img src="http://zmingcx.com/wp-content/uploads/2015/11/alipay.png" /><h4>支付宝扫一扫赞助</h4></div><div class="shang-img"> <img src="http://zmingcx.com/wp-content/uploads/2015/11/alipay.png" /><h4>微信钱包扫描赞助</h4></div><div class="clear"></div></div></div>'> <span class="shang-s"><a title="赞助本站">赏</a></span></span> </span> <span class="share-sd"> <span class="share-s"><a href="javascript:void(0)" id="share-s" title="分享"><i class="fa fa-share-alt"></i>分享</a></span>
                <div id="share">
                  <ul class="bdsharebuttonbox">
                    <li><a title="更多" class="bds_more fa fa-plus-square" data-cmd="more"></a></li>
                    <li><a title="分享到QQ空间" class="fa fa-qq" data-cmd="qzone" href="#"></a></li>
                    <li><a title="分享到新浪微博" class="fa fa-weibo" data-cmd="tsina"></a></li>
                    <li><a title="分享到腾讯微博" class="fa fa-pinterest-square" data-cmd="tqq"></a></li>
                    <li><a title="分享到人人网" class="fa fa-renren" data-cmd="renren"></a></li>
                    <li><a title="分享到微信" class="fa fa-weixin" data-cmd="weixin"></a></li>
                  </ul>
                </div>
                </span>
                <div class="clear"></div>
              </div>
            </div>
            <footer class="single-footer">
              <ul class="single-meta">
                <li class="comment"><a href="http://zmingcx.com/2-0-release-the-begin.html#comments"><i class="fa fa-comment-o"></i> 28</a></li>
                <li class="views"><i class="fa fa-eye"></i> 1,420</li>
                <li class="r-hide"><a href="javascript:pr()" title="侧边栏"><i class="fa fa-caret-left"></i> <i class="fa fa-caret-right"></i></a></li>
              </ul>
              <ul id="fontsize">
                A+
              </ul>
              <div class="single-cat-tag">
                <div class="single-cat">发布日期：2016年05月15日&nbsp;&nbsp;所属分类：<a href="http://zmingcx.com/category/works/" rel="category tag">原创主题</a></div>
                <div class="single-tag">标签：<a href="http://zmingcx.com/tag/wordpress/" rel="tag">WordPress</a><a href="http://zmingcx.com/tag/wordpress%e4%b8%bb%e9%a2%98/" rel="tag">WordPress主题</a><a href="http://zmingcx.com/tag/%e5%8e%9f%e5%88%9b%e4%b8%bb%e9%a2%98/" rel="tag">原创主题</a></div>
              </div>
            </footer>
            <div class="clear"></div>
          </div>
        </article>
      </div>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div class="authorbio"> <img alt="" src='http://zmingcx.com/wp-content/uploads/gravatar/1498899a405ed8ddf3d8c0a4bf6aa0bb-s64.jpg' class="avatar avatar-64" width="64" height="64" />
          <ul class="spostinfo">
            <li><strong>版权声明：</strong>本站原创文章，于7天前，由<a href="http://zmingcx.com/author/robin/" title="由知更鸟发布" rel="author">知更鸟</a>发表，共 1949字。</li>
            <li><strong>转载请注明：</strong><a href="http://zmingcx.com/2-0-release-the-begin.html" rel="bookmark" title="本文固定链接 http://zmingcx.com/2-0-release-the-begin.html">Begin主题 2.0 版正式发布！ | 知更鸟</a><a href="#" onclick="copy_code('http://zmingcx.com/2-0-release-the-begin.html'); return false;"> +复制链接</a></li>
          </ul>
        </div>
      </div>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div id="related-img">
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww4.sinaimg.cn/large/703be3b1jw1f3qh39r9qhj20ju05ut9g.jpg&w=280&h=210&zc=1" alt="自动从其它站点RSS抓取文章" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html">自动从其它站点RSS抓取文章</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/nix-gravatar-cache-modified.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f2uvqv8swij20j605n3zk.jpg&w=280&h=210&zc=1" alt="头像缓存插件：nix-gravatar-cache 修改版" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/nix-gravatar-cache-modified.html">头像缓存插件：nix-gravatar-cache 修改版</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/wordpress-4-5-came.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/mw690/703be3b1jw1f2rfzcqeezj20ju05u3z3.jpg&w=280&h=210&zc=1" alt="WordPress 4.5 如期而至" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/wordpress-4-5-came.html">WordPress 4.5 如期而至</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/wordpress-4-5-rc1.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f2oyx6sc5gj20ju05u3za.jpg&w=280&h=210&zc=1" alt="WordPress 4.5正式版即将发布" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/wordpress-4-5-rc1.html">WordPress 4.5正式版即将发布</a></div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div id="single-widget">
        <div class="wow fadeInUp" data-wow-delay="0.3s">
          <aside id="related_post-7" class="widget widget_related_post wow fadeInUp" data-wow-delay="0.3s">
            <h3 class="widget-title">
              <div class="s-icon"></div>
              相关文章</h3>
            <div id="related_post_widget">
              <ul>
                <li><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html">自动从其它站点RSS抓取文章</a></li>
                <li><a href="http://zmingcx.com/nix-gravatar-cache-modified.html">头像缓存插件：nix-gravatar-cache 修改版</a></li>
                <li><a href="http://zmingcx.com/wordpress-4-5-came.html">WordPress 4.5 如期而至</a></li>
                <li><a href="http://zmingcx.com/wordpress-4-5-rc1.html">WordPress 4.5正式版即将发布</a></li>
                <li><a href="http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html">解答一个WordPress文章中插入图片的困惑</a></li>
              </ul>
            </div>
            <div class="clear"></div>
          </aside>
          <aside id="hot_comment-17" class="widget widget_hot_comment wow fadeInUp" data-wow-delay="0.3s">
            <h3 class="widget-title">
              <div class="s-icon"></div>
              热评文章</h3>
            <div id="hot_comment_widget">
              <ul>
                <li><span class='li-icon li-icon-1'>1</span><a href= "http://zmingcx.com/show-the-upcoming-articles.html" rel="bookmark" title=" (58条评论)" >WordPress显示即将发布的文章列表</a></li>
                <li><span class='li-icon li-icon-2'>2</span><a href= "http://zmingcx.com/nix-gravatar-cache-modified.html" rel="bookmark" title=" (37条评论)" >头像缓存插件：nix-gravatar-cache 修改版</a></li>
                <li><span class='li-icon li-icon-3'>3</span><a href= "http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html" rel="bookmark" title=" (31条评论)" >解答一个WordPress文章中插入图片的困惑</a></li>
                <li><span class='li-icon li-icon-4'>4</span><a href= "http://zmingcx.com/2-0-release-the-begin.html" rel="bookmark" title=" (28条评论)" >Begin主题 2.0 版正式发布！</a></li>
                <li><span class='li-icon li-icon-5'>5</span><a href= "http://zmingcx.com/wordpress-4-5-came.html" rel="bookmark" title=" (18条评论)" >WordPress 4.5 如期而至</a></li>
              </ul>
            </div>
            <div class="clear"></div>
          </aside>
        </div>
        <div class="clear"></div>
      </div>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div class="ad-pc ad-site"><a href="http://zmingcx.com/begin.html" target="_blank"><img src="http://zmingcx.com/wp-content/uploads/2015/02/beginad.jpg" alt="Begin主题购买" /></a></div>
      </div>
      <nav class="nav-single wow fadeInUp" data-wow-delay="0.3s"> <a href="http://zmingcx.com/picture-phone.html" rel="prev"><span class="meta-nav"><span class="post-nav"><i class="fa fa-angle-left"></i> 上一篇</span><br/>
        图像日志自动截图测试：手机</span></a><span class='meta-nav'><span class='post-nav'>没有了<br/>
        </span>已是最新文章</span>
        <div class="clear"></div>
      </nav>
      <nav class="navigation post-navigation" role="navigation">
        <h2 class="screen-reader-text">文章导航</h2>
        <div class="nav-links">
          <div class="nav-previous"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html" rel="prev"><span class="meta-nav-r" aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></div>
          <div class="nav-next"><a href="http://zmingcx.com/sony-xperia-x.html" rel="next"><span class="meta-nav-l" aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></div>
        </div>
      </nav>
      <div class="scroll-comments"></div>
      <div id="comments" class="comments-area">
        <div id="respond" class="comment-respond wow fadeInUp" data-wow-delay="0.3s">
          <h3 id="reply-title" class="comment-reply-title"><i class="fa fa-pencil-square-o"></i>发表评论<small><a rel="nofollow" id="cancel-comment-reply-link" href="/2-0-release-the-begin.html#respond" style="display:none;">取消回复</a></small></h3>
          <form action="http://zmingcx.com/wp-comments-post.php" method="post" id="commentform">
            <div id="comment-author-info">
              <p class="comment-form-author">
                <input type="text" name="author" id="author" class="commenttext" value="" tabindex="1" />
                <label for="author">昵称（必填）</label>
              </p>
              <p class="comment-form-email">
                <input type="text" name="email" id="email" class="commenttext" value="" tabindex="2" />
                <label for="email">邮箱（必填）</label>
              </p>
              <p class="comment-form-url">
                <input type="text" name="url" id="url" class="commenttext" value="" tabindex="3" />
                <label for="url">网址</label>
              </p>
            </div>
            <p class="emoji-box"><script type="text/javascript">/*  */
    function grin(tag) {
    	var myField;
    	tag = ' ' + tag + ' ';
        if (document.getElementById('comment') && document.getElementById('comment').type == 'textarea') {
    		myField = document.getElementById('comment');
    	} else {
    		return false;
    	}
    	if (document.selection) {
    		myField.focus();
    		sel = document.selection.createRange();
    		sel.text = tag;
    		myField.focus();
    	}
    	else if (myField.selectionStart || myField.selectionStart == '0') {
    		var startPos = myField.selectionStart;
    		var endPos = myField.selectionEnd;
    		var cursorPos = endPos;
    		myField.value = myField.value.substring(0, startPos)
    					  + tag
    					  + myField.value.substring(endPos, myField.value.length);
    		cursorPos += tag.length;
    		myField.focus();
    		myField.selectionStart = cursorPos;
    		myField.selectionEnd = cursorPos;
    	}
    	else {
    		myField.value += tag;
    		myField.focus();
    	}
    }
/*  */</script> <a href="javascript:grin(':?:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_question.gif" alt=":?:" title="疑问" /></a> <a href="javascript:grin(':razz:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_razz.gif" alt=":razz:" title="调皮" /></a> <a href="javascript:grin(':sad:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_sad.gif" alt=":sad:" title="难过" /></a> <a href="javascript:grin(':evil:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_evil.gif" alt=":evil:" title="抠鼻" /></a> <a href="javascript:grin(':!:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_exclaim.gif" alt=":!:" title="吓" /></a> <a href="javascript:grin(':smile:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_smile.gif" alt=":smile:" title="微笑" /></a> <a href="javascript:grin(':oops:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_redface.gif" alt=":oops:" title="憨笑" /></a> <a href="javascript:grin(':grin:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_biggrin.gif" alt=":grin:" title="坏笑" /></a> <a href="javascript:grin(':eek:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_surprised.gif" alt=":eek:" title="惊讶" /></a> <a href="javascript:grin(':shock:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_eek.gif" alt=":shock:" title="发呆" /></a> <a href="javascript:grin(':???:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_confused.gif" alt=":???:" title="撇嘴" /></a> <a href="javascript:grin(':cool:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_cool.gif" alt=":cool:" title="大兵" /></a> <a href="javascript:grin(':lol:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_lol.gif" alt=":lol:" title="偷笑" /></a> <a href="javascript:grin(':mad:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_mad.gif" alt=":mad:" title="咒骂" /></a> <a href="javascript:grin(':twisted:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_twisted.gif" alt=":twisted:" title="发怒" /></a> <a href="javascript:grin(':roll:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_rolleyes.gif" alt=":roll:" title="白眼" /></a> <a href="javascript:grin(':wink:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_wink.gif" alt=":wink:" title="鼓掌" /></a> <a href="javascript:grin(':idea:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_idea.gif" alt=":idea:" title="酷" /></a> <a href="javascript:grin(':arrow:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_arrow.gif" alt=":arrow:" title="擦汗" /></a> <a href="javascript:grin(':neutral:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_neutral.gif" alt=":neutral:" title="亲亲" /></a> <a href="javascript:grin(':cry:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_cry.gif" alt=":cry:" title="大哭" /></a> <a href="javascript:grin(':mrgreen:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="呲牙" /></a> <br />
            </p>
            <div class="clear"></div>
            <div class="qaptcha"></div>
            <p class="comment-form-comment">
              <textarea id="comment" name="comment" rows="4" tabindex="4"></textarea>
            </p>
            <p class="comment-tool"> <a class="tool-img" href='javascript:embedImage();' title="插入图片"><i class="icon-img"></i><i class="fa fa-picture-o"></i></a> <a class="emoji" href="" title="插入表情"><i class="fa fa-meh-o"></i></a></p>
            <p class="form-submit">
              <input id="submit" name="submit" type="submit" tabindex="5" value="提交评论"/>
              <input type='hidden' name='comment_post_ID' value='7146' id='comment_post_ID' />
              <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
            </p>
          </form>
        </div>
        <h2 class="comments-title wow fadeInUp" data-wow-delay="0.3s"> 目前评论：28 &nbsp;&nbsp;其中：访客&nbsp;&nbsp;28 &nbsp;&nbsp;博主&nbsp;&nbsp;0</h2>
        <ol class="comment-list">
          <div id="anchor">
            <div id="comment-57120"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1" id="comment-57120">
          <div id="div-comment-57120" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/fb0639b19d43a66c9a1c64bb167bd703-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.d9y.net' rel='external nofollow' target='_blank' class='url'>Koolight</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57120"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57120#respond' onclick='return addComment.moveForm( "div-comment-57120", "57120", "respond", "7146" )' aria-label='回复给Koolight'>回复</a></span> 2016年05月15日 14点00分 <span class="floor"> &nbsp;沙发 </span> </span> </span></div>
            <p>不知道大神新主题什么时候发布啊！</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57121"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-odd thread-alt depth-1 parent" id="comment-57121">
          <div id="div-comment-57121" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/5268952e63c3d2d613f07eea35a87e7c-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://ziji.cc' rel='external nofollow' target='_blank' class='url'>Page</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57121"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57121#respond' onclick='return addComment.moveForm( "div-comment-57121", "57121", "respond", "7146" )' aria-label='回复给Page'>回复</a></span> 2016年05月15日 14点36分 <span class="floor"> &nbsp;板凳 </span> </span> </span></div>
            <p>想用begin做单纯的视频主题，可以不？</p>
          </div>
          <ul class="children">
            <div id="anchor">
              <div id="comment-57164"></div>
            </div>
            <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even depth-2" id="comment-57164">
            <div id="div-comment-57164" class="comment-body">
              <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/5fd6be36df4ac979c3288dc1b493401b-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.sdtclass.com' rel='external nofollow' target='_blank' class='url'>yumanutong</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57164"></a><br />
                <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57164#respond' onclick='return addComment.moveForm( "div-comment-57164", "57164", "respond", "7146" )' aria-label='回复给yumanutong'>回复</a></span> 2016年05月20日 00点34分 <span class="floor"> &nbsp;1层 </span> </span> </span></div>
              <p><span class="at">@<a href="#comment-57121">Page</a></span> 可以啊。你可以看看鸟哥的DEMO</p>
            </div>
            </li>
          </ul>
          </li>
          <div id="anchor">
            <div id="comment-57122"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57122">
          <div id="div-comment-57122" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/31e015a880cfc317fa3ac3c29176d5a5-s64.jpg" /> <strong> 唐情 </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57122"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57122#respond' onclick='return addComment.moveForm( "div-comment-57122", "57122", "respond", "7146" )' aria-label='回复给唐情'>回复</a></span> 2016年05月15日 14点36分 <span class="floor"> &nbsp;地板 </span> </span> </span></div>
            <p>鸟哥很给力</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57123"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57123">
          <div id="div-comment-57123" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/ac1895ec9b91395e450b280b1fbff2a2-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://baike.0752118.com' rel='external nofollow' target='_blank' class='url'>惠州百科</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57123"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57123#respond' onclick='return addComment.moveForm( "div-comment-57123", "57123", "respond", "7146" )' aria-label='回复给惠州百科'>回复</a></span> 2016年05月15日 15点03分 <span class="floor"> &nbsp;4楼 </span> </span> </span></div>
            <p>文章目录变得更漂亮了，要是能跟随页面滚动和全部显示选项就更完美了</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57124"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57124">
          <div id="div-comment-57124" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/69d0a95f21ea8082284bc8b083373545-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.raypin.com' rel='external nofollow' target='_blank' class='url'>瑞品|raypin.com</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57124"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57124#respond' onclick='return addComment.moveForm( "div-comment-57124", "57124", "respond", "7146" )' aria-label='回复给瑞品|raypin.com'>回复</a></span> 2016年05月15日 15点15分 <span class="floor"> &nbsp;5楼 </span> </span> </span></div>
            <p>鸟哥就是给力，加入了商品直达按钮，真心爽了，预估一大波淘客杀过来购买</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57125"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57125">
          <div id="div-comment-57125" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/a2ec712dabfde816b3151dbf6d092bd2-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://xunjinbiji.com/' rel='external nofollow' target='_blank' class='url'>寻金笔记</a> </strong> <a class="vip4" title="评论达人 VIP.4"><i class="fa fa-vimeo-square"></i><span class="lv">4</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57125"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57125#respond' onclick='return addComment.moveForm( "div-comment-57125", "57125", "respond", "7146" )' aria-label='回复给寻金笔记'>回复</a></span> 2016年05月15日 16点33分 <span class="floor"> &nbsp;6楼 </span> </span> </span></div>
            <p>2.0确实不错，有时间升级一下。 <img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_biggrin.gif" alt=":grin:" class="wp-smiley" style="height: 1em; max-height: 1em;" /></p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57126"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57126">
          <div id="div-comment-57126" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/9b04e8cd48186f989df11890bb76a5d1-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.reteng.org' rel='external nofollow' target='_blank' class='url'>热腾网</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57126"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57126#respond' onclick='return addComment.moveForm( "div-comment-57126", "57126", "respond", "7146" )' aria-label='回复给热腾网'>回复</a></span> 2016年05月15日 17点04分 <span class="floor"> &nbsp;7楼 </span> </span> </span></div>
            <p>不知道使用内容播放器  播放MP3 一些JS不懂使用的问题解决了没？</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57127"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57127">
          <div id="div-comment-57127" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/6f986346c4eb0801e594b7d49985b8ae-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.dseme.com' rel='external nofollow' target='_blank' class='url'>点思博客</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57127"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57127#respond' onclick='return addComment.moveForm( "div-comment-57127", "57127", "respond", "7146" )' aria-label='回复给点思博客'>回复</a></span> 2016年05月15日 18点00分 <span class="floor"> &nbsp;8楼 </span> </span> </span></div>
            <p>静待鸟哥给我发升级的主题！！</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57128"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57128">
          <div id="div-comment-57128" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/c66ed37853c36e0b6a9b8ff26de43e77-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://dockerthink.com' rel='external nofollow' target='_blank' class='url'>西秦公子</a> </strong> <a class="vip0" title="评论达人 VIP.0"><i class="fa fa-vimeo-square"></i><span class="lv">0</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57128"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57128#respond' onclick='return addComment.moveForm( "div-comment-57128", "57128", "respond", "7146" )' aria-label='回复给西秦公子'>回复</a></span> 2016年05月15日 19点15分 <span class="floor"> &nbsp;9楼 </span> </span> </span></div>
            <p>坐等发更新的2.0包</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57130"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57130">
          <div id="div-comment-57130" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/5d5cc47e5aca2ab5858d730e54055bed-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=https://immmmm.com' rel='external nofollow' target='_blank' class='url'>林木木</a> </strong> <a class="vip2" title="评论达人 VIP.2"><i class="fa fa-vimeo-square"></i><span class="lv">2</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57130"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57130#respond' onclick='return addComment.moveForm( "div-comment-57130", "57130", "respond", "7146" )' aria-label='回复给林木木'>回复</a></span> 2016年05月15日 22点29分 <span class="floor"> &nbsp;10楼 </span> </span> </span></div>
            <p>支持分享！首页“返回首页”算BUG不？</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57131"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57131">
          <div id="div-comment-57131" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/72de5de022d06607f16a2990b0e4e706-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://dyrm.cn' rel='external nofollow' target='_blank' class='url'>腊五</a> </strong> <a class="vip0" title="评论达人 VIP.0"><i class="fa fa-vimeo-square"></i><span class="lv">0</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57131"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57131#respond' onclick='return addComment.moveForm( "div-comment-57131", "57131", "respond", "7146" )' aria-label='回复给腊五'>回复</a></span> 2016年05月15日 22点37分 <span class="floor"> &nbsp;11楼 </span> </span> </span></div>
            <p>已经发送邮件到鸟大邮箱。希望能尽快收到主题升级包</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57132"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1 parent" id="comment-57132">
          <div id="div-comment-57132" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/74cd35f276a5c2108c653a971b718c20-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://LouisHan.com/' rel='external nofollow' target='_blank' class='url'>路易大叔</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57132"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57132#respond' onclick='return addComment.moveForm( "div-comment-57132", "57132", "respond", "7146" )' aria-label='回复给路易大叔'>回复</a></span> 2016年05月16日 00点10分 <span class="floor"> &nbsp;12楼 </span> </span> </span></div>
            <p>买不起还是赞一个</p>
          </div>
          <ul class="children">
            <div id="anchor">
              <div id="comment-57158"></div>
            </div>
            <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt depth-2" id="comment-57158">
            <div id="div-comment-57158" class="comment-body">
              <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/24a0d07e603264cc4f307917a93cbbc3-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://sunbox.cc' rel='external nofollow' target='_blank' class='url'>aunsen</a> </strong> <a class="vip4" title="评论达人 VIP.4"><i class="fa fa-vimeo-square"></i><span class="lv">4</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57158"></a><br />
                <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57158#respond' onclick='return addComment.moveForm( "div-comment-57158", "57158", "respond", "7146" )' aria-label='回复给aunsen'>回复</a></span> 2016年05月18日 18点14分 <span class="floor"> &nbsp;1层 </span> </span> </span></div>
              <p><span class="at">@<a href="#comment-57132">路易大叔</a></span> 噗！</p>
            </div>
            </li>
          </ul>
          </li>
          <div id="anchor">
            <div id="comment-57134"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1" id="comment-57134">
          <div id="div-comment-57134" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/8b35b5af61ab64641fa69cc49706035a-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.tourongji.com' rel='external nofollow' target='_blank' class='url'>投融记</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57134"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57134#respond' onclick='return addComment.moveForm( "div-comment-57134", "57134", "respond", "7146" )' aria-label='回复给投融记'>回复</a></span> 2016年05月16日 09点42分 <span class="floor"> &nbsp;13楼 </span> </span> </span></div>
            <p>玩wp只一个知更鸟就够了。</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57136"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-odd thread-alt depth-1" id="comment-57136">
          <div id="div-comment-57136" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/a0a4672c67f9d2e44cc7e88de3e90bed-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://2days.org' rel='external nofollow' target='_blank' class='url'>两天</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57136"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57136#respond' onclick='return addComment.moveForm( "div-comment-57136", "57136", "respond", "7146" )' aria-label='回复给两天'>回复</a></span> 2016年05月16日 12点54分 <span class="floor"> &nbsp;14楼 </span> </span> </span></div>
            <p>等待鸟哥给我发新版~~~</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57137"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1" id="comment-57137">
          <div id="div-comment-57137" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/f5f3c8b0eb962a8982d8a12e03e1bd8b-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.loveo.cc' rel='external nofollow' target='_blank' class='url'>墨丶水瓶</a> </strong> <a class="vip4" title="评论达人 VIP.4"><i class="fa fa-vimeo-square"></i><span class="lv">4</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57137"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57137#respond' onclick='return addComment.moveForm( "div-comment-57137", "57137", "respond", "7146" )' aria-label='回复给墨丶水瓶'>回复</a></span> 2016年05月16日 13点51分 <span class="floor"> &nbsp;15楼 </span> </span> </span></div>
            <p>啊哈，新版很不错啊，坐等更新 &#8230;. <img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_neutral.gif" alt=":neutral:" class="wp-smiley" style="height: 1em; max-height: 1em;" /></p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57138"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-odd thread-alt depth-1" id="comment-57138">
          <div id="div-comment-57138" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/95b6a2b91066f7783eb78cc0210d2309-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://boke112.com/' rel='external nofollow' target='_blank' class='url'>boke112导航</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57138"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57138#respond' onclick='return addComment.moveForm( "div-comment-57138", "57138", "respond", "7146" )' aria-label='回复给boke112导航'>回复</a></span> 2016年05月16日 22点03分 <span class="floor"> &nbsp;16楼 </span> </span> </span></div>
            <p>功能越来越多，越来越完善，恭喜 <img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_wink.gif" alt=":wink:" class="wp-smiley" style="height: 1em; max-height: 1em;" /></p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57142"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1" id="comment-57142">
          <div id="div-comment-57142" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/6649af960388d66fe86c9ede272681b4-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.kemei.so' rel='external nofollow' target='_blank' class='url'>软膜天花</a> </strong> <a class="vip4" title="评论达人 VIP.4"><i class="fa fa-vimeo-square"></i><span class="lv">4</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57142"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57142#respond' onclick='return addComment.moveForm( "div-comment-57142", "57142", "respond", "7146" )' aria-label='回复给软膜天花'>回复</a></span> 2016年05月17日 10点12分 <span class="floor"> &nbsp;17楼 </span> </span> </span></div>
            <p>这个主题很漂亮啊！</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57149"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-odd thread-alt depth-1" id="comment-57149">
          <div id="div-comment-57149" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/f886b6944140fadbcba05c89a4949e77-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.ub918.com/' rel='external nofollow' target='_blank' class='url'>通宝娱乐下载</a> </strong> <a class="vip1" title="评论达人 VIP.1"><i class="fa fa-vimeo-square"></i><span class="lv">1</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57149"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57149#respond' onclick='return addComment.moveForm( "div-comment-57149", "57149", "respond", "7146" )' aria-label='回复给通宝娱乐下载'>回复</a></span> 2016年05月17日 16点16分 <span class="floor"> &nbsp;18楼 </span> </span> </span></div>
            <p>坐等大神主题发布！</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57152"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1" id="comment-57152">
          <div id="div-comment-57152" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/53b91bf45b48a0cfd2d960eb9c6423e5-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.pianduan.me' rel='external nofollow' target='_blank' class='url'>片段艺文志</a> </strong> <a class="vip2" title="评论达人 VIP.2"><i class="fa fa-vimeo-square"></i><span class="lv">2</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57152"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57152#respond' onclick='return addComment.moveForm( "div-comment-57152", "57152", "respond", "7146" )' aria-label='回复给片段艺文志'>回复</a></span> 2016年05月17日 18点32分 <span class="floor"> &nbsp;19楼 </span> </span> </span></div>
            <p>漂亮的主题赞一个!</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57157"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-odd thread-alt depth-1" id="comment-57157">
          <div id="div-comment-57157" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/0dfc3376cde17a332f415f7c156edef5-s64.jpg" /> <strong> GAvin </strong> <a class="vip0" title="评论达人 VIP.0"><i class="fa fa-vimeo-square"></i><span class="lv">0</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57157"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57157#respond' onclick='return addComment.moveForm( "div-comment-57157", "57157", "respond", "7146" )' aria-label='回复给GAvin'>回复</a></span> 2016年05月18日 15点09分 <span class="floor"> &nbsp;20楼 </span> </span> </span></div>
            <p>站長您好</p>
            <p>我在台灣，能使用PAYPAL付款嗎？</p>
            <p>後續的處理流程再請您協助我，感謝您。</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57160"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-even depth-1 parent" id="comment-57160">
          <div id="div-comment-57160" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/17a227608839bf4889bb045c54a0c0a3-s64.jpg" /> <strong> bssn </strong> <a class="vip0" title="评论达人 VIP.0"><i class="fa fa-vimeo-square"></i><span class="lv">0</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57160"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57160#respond' onclick='return addComment.moveForm( "div-comment-57160", "57160", "respond", "7146" )' aria-label='回复给bssn'>回复</a></span> 2016年05月18日 22点14分 <span class="floor"> &nbsp;21楼 </span> </span> </span></div>
            <p>最好的还是移动端的那个banner，滑动切换早该上了。移动端的表现需大力加强</p>
          </div>
          <ul class="children">
            <div id="anchor">
              <div id="comment-57161"></div>
            </div>
            <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt depth-2" id="comment-57161">
            <div id="div-comment-57161" class="comment-body">
              <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/8a8155d0ffd8713e23feb74c5f963314-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.ymanz.com' rel='external nofollow' target='_blank' class='url'>玉满斋</a> </strong> <a class="vip4" title="评论达人 VIP.4"><i class="fa fa-vimeo-square"></i><span class="lv">4</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57161"></a><br />
                <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57161#respond' onclick='return addComment.moveForm( "div-comment-57161", "57161", "respond", "7146" )' aria-label='回复给玉满斋'>回复</a></span> 2016年05月19日 16点08分 <span class="floor"> &nbsp;1层 </span> </span> </span></div>
              <p><span class="at">@<a href="#comment-57160">bssn</a></span> 嗯，有道理，移动端现在是流量主要来源！</p>
            </div>
            </li>
          </ul>
          </li>
          <div id="anchor">
            <div id="comment-57163"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57163">
          <div id="div-comment-57163" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/5fd6be36df4ac979c3288dc1b493401b-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.sdtclass.com' rel='external nofollow' target='_blank' class='url'>yumanutong</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57163"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57163#respond' onclick='return addComment.moveForm( "div-comment-57163", "57163", "respond", "7146" )' aria-label='回复给yumanutong'>回复</a></span> 2016年05月20日 00点33分 <span class="floor"> &nbsp;22楼 </span> </span> </span></div>
            <p>啦啦啦，坐等签发。</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57165"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57165">
          <div id="div-comment-57165" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/c0961ee88aca598301b92a5517402d44-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://videoclub.cn/' rel='external nofollow' target='_blank' class='url'>电影天堂迅雷下载</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57165"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57165#respond' onclick='return addComment.moveForm( "div-comment-57165", "57165", "respond", "7146" )' aria-label='回复给电影天堂迅雷下载'>回复</a></span> 2016年05月20日 09点49分 <span class="floor"> &nbsp;23楼 </span> </span> </span></div>
            <p>绝对的支持<br />
              不错的</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57170"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment even thread-odd thread-alt depth-1" id="comment-57170">
          <div id="div-comment-57170" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/c0961ee88aca598301b92a5517402d44-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://videoclub.cn/' rel='external nofollow' target='_blank' class='url'>电影天堂迅雷下载</a> </strong> <a class="vip5" title="评论达人 VIP.5"><i class="fa fa-vimeo-square"></i><span class="lv">5</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57170"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57170#respond' onclick='return addComment.moveForm( "div-comment-57170", "57170", "respond", "7146" )' aria-label='回复给电影天堂迅雷下载'>回复</a></span> 2016年05月20日 19点15分 <span class="floor"> &nbsp;24楼 </span> </span> </span></div>
            <p>超级酷 已经开始使用了</p>
          </div>
          </li>
          <div id="anchor">
            <div id="comment-57171"></div>
          </div>
          <li class="wow fadeInUp" data-wow-delay="0.3s" class="comment odd alt thread-even depth-1" id="comment-57171">
          <div id="div-comment-57171" class="comment-body">
            <div class="comment-author vcard"> <img class="avatar" src="http://zmingcx.com/wp-content/themes/begin/img/load-avatar.gif" alt="avatar" data-original="http://zmingcx.com/wp-content/uploads/gravatar/1d7400c9cdcd8deb75e6fdbb7c776224-s64.jpg" /> <strong> <a href='http://zmingcx.com/wp-content/themes/begin/inc/go.php?url=http://www.lixuejiang.com/' rel='external nofollow' target='_blank' class='url'>Jane博客</a> </strong> <a class="vip0" title="评论达人 VIP.0"><i class="fa fa-vimeo-square"></i><span class="lv">0</span></a> <span class="comment-meta commentmetadata"> <a href="http://zmingcx.com/2-0-release-the-begin.html/#comment-57171"></a><br />
              <span class="comment-aux"> <span class="reply"><a rel='nofollow' class='comment-reply-link' href='http://zmingcx.com/2-0-release-the-begin.html?replytocom=57171#respond' onclick='return addComment.moveForm( "div-comment-57171", "57171", "respond", "7146" )' aria-label='回复给Jane博客'>回复</a></span> 2016年05月20日 23点19分 <span class="floor"> &nbsp;25楼 </span> </span> </span></div>
            <p>喜欢新版幻灯片样式</p>
          </div>
          </li>
        </ol>
      </div>
    </main>
  </div>
  <?php include 'common/sidebar.php';?>
  <div class="clear"></div>
  <div class="clear"></div>
</div>
<?php include 'common/footer-widget-box.php';?>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="site-info wow fadeInUp" data-wow-delay="0.6s"> Copyright ©  知更鸟  版权所有. <a href="http://www.miitbeian.gov.cn" target="_blank">辽ICP备12016417号-1</a> <span class="add-info"> <a href="http://idc.wopus.org/host/yun/" target="_blank"><img class="" title="采用wopus云主机" src="http://zmingcx.com/wp-content/uploads/2015/04/idc.png" alt="" /></a> </span></div>
</footer>
<div id="login">
  <div class="login-t">用户登录</div>
  <form action="http://zmingcx.com/wp-login.php?redirect_to=http%3A%2F%2Fzmingcx.com%2F2-0-release-the-begin.html" method="post" id="loginform">
    <input type="username" type="text"  name="log" id="log" placeholder="名称" required/>
    <input type="password" name="pwd" id="pwd" placeholder="密码" required/>
    <input type="submit" id="submit" value="登录">
    <input type="hidden" name="redirect_to" value="/2-0-release-the-begin.html" />
    <label>
      <input type="checkbox" name="rememberme" id="modlogn_remember" value="yes"  checked="checked" alt="Remember Me" >
      自动登录</label>
    <a href="http://zmingcx.com/wp-login.php?action=lostpassword">&nbsp;&nbsp;忘记密码？</a>
  </form>
  <div class="login-b"></div>
</div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"16"},"share":{"bdSize":16}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<?php include 'common/scroll.php'?>
<script type="text/javascript" src="http://zmingcx.com/wp-content/themes/begin/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://zmingcx.com/wp-content/themes/begin/js/qaptcha.jquery.js"></script>
<script type="text/javascript">var QaptchaJqueryPage="http://zmingcx.com/wp-content/themes/begin/inc/function/qaptcha.jquery.php"</script>
<script type="text/javascript">$(document).ready(function(){$('.qaptcha').QapTcha();});</script>
<script type='text/javascript'>/*  */var viewsCacheL10n = {"admin_ajax_url":"http:\/\/zmingcx.com\/wp-admin\/admin-ajax.php","post_id":"7146"};/*  */</script>
<script type='text/javascript' src='http://zmingcx.com/wp-content/plugins/wp-postviews/postviews-cache.js?ver=1.68'></script>
<script type='text/javascript' src='http://zmingcx.com/wp-content/themes/begin/js/superfish.js?ver=2016.05.16'></script>
<script type='text/javascript' src='http://zmingcx.com/wp-content/themes/begin/js/3dtag.js?ver=4.5.2'></script>
<script type="text/javascript">var ias=$.ias({container:"#comments",item:".comment-list",pagination:".scroll-links",next:".scroll-links .nav-previous a",});ias.extension(new IASTriggerExtension({text:'<i class="fa fa-chevron-circle-down"></i>更多',offset: 0,}));ias.extension(new IASSpinnerExtension());ias.extension(new IASNoneLeftExtension({text:'已是最后',}));ias.on('rendered',function(items){$("img").lazyload({effect: "fadeIn",failure_limit: 10});});</script>
</body>
</html>
<!-- Dynamic page generated in 1.029 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2016-05-22 10:23:03 -->

<!-- Compression = gzip -->