<div id="single-widget">
  <div class="wow fadeInUp" data-wow-delay="0.3s">
    <aside id="related_post-7" class="widget widget_related_post wow fadeInUp" data-wow-delay="0.3s">
      <h3 class="widget-title">
        <div class="s-icon"></div>
        相关文章</h3>
      <div id="related_post_widget">
        <ul>
          <li><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html">自动从其它站点RSS抓取文章</a></li>
          <li><a href="http://zmingcx.com/nix-gravatar-cache-modified.html">头像缓存插件：nix-gravatar-cache 修改版</a></li>
          <li><a href="http://zmingcx.com/wordpress-4-5-came.html">WordPress 4.5 如期而至</a></li>
          <li><a href="http://zmingcx.com/wordpress-4-5-rc1.html">WordPress 4.5正式版即将发布</a></li>
          <li><a href="http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html">解答一个WordPress文章中插入图片的困惑</a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="hot_comment-17" class="widget widget_hot_comment wow fadeInUp" data-wow-delay="0.3s">
      <h3 class="widget-title">
        <div class="s-icon"></div>
        热评文章</h3>
      <div id="hot_comment_widget">
        <ul>
          <li><span class='li-icon li-icon-1'>1</span><a href= "http://zmingcx.com/show-the-upcoming-articles.html" rel="bookmark" title=" (58条评论)" >WordPress显示即将发布的文章列表</a></li>
          <li><span class='li-icon li-icon-2'>2</span><a href= "http://zmingcx.com/nix-gravatar-cache-modified.html" rel="bookmark" title=" (37条评论)" >头像缓存插件：nix-gravatar-cache 修改版</a></li>
          <li><span class='li-icon li-icon-3'>3</span><a href= "http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html" rel="bookmark" title=" (31条评论)" >解答一个WordPress文章中插入图片的困惑</a></li>
          <li><span class='li-icon li-icon-4'>4</span><a href= "http://zmingcx.com/2-0-release-the-begin.html" rel="bookmark" title=" (28条评论)" >Begin主题 2.0 版正式发布！</a></li>
          <li><span class='li-icon li-icon-5'>5</span><a href= "http://zmingcx.com/wordpress-4-5-came.html" rel="bookmark" title=" (18条评论)" >WordPress 4.5 如期而至</a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
  </div>
  <div class="clear"></div>
</div>
