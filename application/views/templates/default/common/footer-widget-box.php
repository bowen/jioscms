<div id="footer-widget-box">
  <div class="footer-widget">
    <aside id="nav_menu-3" class="widget widget_nav_menu wow fadeInUp" data-wow-delay="0.3s">
      <div class="menu-%e9%a1%b5%e8%84%9a-container">
        <ul id="menu-%e9%a1%b5%e8%84%9a" class="menu">
          <li id="menu-item-6308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6308"><a href="http://zmingcx.com/contact.html"><i class="fa-envelope-o fa"></i><span class="font-text">联系方式</span></a></li>
          <li id="menu-item-6307" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6307"><a href="http://zmingcx.com/archives.html"><i class="fa-indent fa"></i><span class="font-text">文章归档</span></a></li>
          <li id="menu-item-6309" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6309"><a href="http://zmingcx.com/category/utility-software/"><i class="fa-wrench fa"></i><span class="font-text">实用软件</span></a></li>
          <li id="menu-item-6306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6306"><a href="http://zmingcx.com/coderender.html"><i class="fa-file-code-o fa"></i><span class="font-text">代码高亮</span></a></li>
          <li id="menu-item-6310" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6310"><a href="http://zmingcx.com/category/skill/"><i class="fa-puzzle-piece fa"></i><span class="font-text">实用技巧</span></a></li>
          <li id="menu-item-6312" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6312"><a href="http://zmingcx.com/category/photoshop/"><i class="fa-leaf fa"></i><span class="font-text">Photoshop</span></a></li>
          <li id="menu-item-6598" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6598"><a target="_blank" href="http://zmingcx.com/category/webdesign/"><i class="fa-desktop fa"></i><span class="font-text">Web前端</span></a></li>
          <li id="menu-item-6846" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6846"><a href="http://zmingcx.com/category/information/"><i class="fa-star-half-o fa"></i><span class="font-text">网文资讯</span></a></li>
          <li id="menu-item-6856" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6856"><a href="http://zmingcx.com/begin.html"><i class="fa-flask fa"></i><span class="font-text">Begin主题</span></a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="advert-6" class="widget widget_advert wow fadeInUp" data-wow-delay="0.3s">
      <h3 class="widget-title">
        <div class="s-icon"></div>
        关于本站</h3>
      <div id="advert_widget"> 分享交流WordPress经验与技巧，关注网站前端设计与网站制作，打造自己专属的WordPress主题，让你的博客与众不同！</div>
      <div class="clear"></div>
    </aside>
    <div class="clear"></div>
  </div>
</div>