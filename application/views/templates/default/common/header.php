<header id="masthead" class="site-header">
  <nav id="top-header">
    <div class="top-nav">
      <div id="user-profile"> 欢迎光临！ <span class="nav-set"> <span class="nav-login"> <a href="#login" class="flatbtn" id="login-main" >登录</a> </span> </span></div>
      <div class="menu-%e5%a4%b4%e9%83%a8-container">
        <ul id="menu-%e5%a4%b4%e9%83%a8" class="top-menu">
          <li id="menu-item-6583" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6583"><a href="http://zmingcx.com/link-user.html">主题用户</a></li>
          <li id="menu-item-6335" class="menu-item menu-item-type-taxonomy menu-item-object-gallery menu-item-6335"><a href="http://zmingcx.com/gallery/enjoy/">美图</a></li>
          <li id="menu-item-6405" class="menu-item menu-item-type-taxonomy menu-item-object-videos menu-item-6405"><a href="http://zmingcx.com/videos/music/">视频</a></li>
          <li id="menu-item-6859" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6859"><a href="http://zmingcx.com/hot-article.html">热门</a></li>
          <li id="menu-item-6287" class="menu-item menu-item-type-taxonomy menu-item-object-taobao menu-item-6287"><a href="http://zmingcx.com/taobao/taoke/">淘客</a></li>
          <li id="menu-item-6445" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6445"><a href="http://zmingcx.com/tags.html">标签</a></li>
          <li id="menu-item-6183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6183"><a href="http://zmingcx.com/links.html">链接</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div id="menu-box">
    <div id="top-menu"> <span class="nav-search"><i class="fa fa-search"></i></span> <span class="mobile-login"><a href="#login" id="login-mobile" ><i class="fa fa-sun-o"></i></a></span>
      <hgroup class="logo-site">
        <p class="site-title"> <a href="http://zmingcx.com/"><img src="http://zmingcx.com/wp-content/uploads/2015/05/logo.png" alt="知更鸟" /></a></p>
      </hgroup>
      <div id="site-nav-wrap">
        <div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close">×</a></div>
        <nav id="site-nav" class="main-nav"> <a href="#sidr-main" id="navigation-toggle" class="bars"><i class="fa fa-bars"></i></a>
          <div class="menu-%e4%b8%bb%e5%af%bc%e8%88%aa-container">
            <ul id="menu-%e4%b8%bb%e5%af%bc%e8%88%aa" class="down-menu nav-menu">
              <li id="menu-item-6181" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-6181"><a href="http://zmingcx.com/"><i class="fa-television fa"></i><span class="font-text">返回首页</span></a></li>
              <li id="menu-item-6316" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6316"><a href="http://zmingcx.com/blog.html"><i class="fa-sticky-note-o fa"></i><span class="font-text">浏览博客</span></a></li>
              <li id="menu-item-6554" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-6554"><a href="http://zmingcx.com/category/share/"><i class="fa-paper-plane-o fa"></i><span class="font-text">技术分享</span></a>
                <ul class="sub-menu">
                  <li id="menu-item-6571" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6571"><a href="http://zmingcx.com/category/share/wordpress/">WordPress</a></li>
                  <li id="menu-item-6176" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6176"><a href="http://zmingcx.com/category/share/wpplugins/">plugins</a></li>
                  <li id="menu-item-6555" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6555"><a href="http://zmingcx.com/category/share/wordpresstheme/">Themes</a></li>
                  <li id="menu-item-6177" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6177"><a href="http://zmingcx.com/category/works/hotnewspor/">HotNews主题</a></li>
                  <li id="menu-item-6179" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6179"><a href="http://zmingcx.com/category/works/ality-original/">Ality主题</a></li>
                </ul>
              </li>
              <li id="menu-item-6862" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6862"><a href="http://zmingcx.com/category/work/"><i class="fa-flask fa"></i><span class="font-text">我的作品</span></a></li>
              <li id="menu-item-6853" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6853"><a target="_blank" href="http://zmingcx.com/begin.html"><i class="fa-home fa"></i><span class="font-text">begin主题购买</span></a></li>
              <li id="menu-item-7038" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7038"><a href="http://zmingcx.com/all-columns.html"><i class="fa-sitemap fa"></i><span class="font-text">全部栏目</span></a></li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</header>