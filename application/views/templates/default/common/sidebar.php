<div id="sidebar" class="widget-area">
  <div class="wow animated" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s;">
    <aside id="hot_commend-25" class="widget widget_hot_commend wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>本站推荐</h3>
      <div id="hot" class="hot_commend">
        <ul>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/show-the-upcoming-articles.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww2.sinaimg.cn/large/703be3b1jw1f1qg59wjhvj20m609q766.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww2.sinaimg.cn/large/703be3b1jw1f1qg59wjhvj20m609q766.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="WordPress显示即将发布的文章列表" style="display: block;"></a></div>
            </figure>
            <div class="hot-title"><a href="http://zmingcx.com/show-the-upcoming-articles.html" rel="bookmark">WordPress显示即将发布的文章列表</a></div>
            <span class="views"><i class="fa fa-eye"></i> 3,277</span> <i class="fa fa-thumbs-o-up">&nbsp;46</i></li>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/phpstudy.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://zmingcx.com/wp-content/uploads/2016/02/study1.png&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://zmingcx.com/wp-content/uploads/2016/02/study1.png&amp;w=280&amp;h=210&amp;zc=1" alt="phpStudy 2016 （支持PHP7）使用教程" style="display: block;"></a></div>
            </figure>
            <div class="hot-title"><a href="http://zmingcx.com/phpstudy.html" rel="bookmark">phpStudy 2016 （支持PHP7）使用教程</a></div>
            <span class="views"><i class="fa fa-eye"></i> 4,366</span> <i class="fa fa-thumbs-o-up">&nbsp;30</i></li>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/wordpress-theme-begin.html"><img src="http://zmingcx.com/wp-content/uploads/2015/02/begin11.jpg" data-original="http://zmingcx.com/wp-content/uploads/2015/02/begin11.jpg" alt="Wordpress主题: Begin" style="display: block;"></a></div>
            </figure>
            <div class="hot-title"><a href="http://zmingcx.com/wordpress-theme-begin.html" rel="bookmark">WordPress主题: Begin</a></div>
            <span class="views"><i class="fa fa-eye"></i> 31,648</span> <i class="fa fa-thumbs-o-up">&nbsp;185</i></li>
          <li style="border: none;">
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/wordpress-theme-ality.html"><img src="http://zmingcx.com/wp-content/uploads/2014/07/ality15.jpg" data-original="http://zmingcx.com/wp-content/uploads/2014/07/ality15.jpg" alt="Wordpress 主题：Ality" style="display: block;"></a></div>
            </figure>
            <div class="hot-title"><a href="http://zmingcx.com/wordpress-theme-ality.html" rel="bookmark">WordPress 主题：Ality</a></div>
            <span class="views"><i class="fa fa-eye"></i> 162,055</span> <i class="fa fa-thumbs-o-up">&nbsp;668</i></li>
          <div class="clear"></div>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="feed-12" class="widget widget_feed wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>关注我们</h3>
      <div id="feed_widget"> <span class="feed-rss">
        <ul>
          <li class="weixin"> <span class="tipso_style" id="tip-w-j" data-tipso="<div id=&quot;weixin-qr&quot;><img src=&quot;http://zmingcx.com/wp-content/uploads/2016/04/weixin.jpg&quot;/></div>"><a title="微信"><i class="fa fa-weixin"></i></a></span></li>
          <li class="feed"><a title="订阅" href="http://zmingcx.com/feed/" target="_blank"><i class="fa fa-rss"></i></a></li>
          <li class="tsina"><a title="" href="http://weibo.com/u/1882973105" target="_blank"><i class="fa fa-weibo"></i></a></li>
          <li class="tqq"><a title="" href="http://weibo.com/u/1882973105" target="_blank"><i class="fa fa-tencent-weibo"></i></a></li>
        </ul>
        </span></div>
      <div class="clear"></div>
    </aside>
    <aside id="new_cat-12" class="widget widget_new_cat wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>最新文章</h3>
      <div class="new_cat">
        <ul>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/sony-xperia-x.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww2.sinaimg.cn/large/703be3b1jw1f41i1fth6yj20zk0qok3j.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww2.sinaimg.cn/large/703be3b1jw1f41i1fth6yj20zk0qok3j.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="索尼 Xperia X" style="display: block;"></a></div>
            </figure>
            <div class="new-title"><a href="http://zmingcx.com/sony-xperia-x.html" rel="bookmark">索尼 Xperia X</a></div>
            <div class="date">05/20</div>
            <span class="views"><i class="fa fa-eye"></i> 44</span></li>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/2-0-release-the-begin.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://zmingcx.com/wp-content/uploads/2015/02/Beginz.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://zmingcx.com/wp-content/uploads/2015/02/Beginz.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="Begin主题 2.0 版正式发布！" style="display: block;"></a></div>
            </figure>
            <div class="new-title"><a href="http://zmingcx.com/2-0-release-the-begin.html" rel="bookmark">Begin主题 2.0 版正式发布！</a></div>
            <div class="date">05/15</div>
            <span class="views"><i class="fa fa-eye"></i> 1,420</span></li>
          <li>
            <figure class="thumbnail">
              <div class="load"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww4.sinaimg.cn/large/703be3b1jw1f3qh39r9qhj20ju05ut9g.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww4.sinaimg.cn/large/703be3b1jw1f3qh39r9qhj20ju05ut9g.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="自动从其它站点RSS抓取文章" style="display: block;"></a></div>
            </figure>
            <div class="new-title"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html" rel="bookmark">自动从其它站点RSS抓取文章</a></div>
            <div class="date">05/15</div>
            <span class="views"><i class="fa fa-eye"></i> 434</span></li>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="video_widget-2" class="widget widget_video_widget wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>最新视频</h3>
      <div id="video" class="video_widget">
        <ul>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="load"><a class="videor" href="http://zmingcx.com/video/the-cranberries%ef%bc%9a-zombie.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f3ukim12s7j20xx0wftq5.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f3ukim12s7j20xx0wftq5.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="The Cranberries： Zombie" style="display: block;"><i class="fa fa-play-circle-o"></i></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="load"><a class="videor" href="http://zmingcx.com/video/7097.html"><img src="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://www.zhuayoukong.com/wp-content/uploads/tech/2016-03-09/9b61df94ca7b8aa7f40537dc46f67947.jpg&amp;w=280&amp;h=210&amp;zc=1" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://www.zhuayoukong.com/wp-content/uploads/tech/2016-03-09/9b61df94ca7b8aa7f40537dc46f67947.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="iPhone 7概念" style="display: block;"><i class="fa fa-play-circle-o"></i></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="load"><a class="videor" href="http://zmingcx.com/video/ipad-pro.html"><img src="http://ww1.sinaimg.cn/mw690/703be3b1jw1ezuvcl2tn4j207s05ujro.jpg" data-original="http://ww1.sinaimg.cn/mw690/703be3b1jw1ezuvcl2tn4j207s05ujro.jpg" alt="iPad Pro" style="display: block;"><i class="fa fa-play-circle-o"></i></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="load"><a class="videor" href="http://zmingcx.com/video/surface-pro-4.html"><img src="http://ww3.sinaimg.cn/large/703be3b1jw1ezuuxpqv58j207s05udg2.jpg" data-original="http://ww3.sinaimg.cn/large/703be3b1jw1ezuuxpqv58j207s05udg2.jpg" alt="Surface Pro 4" style="display: block;"><i class="fa fa-play-circle-o"></i></a></div>
              </figure>
            </div>
          </div>
          <div class="clear"></div>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
  </div>
  <div class="sidebar-roll">
    <aside id="php_text-3" class="widget widget_php_text wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <div class="textwidget widget-text"><a href="http://zmingcx.com/begin.html" target="_blank"><img src="http://zmingcx.com/wp-content/uploads/2016/04/gif28.gif" alt="购买begin主题"></a></div>
      <div class="clear"></div>
    </aside>
  </div>
  <div class="wow animated" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
    <aside id="categories-18" class="widget widget_categories wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>分类目录</h3>
      <ul>
        <li class="cat-item cat-item-725"><a href="http://zmingcx.com/category/works/ality-original/">Ality主题</a></li>
        <li class="cat-item cat-item-11"><a href="http://zmingcx.com/category/works/hotnewspor/" title="HotNews主题相关的文章">HotNews主题</a></li>
        <li class="cat-item cat-item-3"><a href="http://zmingcx.com/category/photoshop/" title="photoshop">Photoshop</a></li>
        <li class="cat-item cat-item-763"><a href="http://zmingcx.com/category/share/wpplugins/" title="推荐功能不错的WP插件">plugins</a></li>
        <li class="cat-item cat-item-764"><a href="http://zmingcx.com/category/share/wordpresstheme/" title="Wordpress主题">Themes</a></li>
        <li class="cat-item cat-item-5"><a href="http://zmingcx.com/category/webdesign/" title="分享Web前端设计理念及技术">Web前端</a></li>
        <li class="cat-item cat-item-765"><a href="http://zmingcx.com/category/share/wordpress/" title="记录分享WP使用经验和技巧">WordPress</a></li>
        <li class="cat-item cat-item-10"><a href="http://zmingcx.com/category/works/">原创主题</a></li>
        <li class="cat-item cat-item-277"><a href="http://zmingcx.com/category/skill/">实用技巧</a></li>
        <li class="cat-item cat-item-9"><a href="http://zmingcx.com/category/utility-software/" title="推荐一些实用工具软件">实用软件</a></li>
        <li class="cat-item cat-item-786"><a href="http://zmingcx.com/category/work/">我的作品</a></li>
        <li class="cat-item cat-item-767"><a href="http://zmingcx.com/category/digital/">数码硬件</a></li>
        <li class="cat-item cat-item-766"><a href="http://zmingcx.com/category/information/" title="转载一些有价值的网文资讯">网文资讯</a></li>
        <li class="cat-item cat-item-4"><a href="http://zmingcx.com/category/uidesign/" title="搜刮来的设计资源">设计资源</a></li>
        <li class="cat-item cat-item-1"><a href="http://zmingcx.com/category/gossip/" title="记录一些闲言琐碎的东西">闲言碎语</a></li>
      </ul>
      <div class="clear"></div>
    </aside>
    <aside id="cx_tag_cloud-9" class="widget widget_cx_tag_cloud wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>热门标签</h3>
      <div id="tag_cloud_widget"> <a href="http://zmingcx.com/tag/css/" class="tag-link-29 tag-link-position-1" title="19个话题" style="font-size: 14px; left: 73.1672px; top: 192.157px; z-index: 146; opacity: 1.36074;">CSS</a> <a href="http://zmingcx.com/tag/wpplugins/" class="tag-link-8 tag-link-position-2" title="58个话题" style="font-size: 14px; left: 86.0712px; top: 8.11037px; z-index: 102; opacity: 0.114166; display: none;">plugins</a> <a href="http://zmingcx.com/tag/%e9%97%b2%e8%a8%80%e7%a2%8e%e8%af%ad/" class="tag-link-258 tag-link-position-3" title="45个话题" style="font-size: 14px; left: 149.704px; top: 185.524px; z-index: 108; opacity: 0.193473;">闲言碎语</a> <a href="http://zmingcx.com/tag/information/" class="tag-link-12 tag-link-position-4" title="150个话题" style="font-size: 14px; left: 140.508px; top: 148.958px; z-index: 174; opacity: 4.23627;">网文资讯</a> <a href="http://zmingcx.com/tag/wordpress-course/" class="tag-link-128 tag-link-position-5" title="102个话题" style="font-size: 14px; left: 46.9759px; top: 129.581px; z-index: 197; opacity: 10.5488;">Wordpress教程</a> <a href="http://zmingcx.com/tag/wordpress/" class="tag-link-6 tag-link-position-6" title="378个话题" style="font-size: 14px; left: -1.6792px; top: 136.018px; z-index: 153; opacity: 1.76496;">WordPress</a> <a href="http://zmingcx.com/tag/utility-software/" class="tag-link-778 tag-link-position-7" title="99个话题" style="font-size: 14px; left: 1.56067px; top: 56.0541px; z-index: 102; opacity: 0.111327; display: none;">实用软件</a> <a href="http://zmingcx.com/tag/wordpress%e4%b8%bb%e9%a2%98/" class="tag-link-347 tag-link-position-8" title="99个话题" style="font-size: 14px; left: 34.8271px; top: 12.6781px; z-index: 102; opacity: 0.117261; display: none;">WordPress主题</a> <a href="http://zmingcx.com/tag/skill/" class="tag-link-779 tag-link-position-9" title="88个话题" style="font-size: 14px; left: 133.748px; top: 19.6142px; z-index: 102; opacity: 0.114464; display: none;">实用技巧</a> <a href="http://zmingcx.com/tag/%e5%93%8d%e5%ba%94%e5%bc%8f/" class="tag-link-617 tag-link-position-10" title="33个话题" style="font-size: 14px; left: 189.95px; top: 79.8403px; z-index: 101; opacity: 0.105589; display: none;">响应式</a> <a href="http://zmingcx.com/tag/hotnews-pro/" class="tag-link-265 tag-link-position-11" title="78个话题" style="font-size: 14px; left: 157.582px; top: 85.5765px; z-index: 144; opacity: 1.25563;">HotNews Pro</a> <a href="http://zmingcx.com/tag/webdesign/" class="tag-link-777 tag-link-position-12" title="146个话题" style="font-size: 14px; left: 111.116px; top: 59.3369px; z-index: 184; opacity: 6.25293;">Web前端</a> <a href="http://zmingcx.com/tag/web%e8%ae%be%e8%ae%a1/" class="tag-link-109 tag-link-position-13" title="20个话题" style="font-size: 14px; left: 35.1121px; top: 54.6739px; z-index: 168; opacity: 3.34186;">Web设计</a> <a href="http://zmingcx.com/tag/photoshop/" class="tag-link-775 tag-link-position-14" title="33个话题" style="font-size: 14px; left: -13.4529px; top: 68.8963px; z-index: 104; opacity: 0.143284;">Photoshop</a> <a href="http://zmingcx.com/tag/%e5%8e%9f%e5%88%9b%e4%b8%bb%e9%a2%98/" class="tag-link-341 tag-link-position-15" title="47个话题" style="font-size: 14px; left: 17.1973px; top: 36.4122px; z-index: 102; opacity: 0.115374; display: none;">原创主题</a> <a href="http://zmingcx.com/tag/hotnews%e4%b8%bb%e9%a2%98/" class="tag-link-453 tag-link-position-16" title="46个话题" style="font-size: 14px; left: 71.5885px; top: 8.03725px; z-index: 103; opacity: 0.121937; display: none;">HotNews主题</a> <a href="http://zmingcx.com/tag/digital/" class="tag-link-658 tag-link-position-17" title="20个话题" style="font-size: 14px; left: 151.418px; top: 31.5201px; z-index: 102; opacity: 0.108257; display: none;">数码硬件</a> <a href="http://zmingcx.com/tag/jquery/" class="tag-link-56 tag-link-position-18" title="50个话题" style="font-size: 14px; left: 148.239px; top: 25.4763px; z-index: 106; opacity: 0.15958;">JQuery</a> <a href="http://zmingcx.com/tag/uidesign/" class="tag-link-776 tag-link-position-19" title="42个话题" style="font-size: 14px; left: 70.6998px; top: 10.6099px; z-index: 116; opacity: 0.330607;">设计资源</a> <a href="http://zmingcx.com/tag/firefox/" class="tag-link-36 tag-link-position-20" title="28个话题" style="font-size: 14px; left: 67.1667px; top: 11.2721px; z-index: 102; opacity: 0.112118; display: none;">Firefox</a>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="img_cat-5" class="widget widget_img_cat wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>分类图片</h3>
      <div class="img_cat">
        <ul>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="img-title"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html" rel="bookmark">自动从其它站点RSS抓取文章</a></div>
                <div class="load"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww4.sinaimg.cn/large/703be3b1jw1f3qh39r9qhj20ju05ut9g.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="自动从其它站点RSS抓取文章"></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="img-title"><a href="http://zmingcx.com/wordpress-4-5-came.html" rel="bookmark">WordPress 4.5 如期而至</a></div>
                <div class="load"><a href="http://zmingcx.com/wordpress-4-5-came.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/mw690/703be3b1jw1f2rfzcqeezj20ju05u3z3.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="WordPress 4.5 如期而至"></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="img-title"><a href="http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html" rel="bookmark">解答一个WordPress文章中插入图片的困惑</a></div>
                <div class="load"><a href="http://zmingcx.com/answer-a-wordpress-insert-image-confusion.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww2.sinaimg.cn/large/703be3b1jw1f2eu204dtuj20ju05u0td.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="解答一个WordPress文章中插入图片的困惑"></a></div>
              </figure>
            </div>
          </div>
          <div class="img-box">
            <div class="img-x2">
              <figure class="insets">
                <div class="img-title"><a href="http://zmingcx.com/wordpress-current-category-all-tags.html" rel="bookmark">获取WordPress当前分类文章所有标签</a></div>
                <div class="load"><a href="http://zmingcx.com/wordpress-current-category-all-tags.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f2emsjwpttj20ju05ugnd.jpg&amp;w=280&amp;h=210&amp;zc=1" alt="获取WordPress当前分类文章所有标签"></a></div>
              </figure>
            </div>
          </div>
          <div class="clear"></div>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
    <aside id="recent_comments-28" class="widget widget_recent_comments wow animated" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
      <h3 class="widget-title"><i class="fa fa-bars"></i>近期评论</h3>
      <div id="message" class="message-widget">
        <ul>
          <li> <a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html#comment-57173" title="自动从其它站点RSS抓取文章"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/7a807a4691c26b1bafd0e5ed9578215e-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>叮咚博客</strong></span> 这个可以有哦！ </a></li>
          <li> <a href="http://zmingcx.com/55-the-latest-jquery-slideshow.html#comment-57172" title="55个最新jQuery幻灯"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/92a949e7d7e91fb12a66d6efed5c7d66-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>无聊赚</strong></span> 很全 </a></li>
          <li> <a href="http://zmingcx.com/2-0-release-the-begin.html#comment-57171" title="Begin主题 2.0 版正式发布！"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/1d7400c9cdcd8deb75e6fdbb7c776224-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>Jane博客</strong></span> 喜欢新版幻灯片样式 </a></li>
          <li> <a href="http://zmingcx.com/2-0-release-the-begin.html#comment-57170" title="Begin主题 2.0 版正式发布！"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/c0961ee88aca598301b92a5517402d44-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>电影天堂迅雷下载</strong></span> 超级酷 已经开始使用了 </a></li>
          <li> <a href="http://zmingcx.com/let-your-wordpress-link-opens-in-a-new-window.html#comment-57169" title="让你的wordpress在新窗口打开链接"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/f6177be61c46612f46ee631a2045b286-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>夏天烤洋芋</strong></span> 还是最后的， 这个方法省事。 </a></li>
          <li> <a href="http://zmingcx.com/bulletin/begin-2-0-upgrade-is-complete.html#comment-57168" title="Begin 2.0升级完成，正式启用！"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/ac1895ec9b91395e450b280b1fbff2a2-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>惠州百科</strong></span> 那么多人在排队，我还是晚点用好了 <img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_mrgreen.gif" alt=":mrgreen:" class="wp-smiley" style="height: 1em; max-height: 1em;"> </a></li>
          <li> <a href="http://zmingcx.com/sony-xperia-x.html#comment-57167" title="索尼 Xperia X"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/95b6a2b91066f7783eb78cc0210d2309-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>boke112导航</strong></span> 其实日本的手机还是挺不错的，就是太贵了，还是用国产的省钱 </a></li>
          <li style="border: none;"> <a href="http://zmingcx.com/video/tydisarah-howellswhen-i-go.html#comment-57166" title="TyDi,Sarah Howells：When I Go"> <img alt="" src="http://zmingcx.com/wp-content/uploads/gravatar/c0961ee88aca598301b92a5517402d44-s64.jpg" class="avatar avatar-64" width="64" height="64"> <span class="comment_author"><strong>电影天堂迅雷下载</strong></span> 音乐很好听 </a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </aside>
  </div>
</div>
