<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Cache-Control" content="no-transform" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link type="text/css" media="all" href="<?php echo base_url();?>out/css/autoptimize.css" rel="stylesheet" />
<title><?php echo isset($title)?$title:'jioscms';?></title>
<meta name="description" content="&nbsp;" />
<meta name="keywords" content="WordPress,WordPress主题,原创主题" />
<link rel="shortcut icon" href="http://zmingcx.com/wp-content/uploads/2015/05/favicon.ico">
<link rel="apple-touch-icon" sizes="114x114" href="http://zmingcx.com/wp-content/uploads/2015/05/favicon.png" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://zmingcx.com/xmlrpc.php">
<!--[if lt IE 9]> <script src="<?php echo base_url();?>out/js/html5.js"></script> <script src="<?php echo base_url();?>out/js/css3-mediaqueries.js"></script> <![endif]-->
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.min.js?ver=1.10.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/main.js?ver=4.5.2'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/slides.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.qrcode.min.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/wow.js?ver=0.1.9'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery-ias.js?ver=2.2.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/jquery.lazyload.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/tipso.js?ver=1.0.1'></script>
<script type='text/javascript'>/*  */var wpl_ajax_url = "http:\/\/zmingcx.com\/wp-admin\/admin-ajax.php";/*  */</script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/script.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/flexisel.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/fancybox.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/comments-ajax-qt.js?ver=2016.05.16'></script>
<link rel="canonical" href="http://zmingcx.com/2-0-release-the-begin.html" />
</head>
<body class="single single-post postid-7146 single-format-standard">
<div id="page" class="hfeed site">
<?php include 'common/header.php';?>
<div id="search-main">
  <div id="searchbar">
    <form method="get" id="searchform" action="http://zmingcx.com/">
      <input type="text" value="" name="s" id="s" placeholder="输入搜索内容" required />
      <button type="submit" id="searchsubmit">搜索</button>
    </form>
  </div>
  <div id="searchbar">
    <form method="get" id="searchform" action="http://zmingcx.com/baidu.html" target="_blank">
      <input type="hidden" value="1" name="entry">
      <input class="swap_value" placeholder="输入百度站内搜索关键词" name="q">
      <button type="submit" id="searchsubmit">百度</button>
    </form>
  </div>
  <div class="clear"></div>
</div>
<?php include 'common/breadcrumb.php';?>
<div id="content" class="site-content">
  <div class="clear"></div>
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <article id="post-7146" class="post-7146 post type-post status-publish format-standard hentry category-works tag-wordpress tag-341">
          <header class="entry-header">
            <h1 class="entry-title"><?php echo $post->title;?></h1>
          </header>
          <div class="entry-content">
            <div class="single-content">
            	<?php echo $post->content;?>
            </div>
            <div class="clear"></div>
            <div id="social">
              <div class="social-main"> <span class="like"> <a href="javascript:;" data-action="ding" data-id="7146" title="我赞" class="favorite"><i class="fa fa-thumbs-up"></i>赞 <i class="count"> 49</i> </a> </span> <span class="shang-p"> <span class="tipso_style" id="tip-p" data-tipso='<div id="shang"><div class="shang-main"><h4><i class="fa fa-heart" aria-hidden="true"></i> 如果认为本文对您有所帮助请赞助本站</h4><div class="shang-img"> <img src="http://zmingcx.com/wp-content/uploads/2015/11/alipay.png" /><h4>支付宝扫一扫赞助</h4></div><div class="shang-img"> <img src="http://zmingcx.com/wp-content/uploads/2015/11/alipay.png" /><h4>微信钱包扫描赞助</h4></div><div class="clear"></div></div></div>'> <span class="shang-s"><a title="赞助本站">赏</a></span></span> </span> <span class="share-sd"> <span class="share-s"><a href="javascript:void(0)" id="share-s" title="分享"><i class="fa fa-share-alt"></i>分享</a></span>
                <div id="share">
                  <ul class="bdsharebuttonbox">
                    <li><a title="更多" class="bds_more fa fa-plus-square" data-cmd="more"></a></li>
                    <li><a title="分享到QQ空间" class="fa fa-qq" data-cmd="qzone" href="#"></a></li>
                    <li><a title="分享到新浪微博" class="fa fa-weibo" data-cmd="tsina"></a></li>
                    <li><a title="分享到腾讯微博" class="fa fa-pinterest-square" data-cmd="tqq"></a></li>
                    <li><a title="分享到人人网" class="fa fa-renren" data-cmd="renren"></a></li>
                    <li><a title="分享到微信" class="fa fa-weixin" data-cmd="weixin"></a></li>
                  </ul>
                </div>
                </span>
                <div class="clear"></div>
              </div>
            </div>
            <footer class="single-footer">
              <ul class="single-meta">
                <li class="comment"><a href="http://zmingcx.com/2-0-release-the-begin.html#comments"><i class="fa fa-comment-o"></i> 28</a></li>
                <li class="views"><i class="fa fa-eye"></i> 1,420</li>
                <li class="r-hide"><a href="javascript:pr()" title="侧边栏"><i class="fa fa-caret-left"></i> <i class="fa fa-caret-right"></i></a></li>
              </ul>
              <ul id="fontsize">
                A+
              </ul>
              <div class="single-cat-tag">
                <div class="single-cat">发布日期：2016年05月15日&nbsp;&nbsp;所属分类：<a href="http://zmingcx.com/category/works/" rel="category tag">原创主题</a></div>
                <div class="single-tag">标签：<a href="http://zmingcx.com/tag/wordpress/" rel="tag">WordPress</a><a href="http://zmingcx.com/tag/wordpress%e4%b8%bb%e9%a2%98/" rel="tag">WordPress主题</a><a href="http://zmingcx.com/tag/%e5%8e%9f%e5%88%9b%e4%b8%bb%e9%a2%98/" rel="tag">原创主题</a></div>
              </div>
            </footer>
            <div class="clear"></div>
          </div>
        </article>
      </div>
      <?php if(false):?>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div class="authorbio"> <img alt="" src='http://zmingcx.com/wp-content/uploads/gravatar/1498899a405ed8ddf3d8c0a4bf6aa0bb-s64.jpg' class="avatar avatar-64" width="64" height="64" />
          <ul class="spostinfo">
            <li><strong>版权声明：</strong>本站原创文章，于7天前，由<a href="http://zmingcx.com/author/robin/" title="由知更鸟发布" rel="author">知更鸟</a>发表，共 1949字。</li>
            <li><strong>转载请注明：</strong><a href="http://zmingcx.com/2-0-release-the-begin.html" rel="bookmark" title="本文固定链接 http://zmingcx.com/2-0-release-the-begin.html">Begin主题 2.0 版正式发布！ | 知更鸟</a><a href="#" onclick="copy_code('http://zmingcx.com/2-0-release-the-begin.html'); return false;"> +复制链接</a></li>
          </ul>
        </div>
      </div>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div id="related-img">
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww4.sinaimg.cn/large/703be3b1jw1f3qh39r9qhj20ju05ut9g.jpg&w=280&h=210&zc=1" alt="自动从其它站点RSS抓取文章" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html">自动从其它站点RSS抓取文章</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/nix-gravatar-cache-modified.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f2uvqv8swij20j605n3zk.jpg&w=280&h=210&zc=1" alt="头像缓存插件：nix-gravatar-cache 修改版" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/nix-gravatar-cache-modified.html">头像缓存插件：nix-gravatar-cache 修改版</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/wordpress-4-5-came.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/mw690/703be3b1jw1f2rfzcqeezj20ju05u3z3.jpg&w=280&h=210&zc=1" alt="WordPress 4.5 如期而至" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/wordpress-4-5-came.html">WordPress 4.5 如期而至</a></div>
            </div>
          </div>
          <div class="r4">
            <div class="related-site">
              <figure class="related-site-img">
                <div class="load"><a href="http://zmingcx.com/wordpress-4-5-rc1.html"><img src="http://zmingcx.com/wp-content/themes/begin/img/loading.png" data-original="http://zmingcx.com/wp-content/themes/begin/timthumb.php?src=http://ww1.sinaimg.cn/large/703be3b1jw1f2oyx6sc5gj20ju05u3za.jpg&w=280&h=210&zc=1" alt="WordPress 4.5正式版即将发布" /></a></div>
              </figure>
              <div class="related-title"><a href="http://zmingcx.com/wordpress-4-5-rc1.html">WordPress 4.5正式版即将发布</a></div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <?php endif;?>
      <?php if(false):?>
      <?php include 'common/single-widget.php';?>
      <?php endif;?>
    <?php if(false):?>
      <div class="wow fadeInUp" data-wow-delay="0.3s">
        <div class="ad-pc ad-site"><a href="http://zmingcx.com/begin.html" target="_blank"><img src="http://zmingcx.com/wp-content/uploads/2015/02/beginad.jpg" alt="Begin主题购买" /></a></div>
      </div>
      <nav class="nav-single wow fadeInUp" data-wow-delay="0.3s"> <a href="http://zmingcx.com/picture-phone.html" rel="prev"><span class="meta-nav"><span class="post-nav"><i class="fa fa-angle-left"></i> 上一篇</span><br/>
        图像日志自动截图测试：手机</span></a><span class='meta-nav'><span class='post-nav'>没有了<br/>
        </span>已是最新文章</span>
        <div class="clear"></div>
      </nav>
    <?php endif;?>
    <?php if(false):?>
      <nav class="navigation post-navigation" role="navigation">
        <h2 class="screen-reader-text">文章导航</h2>
        <div class="nav-links">
          <div class="nav-previous"><a href="http://zmingcx.com/rss-fetching-articles-from-other-site.html" rel="prev"><span class="meta-nav-r" aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></div>
          <div class="nav-next"><a href="http://zmingcx.com/sony-xperia-x.html" rel="next"><span class="meta-nav-l" aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></div>
        </div>
      </nav>
      <?php endif;?>
      <div class="scroll-comments"></div>
      <div id="comments" class="comments-area">
        <div id="respond" class="comment-respond wow fadeInUp" data-wow-delay="0.3s">
          <h3 id="reply-title" class="comment-reply-title"><i class="fa fa-pencil-square-o"></i>发表评论<small><a rel="nofollow" id="cancel-comment-reply-link" href="/2-0-release-the-begin.html#respond" style="display:none;">取消回复</a></small></h3>
          <form action="http://zmingcx.com/wp-comments-post.php" method="post" id="commentform">
            <div id="comment-author-info">
              <p class="comment-form-author">
                <input type="text" name="author" id="author" class="commenttext" value="" tabindex="1" />
                <label for="author">昵称（必填）</label>
              </p>
              <p class="comment-form-email">
                <input type="text" name="email" id="email" class="commenttext" value="" tabindex="2" />
                <label for="email">邮箱（必填）</label>
              </p>
              <p class="comment-form-url">
                <input type="text" name="url" id="url" class="commenttext" value="" tabindex="3" />
                <label for="url">网址</label>
              </p>
            </div>
            <p class="emoji-box"><script type="text/javascript">/*  */
    function grin(tag) {
    	var myField;
    	tag = ' ' + tag + ' ';
        if (document.getElementById('comment') && document.getElementById('comment').type == 'textarea') {
    		myField = document.getElementById('comment');
    	} else {
    		return false;
    	}
    	if (document.selection) {
    		myField.focus();
    		sel = document.selection.createRange();
    		sel.text = tag;
    		myField.focus();
    	}
    	else if (myField.selectionStart || myField.selectionStart == '0') {
    		var startPos = myField.selectionStart;
    		var endPos = myField.selectionEnd;
    		var cursorPos = endPos;
    		myField.value = myField.value.substring(0, startPos)
    					  + tag
    					  + myField.value.substring(endPos, myField.value.length);
    		cursorPos += tag.length;
    		myField.focus();
    		myField.selectionStart = cursorPos;
    		myField.selectionEnd = cursorPos;
    	}
    	else {
    		myField.value += tag;
    		myField.focus();
    	}
    }
/*  */</script> <a href="javascript:grin(':?:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_question.gif" alt=":?:" title="疑问" /></a> <a href="javascript:grin(':razz:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_razz.gif" alt=":razz:" title="调皮" /></a> <a href="javascript:grin(':sad:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_sad.gif" alt=":sad:" title="难过" /></a> <a href="javascript:grin(':evil:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_evil.gif" alt=":evil:" title="抠鼻" /></a> <a href="javascript:grin(':!:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_exclaim.gif" alt=":!:" title="吓" /></a> <a href="javascript:grin(':smile:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_smile.gif" alt=":smile:" title="微笑" /></a> <a href="javascript:grin(':oops:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_redface.gif" alt=":oops:" title="憨笑" /></a> <a href="javascript:grin(':grin:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_biggrin.gif" alt=":grin:" title="坏笑" /></a> <a href="javascript:grin(':eek:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_surprised.gif" alt=":eek:" title="惊讶" /></a> <a href="javascript:grin(':shock:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_eek.gif" alt=":shock:" title="发呆" /></a> <a href="javascript:grin(':???:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_confused.gif" alt=":???:" title="撇嘴" /></a> <a href="javascript:grin(':cool:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_cool.gif" alt=":cool:" title="大兵" /></a> <a href="javascript:grin(':lol:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_lol.gif" alt=":lol:" title="偷笑" /></a> <a href="javascript:grin(':mad:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_mad.gif" alt=":mad:" title="咒骂" /></a> <a href="javascript:grin(':twisted:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_twisted.gif" alt=":twisted:" title="发怒" /></a> <a href="javascript:grin(':roll:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_rolleyes.gif" alt=":roll:" title="白眼" /></a> <a href="javascript:grin(':wink:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_wink.gif" alt=":wink:" title="鼓掌" /></a> <a href="javascript:grin(':idea:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_idea.gif" alt=":idea:" title="酷" /></a> <a href="javascript:grin(':arrow:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_arrow.gif" alt=":arrow:" title="擦汗" /></a> <a href="javascript:grin(':neutral:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_neutral.gif" alt=":neutral:" title="亲亲" /></a> <a href="javascript:grin(':cry:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_cry.gif" alt=":cry:" title="大哭" /></a> <a href="javascript:grin(':mrgreen:')"><img src="http://zmingcx.com/wp-content/themes/begin/img/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="呲牙" /></a> <br />
            </p>
            <div class="clear"></div>
            <div class="qaptcha"></div>
            <p class="comment-form-comment">
              <textarea id="comment" name="comment" rows="4" tabindex="4"></textarea>
            </p>
            <p class="comment-tool"> <a class="tool-img" href='javascript:embedImage();' title="插入图片"><i class="icon-img"></i><i class="fa fa-picture-o"></i></a> <a class="emoji" href="" title="插入表情"><i class="fa fa-meh-o"></i></a></p>
            <p class="form-submit">
              <input id="submit" name="submit" type="submit" tabindex="5" value="提交评论"/>
              <input type='hidden' name='comment_post_ID' value='7146' id='comment_post_ID' />
              <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
            </p>
          </form>
        </div>
        <h2 class="comments-title wow fadeInUp" data-wow-delay="0.3s"> 目前评论：28 &nbsp;&nbsp;其中：访客&nbsp;&nbsp;28 &nbsp;&nbsp;博主&nbsp;&nbsp;0</h2>
        <ol class="comment-list">
        </ol>
      </div>
    </main>
  </div>
  <?php if(false):?>
  <?php include 'common/sidebar.php';?>
  <?php endif;?>
  <div class="clear"></div>
  <div class="clear"></div>
</div>
<?php include 'common/footer-widget-box.php';?>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="site-info wow fadeInUp" data-wow-delay="0.6s"> Copyright ©  知更鸟  版权所有. <a href="http://www.miitbeian.gov.cn" target="_blank">辽ICP备12016417号-1</a> <span class="add-info"> <a href="http://idc.wopus.org/host/yun/" target="_blank"><img class="" title="采用wopus云主机" src="http://zmingcx.com/wp-content/uploads/2015/04/idc.png" alt="" /></a> </span></div>
</footer>
<div id="login">
  <div class="login-t">用户登录</div>
  <form action="http://zmingcx.com/wp-login.php?redirect_to=http%3A%2F%2Fzmingcx.com%2F2-0-release-the-begin.html" method="post" id="loginform">
    <input type="username" type="text"  name="log" id="log" placeholder="名称" required/>
    <input type="password" name="pwd" id="pwd" placeholder="密码" required/>
    <input type="submit" id="submit" value="登录">
    <input type="hidden" name="redirect_to" value="/2-0-release-the-begin.html" />
    <label>
      <input type="checkbox" name="rememberme" id="modlogn_remember" value="yes"  checked="checked" alt="Remember Me" >
      自动登录</label>
    <a href="http://zmingcx.com/wp-login.php?action=lostpassword">&nbsp;&nbsp;忘记密码？</a>
  </form>
  <div class="login-b"></div>
</div>
<?php include 'common/scroll.php'?>
<script type="text/javascript" src="<?php echo base_url();?>out/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>out/js/qaptcha.jquery.js"></script>
<script type="text/javascript">$(document).ready(function(){$('.qaptcha').QapTcha();});</script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/postviews-cache.js?ver=1.68'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/superfish.js?ver=2016.05.16'></script>
<script type='text/javascript' src='<?php echo base_url();?>out/js/3dtag.js?ver=4.5.2'></script>
</body>
</html>
<!-- Dynamic page generated in 1.029 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2016-05-22 10:23:03 -->

<!-- Compression = gzip -->