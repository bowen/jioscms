
<title><?php echo $title;?></title>

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		Nestable lists
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Drag &amp; drop hierarchical list
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-sm-6">
				<div class="dd" id="nestable-menus">
					<?php echo $html;?>
				</div>
				<textarea id="nestable-output" cols="80" rows="5"></textarea>
				<button id="btn-save">Save</button>
			</div>
			<div class="vspace-16-sm"></div>
			
			<div class="col-sm-6">
			</div>
			
		</div>


		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- page specific plugin scripts -->
<script type="text/javascript">
	var scripts = [null,"<?php echo base_url();?>assets/js/jquery.nestable.js", null]
	$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
	  //inline scripts related to this page
		 jQuery(function($){
			var updateOutput = function(e)
		    {
		        var list   = e.length ? e : $(e.target),
		            output = list.data('output');
		        if (window.JSON) {
		            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		        } else {
		            output.val('JSON browser support required for this demo.');
		        }

		    };

		$("#btn-save").click(function(){
			$.post("/b/doSaveType",{"content":$("#nestable-output").val()},function(res){
				console.log(res);
			},"json");
		});			 
	
		$('#nestable-menus').nestable().on('change',updateOutput);
	    updateOutput($('#nestable-menus').data('output', $('#nestable-output')));

		$('.dd-handle a').on('mousedown', function(e){
			console.log(e);
			e.stopPropagation();
		});
		
		$('[data-rel="tooltip"]').tooltip();
	
	});
	});
</script>
