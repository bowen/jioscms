
<title><?php echo $title;?></title>

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		Tables
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Static &amp; Dynamic Tables
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-xs-12">
				<table id="simple-table" class="table  table-bordered table-hover">
					<thead>
						<tr>
							<th class="center">
								<label>
									<input type="checkbox" class="ace" />
									<span class="lbl"></span>
								</label>
							</th>
							<th>name</th>																											
							<th>
								URL
							</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
					<?php foreach ($list as $key=>$val):?>
						<tr>
							<td class="center">
								<label>
									<input type="checkbox" class="ace" value="<?php echo $val->id;?>"/>
									<span class="lbl"></span>
								</label>
							</td>
							<td>
								<a href="#page/saveRole/<?php echo $val->id;?>"><?php echo $val->name;?></a>
							</td>														
							<td><?php echo $val->tarurl;?></td>
							<td>
								<div class="hidden-sm hidden-xs btn-group">

									<button class="btn btn-xs btn-info">
										<i class="ace-icon fa fa-pencil bigger-120"></i>
									</button>

									<button class="btn btn-xs btn-danger">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>
									</button>
								</div>

								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
										</button>

										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
											<li>
												<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
													<span class="blue">
														<i class="ace-icon fa fa-search-plus bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>

											<li>
												<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
				<a href="#save-modal" role="button" class=" btn" data-toggle="modal">添加权限</a>

		<div id="save-modal" class="modal fade" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="smaller lighter blue no-margin">添加权限</h3>
					</div>

					<div class="modal-body">
					   <div class="form-horizontal">
    						<div class="form-group">
                				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name: </label>
                
                				<div class="col-sm-9">
                					<input type="text" id="role_name" placeholder="名称" class="col-xs-10 col-sm-5">
                				</div>
                			</div>
                			<div class="form-group">
                				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> URL: </label>
                
                				<div class="col-sm-9">
                					<input type="text" id="tarurl" placeholder="URL" class="col-xs-10 col-sm-5">
                				</div>
                			</div>
                			<div class="clearfix form-group">
                				<div class="col-md-offset-3 col-md-9">
                					<button class="btn btn-info btn-save" type="button">
                						<i class="ace-icon fa fa-check bigger-110"></i>
                						Save
                					</button>
                				</div>
                			</div>
            			</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i>
							Close
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

				
			</div><!-- /.span -->
		</div><!-- /.row -->

		<div class="hr hr-18 dotted hr-double"></div>


		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- page specific plugin scripts -->
<script type="text/javascript">
    var scripts = [null, null]
    $('.page-content-area').ace_ajax('loadScripts', scripts, function() {
      //inline scripts related to this page
        jQuery(function($) {
            $(".btn-save").click(function(){
				var rolename = $("#role_name").val();
				var tarurl = $("#tarurl").val();
				$.post("/b/doSaveRole",{"rolename":rolename,"tarurl":tarurl},function(res){
					if(res.status)
					{						
						window.location.href=res.url;
					}else
					{
						alert(res.msg);
					}
				},"json");
             });
        })
    });

</script>
