<?php
function a1()
{
	$arr=array(1,0,5,0,3,0,7);
	$m1=memory_get_usage ();
	 
	$len=count($arr);
	 
	for($i=0;$i<$len;$i++){
	    if($arr[$i]==0){
	        for($j=$i;$j<=$len-2;$j++){
	            $arr[$j]=$arr[$j+1];
	        }
	        $arr[$j]=0;
	    }
	}
	$m2=memory_get_usage () ;
	echo $m2-$m1;
	echo "<pre>";
	print_r($arr);
}
function a2()
{
	$arr=array(1,0,5,0,3,0,7);
	$m1=memory_get_usage ();
	 
	foreach($arr as $k=>$v){
	    if($v==0){
	        unset($arr[$k]);
	        array_push($arr, 0);
	    }
	}
	$arr=array_values($arr);
	 
	$m2=memory_get_usage () ;
	echo $m2-$m1;
	 
	echo "<pre>";
	print_r($arr);

}
$start = microtime(true);
a1();
$elapsed = microtime(true) - $start;
echo "1 That took $elapsed seconds.\n";

$start = microtime(true);
a2();
$elapsed = microtime(true) - $start;
echo "2 That took $elapsed seconds.\n";