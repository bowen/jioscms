﻿<?php
$pwd = "123456";
$hash = password_hash($pwd, PASSWORD_DEFAULT);
echo $hash;
echo '<br>';
var_dump(password_get_info($hash));
echo '<br>';
var_dump(password_verify($pwd,'$2y$10$Ok9CRBsYTj/W9nkSMo6QquFP0B4OcO8gpB.58M5n0xxrsGone8IWG'));
if (password_verify($pwd,'$2y$10$Ok9CRBsYTj/W9nkSMo6QquFP0B4OcO8gpB.58M5n0xxrsGone8IWG')) { 
    echo "密码正确";
} else {  
    echo "密码错误";
}
echo '----------------------------------------*------------------------------------------------<br>';
$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
$salt = base64_encode($salt);
$salt = str_replace('+', '.', $salt);
$hash = crypt('rasmuslerdorf', '$2y$10$'.$salt.'$');
echo $hash;

echo '<br>--------------*************************************************------------------------------------------------<br>';
echo password_hash("rasmuslerdorf", PASSWORD_DEFAULT)."<br>";
$options = [
    'cost' => 12,
];
echo password_hash("rasmuslerdorf", PASSWORD_BCRYPT, $options)."<br>";


$options = [
    'cost' => 11
];
echo password_hash("rasmuslerdorf", PASSWORD_BCRYPT, $options)."<br>|||||||";


echo password_hash("trouble 666",PASSWORD_DEFAULT)."<br>";



$timeTarget = 0.05; // 50 milliseconds 

$cost = 12;
do {
    $cost++;
    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
    $end = microtime(true);
} while (($end - $start) < $timeTarget);

echo "Appropriate Cost Found: " . $cost . "\n";
?>